﻿
namespace FitnessClubApp
{
    partial class FormDiagramm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.pnlReport = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbQuarter = new System.Windows.Forms.ComboBox();
            this.gbMonth = new System.Windows.Forms.GroupBox();
            this.cbMonth = new System.Windows.Forms.ComboBox();
            this.rbPeriod = new System.Windows.Forms.RadioButton();
            this.gbPeriod = new System.Windows.Forms.GroupBox();
            this.lblEnd = new System.Windows.Forms.Label();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.lblStart = new System.Windows.Forms.Label();
            this.rbLast5Year = new System.Windows.Forms.RadioButton();
            this.rbQuarter = new System.Windows.Forms.RadioButton();
            this.rbMonth = new System.Windows.Forms.RadioButton();
            this.btnReport = new System.Windows.Forms.Button();
            this.chartSales = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.pnlReport.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbMonth.SuspendLayout();
            this.gbPeriod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartSales)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlReport
            // 
            this.pnlReport.Controls.Add(this.groupBox1);
            this.pnlReport.Controls.Add(this.gbMonth);
            this.pnlReport.Controls.Add(this.rbPeriod);
            this.pnlReport.Controls.Add(this.gbPeriod);
            this.pnlReport.Controls.Add(this.rbLast5Year);
            this.pnlReport.Controls.Add(this.rbQuarter);
            this.pnlReport.Controls.Add(this.rbMonth);
            this.pnlReport.Location = new System.Drawing.Point(6, 8);
            this.pnlReport.Name = "pnlReport";
            this.pnlReport.Size = new System.Drawing.Size(1034, 149);
            this.pnlReport.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbQuarter);
            this.groupBox1.Location = new System.Drawing.Point(613, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(180, 78);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            // 
            // cbQuarter
            // 
            this.cbQuarter.FormattingEnabled = true;
            this.cbQuarter.Items.AddRange(new object[] {
            "1-й квартал",
            "2-й квартал",
            "3-й квартал",
            "4-й квартал"});
            this.cbQuarter.Location = new System.Drawing.Point(33, 31);
            this.cbQuarter.Name = "cbQuarter";
            this.cbQuarter.Size = new System.Drawing.Size(121, 21);
            this.cbQuarter.TabIndex = 11;
            // 
            // gbMonth
            // 
            this.gbMonth.Controls.Add(this.cbMonth);
            this.gbMonth.Location = new System.Drawing.Point(364, 41);
            this.gbMonth.Name = "gbMonth";
            this.gbMonth.Size = new System.Drawing.Size(180, 78);
            this.gbMonth.TabIndex = 16;
            this.gbMonth.TabStop = false;
            // 
            // cbMonth
            // 
            this.cbMonth.FormattingEnabled = true;
            this.cbMonth.Items.AddRange(new object[] {
            "",
            "",
            ""});
            this.cbMonth.Location = new System.Drawing.Point(31, 31);
            this.cbMonth.Name = "cbMonth";
            this.cbMonth.Size = new System.Drawing.Size(121, 21);
            this.cbMonth.TabIndex = 6;
            // 
            // rbPeriod
            // 
            this.rbPeriod.AutoSize = true;
            this.rbPeriod.Checked = true;
            this.rbPeriod.Location = new System.Drawing.Point(7, 13);
            this.rbPeriod.Name = "rbPeriod";
            this.rbPeriod.Size = new System.Drawing.Size(77, 17);
            this.rbPeriod.TabIndex = 12;
            this.rbPeriod.TabStop = true;
            this.rbPeriod.Text = "За период";
            this.rbPeriod.UseVisualStyleBackColor = true;
            // 
            // gbPeriod
            // 
            this.gbPeriod.Controls.Add(this.lblEnd);
            this.gbPeriod.Controls.Add(this.dtpStart);
            this.gbPeriod.Controls.Add(this.dtpEnd);
            this.gbPeriod.Controls.Add(this.lblStart);
            this.gbPeriod.Location = new System.Drawing.Point(7, 40);
            this.gbPeriod.Name = "gbPeriod";
            this.gbPeriod.Size = new System.Drawing.Size(286, 78);
            this.gbPeriod.TabIndex = 7;
            this.gbPeriod.TabStop = false;
            // 
            // lblEnd
            // 
            this.lblEnd.AutoSize = true;
            this.lblEnd.Location = new System.Drawing.Point(8, 51);
            this.lblEnd.Name = "lblEnd";
            this.lblEnd.Size = new System.Drawing.Size(81, 13);
            this.lblEnd.TabIndex = 2;
            this.lblEnd.Text = "Конечная дата";
            // 
            // dtpStart
            // 
            this.dtpStart.Location = new System.Drawing.Point(111, 19);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(169, 20);
            this.dtpStart.TabIndex = 4;
            // 
            // dtpEnd
            // 
            this.dtpEnd.Location = new System.Drawing.Point(111, 51);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(169, 20);
            this.dtpEnd.TabIndex = 5;
            // 
            // lblStart
            // 
            this.lblStart.AutoSize = true;
            this.lblStart.Location = new System.Drawing.Point(8, 19);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(88, 13);
            this.lblStart.TabIndex = 1;
            this.lblStart.Text = "Начальная дата";
            // 
            // rbLast5Year
            // 
            this.rbLast5Year.AutoSize = true;
            this.rbLast5Year.Location = new System.Drawing.Point(855, 59);
            this.rbLast5Year.Name = "rbLast5Year";
            this.rbLast5Year.Size = new System.Drawing.Size(124, 17);
            this.rbLast5Year.TabIndex = 15;
            this.rbLast5Year.Text = "За последние 5 лет";
            this.rbLast5Year.UseVisualStyleBackColor = true;
            // 
            // rbQuarter
            // 
            this.rbQuarter.AutoSize = true;
            this.rbQuarter.Location = new System.Drawing.Point(613, 13);
            this.rbQuarter.Name = "rbQuarter";
            this.rbQuarter.Size = new System.Drawing.Size(97, 17);
            this.rbQuarter.TabIndex = 14;
            this.rbQuarter.Text = "По кварталам";
            this.rbQuarter.UseVisualStyleBackColor = true;
            // 
            // rbMonth
            // 
            this.rbMonth.AutoSize = true;
            this.rbMonth.Location = new System.Drawing.Point(362, 13);
            this.rbMonth.Name = "rbMonth";
            this.rbMonth.Size = new System.Drawing.Size(88, 17);
            this.rbMonth.TabIndex = 13;
            this.rbMonth.Text = "По месяцам";
            this.rbMonth.UseVisualStyleBackColor = true;
            // 
            // btnReport
            // 
            this.btnReport.BackColor = System.Drawing.Color.Moccasin;
            this.btnReport.FlatAppearance.BorderSize = 0;
            this.btnReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReport.Location = new System.Drawing.Point(475, 177);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(75, 23);
            this.btnReport.TabIndex = 7;
            this.btnReport.Text = "Построить";
            this.btnReport.UseVisualStyleBackColor = false;
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // chartSales
            // 
            chartArea1.Name = "ChartArea1";
            this.chartSales.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartSales.Legends.Add(legend1);
            this.chartSales.Location = new System.Drawing.Point(6, 220);
            this.chartSales.Name = "chartSales";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Абонемент";
            this.chartSales.Series.Add(series1);
            this.chartSales.Size = new System.Drawing.Size(1034, 300);
            this.chartSales.TabIndex = 9;
            this.chartSales.Text = "chart1";
            this.chartSales.Visible = false;
            // 
            // FormDiagramm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.ClientSize = new System.Drawing.Size(1046, 532);
            this.Controls.Add(this.chartSales);
            this.Controls.Add(this.pnlReport);
            this.Controls.Add(this.btnReport);
            this.Name = "FormDiagramm";
            this.Text = "FormDiagramm";
            this.pnlReport.ResumeLayout(false);
            this.pnlReport.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.gbMonth.ResumeLayout(false);
            this.gbPeriod.ResumeLayout(false);
            this.gbPeriod.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartSales)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlReport;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbQuarter;
        private System.Windows.Forms.GroupBox gbMonth;
        private System.Windows.Forms.ComboBox cbMonth;
        private System.Windows.Forms.RadioButton rbPeriod;
        private System.Windows.Forms.GroupBox gbPeriod;
        private System.Windows.Forms.Label lblEnd;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.RadioButton rbLast5Year;
        private System.Windows.Forms.RadioButton rbQuarter;
        private System.Windows.Forms.RadioButton rbMonth;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartSales;
    }
}
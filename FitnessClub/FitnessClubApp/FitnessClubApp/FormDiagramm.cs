﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace FitnessClubApp
{
    public partial class FormDiagramm : Form
    {
        public FormDiagramm()
        {
            InitializeComponent();

            int countQuarter = (DateTime.Now.Month + 2) / 3;
            string[] quarter = new string[countQuarter];
            for (int i = 0; i < countQuarter; i++)
            {
                quarter[i] = i + 1 + "-й квартал";
            }

            int countMonth = DateTime.Now.Month;
            string[] month = new string[countMonth];
            for (int i = 0; i < countMonth; i++)
            {
                month[i] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i + 1);
            }

            cbMonth.DataSource = month;
            cbQuarter.DataSource = quarter;
        }

        dbFitnessClubEntities db = new dbFitnessClubEntities();

        private void btnReport_Click(object sender, EventArgs e)
        {
            chartSales.Series[0].Points.Clear();
            DateTime dateStart = new DateTime();
            DateTime dateEnd = new DateTime();
            if (rbPeriod.Checked)
            {
                if (dtpStart.Value <= dtpEnd.Value)
                {
                    dateStart = dtpStart.Value.AddDays(-1);
                    dateEnd = dtpEnd.Value;
                }
                else
                {
                    MessageBox.Show("Неверный формат даты.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (rbMonth.Checked)
            {
                dateStart = new DateTime(DateTime.Now.Year, cbMonth.SelectedIndex + 1, 1);
                dateEnd = new DateTime(DateTime.Now.Year, cbMonth.SelectedIndex + 1, DateTime.DaysInMonth(DateTime.Now.Year, cbMonth.SelectedIndex + 1));
            }
            else if (rbQuarter.Checked)
            {
                int month = (cbQuarter.SelectedIndex + 1) * 3;
                dateStart = new DateTime(DateTime.Now.Year, month - 2, 1);
                dateEnd = new DateTime(DateTime.Now.Year, month, DateTime.DaysInMonth(DateTime.Now.Year, month));
            }
            else
            {
                dateStart = new DateTime(DateTime.Now.Year - 5, 1, 1);
                dateEnd = new DateTime(DateTime.Now.Year, 12, 31);
            }
            if (dtpStart.Value <= dtpEnd.Value || !rbPeriod.Checked)
            {
                var report = db.Klub_Karta.Where(n => n.Data_nachala >= dateStart && n.Data_nachala <= dateEnd)
                    .Select(m => new
                    {
                        m.Abonement.Name
                    }).ToList();

                if (report.Count != 0)
                {
                    chartSales.ChartAreas[0].AxisX.LabelStyle.Angle = -90;
                    chartSales.ChartAreas[0].AxisX.IntervalAutoMode = IntervalAutoMode.VariableCount;
                    chartSales.ChartAreas[0].AxisX.IsLabelAutoFit = false;
                    chartSales.Series[0].IsValueShownAsLabel = true;

                    for (int i = 0; i < report.Count(); i++)
                    {
                        bool check = true;

                        for (int j = 0; j < chartSales.Series[0].Points.Count(); j++)
                        {
                            if (chartSales.Series[0].Points[j].AxisLabel == report[i].Name)
                            {
                                check = false;
                            }
                        }
                        if (check)
                        {
                            chartSales.Series[0].Points.AddXY(report[i].Name, report.Where(n => n.Name == report[i].Name).Count());
                        }
                    }
                    chartSales.Visible = true;
                }
                else
                {
                    MessageBox.Show("За данный период не было продаж", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    chartSales.Visible = false;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;
using Font = System.Drawing.Font;

namespace FitnessClubApp
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }

        dbFitnessClubEntities db = new dbFitnessClubEntities();

        string table = "";

        public void EditTable()
        {
            dgvFitnessClub.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dgvFitnessClub.AutoResizeColumns();
            dgvFitnessClub.AutoResizeRows();
            dgvFitnessClub.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvFitnessClub.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvFitnessClub.ColumnHeadersDefaultCellStyle.Font = new Font(dgvFitnessClub.DefaultCellStyle.Font, FontStyle.Bold);
        }

        private void miCustomers_Click(object sender, EventArgs e)
        {
            miAdd.Enabled = true;
            miEdit.Enabled = false;
            miDelete.Enabled = true;
            table = "CustomersTable";
            var customers = db.Klients.Select(n => new { n.id_klient, n.FIO, n.Data_rozhdeniya }).ToList();
            dgvFitnessClub.DataSource = customers;
            dgvFitnessClub.Columns[0].Visible = false;
            dgvFitnessClub.Columns[1].HeaderText = "ФИО";
            dgvFitnessClub.Columns[2].HeaderText = "Дата рождения";
            EditTable();
        }

        private void miSubscription_Click(object sender, EventArgs e)
        {
            miAdd.Enabled = false;
            miEdit.Enabled = true;
            miDelete.Enabled = false;
            table = "SubscriptionTable";
            var subscription = db.Abonements.Select(n => new { n.id_abonement, n.Name, price = (int)n.Stoimost, n.Opisanie, n.Srok__days_ }).ToList();
            dgvFitnessClub.DataSource = subscription;
            dgvFitnessClub.Columns[0].Visible = false;
            dgvFitnessClub.Columns[1].HeaderText = "Название";
            dgvFitnessClub.Columns[2].HeaderText = "Стоимость";
            dgvFitnessClub.Columns[3].HeaderText = "Описание";
            dgvFitnessClub.Columns[4].HeaderText = "Срок действия (в днях)";
            EditTable();
        }

        private void miClubCard_Click(object sender, EventArgs e)
        {
            miAdd.Enabled = true;
            miEdit.Enabled = false;
            miDelete.Enabled = false;
            table = "ClubCardTable";
            var clubCard = db.Klub_Karta.Select(n => new { n.id_klub, n.Klient.FIO, n.Abonement.Name, n.Data_nachala }).ToList();
            dgvFitnessClub.DataSource = clubCard;
            dgvFitnessClub.Columns[0].Visible = false;
            dgvFitnessClub.Columns[1].HeaderText = "Клиент";
            dgvFitnessClub.Columns[2].HeaderText = "Абонемент";
            dgvFitnessClub.Columns[3].HeaderText = "Дата начала";
            EditTable();
        }

        private void miEdit_Click(object sender, EventArgs e)
        {
            if (dgvFitnessClub.CurrentRow == null)
            {
                MessageBox.Show("Выберите запись для редактирования", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                FormSubscriptionEdit subscriptionEdit = new FormSubscriptionEdit();
                subscriptionEdit.index = int.Parse(dgvFitnessClub.Rows[dgvFitnessClub.CurrentCell.RowIndex].Cells[0].Value.ToString());
                subscriptionEdit.ShowDialog();
                miSubscription_Click(null, null);
            }
        }

        private void miDelete_Click(object sender, EventArgs e)
        {
            if (dgvFitnessClub.CurrentRow == null)
            {
                MessageBox.Show("Выберите запись для удаления", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var index = int.Parse(dgvFitnessClub[0, dgvFitnessClub.CurrentRow.Index].Value.ToString());
                if (MessageBox.Show("Вы уверены, что хотите Удалить запись?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    var customer = db.Klients.FirstOrDefault(n => n.id_klient == index);
                    var clubCard = db.Klub_Karta.Where(n => n.Klient.id_klient == index).ToList();
                    db.Klub_Karta.RemoveRange(clubCard);
                    db.Klients.Remove(customer);
                    db.SaveChanges();
                    miCustomers_Click(null, null);
                }
            }
        }

        private void miAdd_Click(object sender, EventArgs e)
        {
            if (table == "CustomersTable")
            {
                FormNewCustomers newCustomers = new FormNewCustomers();
                newCustomers.ShowDialog();
                miCustomers_Click(null, null);
            } 
            else if (table == "ClubCardTable")
            {
                FormNewClubCard clubCard = new FormNewClubCard();
                clubCard.ShowDialog();
                miClubCard_Click(null, null);
            }
        }

        private void miWord_Click(object sender, EventArgs e)
        {
            saveWord.FileName = "Документ Microsoft Word";
            if (dgvFitnessClub.Rows.Count > 0)
            {
                saveWord.ShowDialog();
            }
            else
            {
                MessageBox.Show("Нет данных для экспорта", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void saveWord_FileOk(object sender, CancelEventArgs e)
        {
            Word.Document Odoc = new Word.Document();
            Odoc.Application.Visible = true;

            object start = 0;
            object end = 0;
            Word.Range tableLocation = Odoc.Range(ref start, ref end);
            Odoc.Tables.Add(tableLocation, dgvFitnessClub.RowCount + 1, dgvFitnessClub.ColumnCount - 1);

            Odoc.Tables[1].Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;
            Odoc.Tables[1].Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;

            for (int j = 1; j < dgvFitnessClub.ColumnCount; j++)
            {
                Odoc.Tables[1].Cell(1, j).Range.Text = dgvFitnessClub.Columns[j].HeaderCell.Value.ToString();
                Odoc.Tables[1].Cell(1, j).Range.Paragraphs.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                Odoc.Tables[1].Cell(1, j).Range.Bold = 5;
            }
            for (int i = 1; i <= dgvFitnessClub.RowCount; i++)
            {
                for (int j = 1; j < dgvFitnessClub.ColumnCount; j++)
                {
                    Odoc.Tables[1].Cell(i + 1, j).Range.Text = dgvFitnessClub[j, i - 1].Value.ToString().Replace(" 0:00:00","");
                }
            }

            Odoc.SaveAs(saveWord.FileName);
        }

        private void miExel_Click(object sender, EventArgs e)
        {
            saveExcel.FileName = "Лист Microsoft Excel";
            if (dgvFitnessClub.Rows.Count > 0)
            {
                saveExcel.ShowDialog();
            }
            else
            {
                MessageBox.Show("Нет данных для экспорта", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void saveExcel_FileOk(object sender, CancelEventArgs e)
        {
            Excel.Application exApp = new Excel.Application();
            exApp.Visible = true;
            exApp.Workbooks.Add();
            Worksheet worksheet = (Worksheet)exApp.ActiveSheet;

            for (int j = 1; j < dgvFitnessClub.ColumnCount; j++)
            {
                worksheet.Cells[1, j] = dgvFitnessClub.Columns[j].HeaderCell.Value.ToString();
            }
            for (int i = 1; i <= dgvFitnessClub.RowCount; i++)
            {
                for (int j = 1; j < dgvFitnessClub.ColumnCount; j++)
                {
                    worksheet.Cells[i + 1, j] = dgvFitnessClub[j, i - 1].Value.ToString().Replace(" 0:00:00", ""); ;

                    if (exApp.Interactive == true)
                    {
                        exApp.Interactive = false;
                    }
                }
            }

            worksheet.Columns.AutoFit();
            worksheet.SaveAs(saveExcel.FileName);
        }

        private void miQuery1_Click(object sender, EventArgs e)
        {
            var clubCardActive = db.Klub_Karta
                .Select(n => new { n.id_klub, n.Klient.FIO, n.Klient.Data_rozhdeniya, n.Abonement.Name, n.Data_nachala, n.Abonement.Srok__days_ })
                .Where(n => n.Data_nachala > System.Data.Entity.DbFunctions.AddDays(DateTime.Now, -n.Srok__days_))
                .ToList();
            dgvFitnessClub.DataSource = clubCardActive;
            dgvFitnessClub.Columns[0].Visible = false;
            dgvFitnessClub.Columns[1].HeaderText = "ФИО клиента";
            dgvFitnessClub.Columns[2].HeaderText = "Дата рождения клиента";
            dgvFitnessClub.Columns[3].HeaderText = "Название абонемента";
            dgvFitnessClub.Columns[4].HeaderText = "Дата начала";
            dgvFitnessClub.Columns[5].HeaderText = "Срок действия";
            EditTable();
        }

        private void miQuery2_Click(object sender, EventArgs e)
        {
            var customersAge = db.Klients
                .Select(n => new { n.id_klient, n.FIO, n.Data_rozhdeniya, 
                    age = System.Data.Entity.DbFunctions.AddYears(n.Data_rozhdeniya, DateTime.Now.Year - n.Data_rozhdeniya.Value.Year) > DateTime.Now ? (DateTime.Now.Year - 1) - n.Data_rozhdeniya.Value.Year : DateTime.Now.Year - n.Data_rozhdeniya.Value.Year })
                .ToList();
            dgvFitnessClub.DataSource = customersAge;
            dgvFitnessClub.Columns[0].Visible = false;
            dgvFitnessClub.Columns[1].HeaderText = "ФИО клиента";
            dgvFitnessClub.Columns[2].HeaderText = "Дата рождения клиента";
            dgvFitnessClub.Columns[3].HeaderText = "Возраст";
            EditTable();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string textSearch = tbSearch.Text;
            if (table == "CustomersTable")
            {
                var search = db.Klients.Where(n => n.FIO.Contains(textSearch))
                    .Select(n => new { n.id_klient, n.FIO, n.Data_rozhdeniya })
                    .ToList();
                dgvFitnessClub.DataSource = search;
                dgvFitnessClub.Columns[0].Visible = false;
                dgvFitnessClub.Columns[1].HeaderText = "ФИО";
                dgvFitnessClub.Columns[2].HeaderText = "Дата рождения";
            }
            else if (table == "SubscriptionTable")
            {
                var search = db.Abonements.Where(n => n.Name.Contains(textSearch) || n.Opisanie.Contains(textSearch))
                    .Select(n => new { n.id_abonement, n.Name, price = (int)n.Stoimost, n.Opisanie, n.Srok__days_ })
                    .ToList();
                dgvFitnessClub.DataSource = search;
                dgvFitnessClub.Columns[0].Visible = false;
                dgvFitnessClub.Columns[1].HeaderText = "Название";
                dgvFitnessClub.Columns[2].HeaderText = "Стоимость";
                dgvFitnessClub.Columns[3].HeaderText = "Описание";
                dgvFitnessClub.Columns[4].HeaderText = "Срок действия (в днях)";
            }
            else if (table == "ClubCardTable")
            {
                var search = db.Klub_Karta.Where(n => n.Klient.FIO.Contains(textSearch) || n.Abonement.Name.Contains(textSearch))
                    .Select(n => new { n.id_klub, n.Klient.FIO, n.Abonement.Name, n.Data_nachala }).ToList();
                dgvFitnessClub.DataSource = search;
                dgvFitnessClub.Columns[0].Visible = false;
                dgvFitnessClub.Columns[1].HeaderText = "Клиент";
                dgvFitnessClub.Columns[2].HeaderText = "Абонемент";
                dgvFitnessClub.Columns[3].HeaderText = "Дата начала";
            }
            EditTable();
        }

        private void mainForm_Load(object sender, EventArgs e)
        {
            miClubCard_Click(null, null);
        }

        private void miChart_Click(object sender, EventArgs e)
        {
            FormDiagramm formDiagramm = new FormDiagramm();
            formDiagramm.Show();
        }
    }
}
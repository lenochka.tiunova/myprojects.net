﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelApp
{
    public partial class AuthorizationForm : Form
    {
        dbHotelEntities db = new dbHotelEntities();
        bool exit = false;
        FormHotelAccount main = new FormHotelAccount(true, -1);

        public AuthorizationForm()
        {
            InitializeComponent();
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            bool login = false;
            var users = db.UserTables.Select(n => new { n.UserId, n.Login, n.Password, n.RoleTable.Privileged }).ToList();
            for (int i = 0; i < users.Count(); i++)
            {
                if (users[i].Login == tbLogin.Text)
                {
                    login = true;
                    break;
                }
            }
            if (login)
            {
                int index = users.FirstOrDefault(n => n.Login == tbLogin.Text).UserId;
                string password = users.FirstOrDefault(n => n.UserId == index).Password.Trim();
                if (password == tbPassword.Text)
                {
                    MessageBox.Show("Вы вошли в систему", "Информация", MessageBoxButtons.OK);
                    if (users.FirstOrDefault(n => n.UserId == index).Privileged)
                    {
                        main = new FormHotelAccount(true, index);
                    }
                    else
                    {
                        main = new FormHotelAccount(false, index);
                    }

                    main.Show();
                    this.Visible = false;
                }
                else
                {
                    MessageBox.Show("Неверный пароль", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Пользователь не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnVisible_Click(object sender, EventArgs e)
        {
            if (tbPassword.UseSystemPasswordChar)
            {
                tbPassword.UseSystemPasswordChar = false;
                btnVisible.Image = Properties.Resources.eye_slash_visible_hidden;
            }
            else
            {
                tbPassword.UseSystemPasswordChar = true;
                btnVisible.Image = Properties.Resources.eye_visible_hidden;
            }
        }

        private void AuthorizationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit && main.DialogResult != DialogResult.OK)
            {
                if ((MessageBox.Show("Вы уверены, что хотите выйти?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }
}

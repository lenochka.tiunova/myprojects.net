﻿namespace HotelApp
{
    partial class FormAddReferenceInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAddReferenceInfo));
            this.btnSave = new System.Windows.Forms.Button();
            this.lblRoomFeatures = new System.Windows.Forms.Label();
            this.tbRoomFeatures = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Sienna;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(181, 68);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblRoomFeatures
            // 
            this.lblRoomFeatures.AutoSize = true;
            this.lblRoomFeatures.Location = new System.Drawing.Point(32, 27);
            this.lblRoomFeatures.Name = "lblRoomFeatures";
            this.lblRoomFeatures.Size = new System.Drawing.Size(118, 13);
            this.lblRoomFeatures.TabIndex = 10;
            this.lblRoomFeatures.Text = "Особенность номера:";
            // 
            // tbRoomFeatures
            // 
            this.tbRoomFeatures.Location = new System.Drawing.Point(156, 24);
            this.tbRoomFeatures.MaxLength = 30;
            this.tbRoomFeatures.Name = "tbRoomFeatures";
            this.tbRoomFeatures.Size = new System.Drawing.Size(268, 20);
            this.tbRoomFeatures.TabIndex = 22;
            // 
            // FormAddReferenceInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tan;
            this.ClientSize = new System.Drawing.Size(447, 114);
            this.Controls.Add(this.tbRoomFeatures);
            this.Controls.Add(this.lblRoomFeatures);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormAddReferenceInfo";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAddReferenceInfo_FormClosing);
            this.Load += new System.EventHandler(this.FormAddReferenceInfo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox tbRoomFeatures;
        public System.Windows.Forms.Label lblRoomFeatures;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelApp
{
    public partial class FormAddReferenceInfo : Form
    {
        public FormAddReferenceInfo()
        {
            InitializeComponent();
        }

        static dbHotelEntities db = new dbHotelEntities();
        public int index = -1;
        private bool exit = false;

        private void FormAddReferenceInfo_Load(object sender, EventArgs e)
        {
            if (this.Text == "Редактирование уровня комфорта")
            {
                tbRoomFeatures.Text = db.TypeComfortTables.FirstOrDefault(n => n.ComfortId == index).Comfort;
            } 
            else if (this.Text == "Редактирование типа размещения")
            {
                tbRoomFeatures.Text = db.TypeAccommodationTables.FirstOrDefault(n => n.AccommodationId == index).Accommodation;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tbRoomFeatures.Text))
            {
                if (lblRoomFeatures.Text == "Тип размещения:")
                {
                    if (db.TypeAccommodationTables.FirstOrDefault(n => n.Accommodation == tbRoomFeatures.Text
                    && n.AccommodationId != index) == null)
                    {
                        if (this.Text == "Добавление типа размещения")
                        {
                            TypeAccommodationTable typeAccommodation = new TypeAccommodationTable
                            {
                                Accommodation = tbRoomFeatures.Text
                            };
                            db.TypeAccommodationTables.Add(typeAccommodation);
                            db.SaveChanges();
                            this.DialogResult = DialogResult.OK;
                            MessageBox.Show("Тип размещения добавлен.", "Внимание!", 
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                        } 
                        else if(this.Text == "Редактирование типа размещения")
                        {
                            var accommodation = db.TypeAccommodationTables.FirstOrDefault(n => n.AccommodationId == index);
                            accommodation.Accommodation = tbRoomFeatures.Text;
                            db.SaveChanges();
                            this.DialogResult = DialogResult.OK;
                            MessageBox.Show("Данные успешно изменены.", "Внимание!",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Данная запись уже существует", "Ошибка", 
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (lblRoomFeatures.Text == "Уровень комфорта:")
                {
                    if (db.TypeComfortTables.FirstOrDefault(n => n.Comfort == tbRoomFeatures.Text
                    && n.ComfortId != index) == null)
                    {
                        if (this.Text == "Добавление уровня комфорта")
                        {
                            TypeComfortTable typeComfort = new TypeComfortTable
                            {
                                Comfort = tbRoomFeatures.Text
                            };
                            db.TypeComfortTables.Add(typeComfort);
                            db.SaveChanges();
                            this.DialogResult = DialogResult.OK;
                            MessageBox.Show("Уровень комфорта добавлен.", "Внимание!", 
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else if((this.Text == "Редактирование уровня комфорта"))
                        {
                            var comfort = db.TypeComfortTables.FirstOrDefault(n => n.ComfortId == index);
                            comfort.Comfort = tbRoomFeatures.Text;
                            db.SaveChanges();
                            this.DialogResult = DialogResult.OK;
                            MessageBox.Show("Данные успешно изменены.", "Внимание!",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Данная запись уже существует", "Ошибка", 
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                exit = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Заполните поле", "Ошибка", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormAddReferenceInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }
}

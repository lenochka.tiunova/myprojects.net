﻿namespace HotelApp
{
    partial class FormBooking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBooking));
            this.lblName = new System.Windows.Forms.Label();
            this.lblNumber = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.cbNumber = new System.Windows.Forms.ComboBox();
            this.lblContactPhone = new System.Windows.Forms.Label();
            this.lblDateSettlement = new System.Windows.Forms.Label();
            this.lblEviction = new System.Windows.Forms.Label();
            this.dtpSettlement = new System.Windows.Forms.DateTimePicker();
            this.tbContactPhone = new System.Windows.Forms.TextBox();
            this.dtpEviction = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(5, 15);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(32, 13);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Имя:";
            // 
            // lblNumber
            // 
            this.lblNumber.AutoSize = true;
            this.lblNumber.Location = new System.Drawing.Point(5, 50);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(44, 13);
            this.lblNumber.TabIndex = 3;
            this.lblNumber.Text = "Номер:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(55, 12);
            this.tbName.MaxLength = 150;
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(305, 20);
            this.tbName.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Sienna;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(201, 134);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbNumber
            // 
            this.cbNumber.BackColor = System.Drawing.Color.White;
            this.cbNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNumber.FormattingEnabled = true;
            this.cbNumber.Location = new System.Drawing.Point(55, 46);
            this.cbNumber.Name = "cbNumber";
            this.cbNumber.Size = new System.Drawing.Size(182, 21);
            this.cbNumber.TabIndex = 2;
            // 
            // lblContactPhone
            // 
            this.lblContactPhone.AutoSize = true;
            this.lblContactPhone.Location = new System.Drawing.Point(243, 50);
            this.lblContactPhone.Name = "lblContactPhone";
            this.lblContactPhone.Size = new System.Drawing.Size(117, 13);
            this.lblContactPhone.TabIndex = 10;
            this.lblContactPhone.Text = "Контактный телефон:";
            // 
            // lblDateSettlement
            // 
            this.lblDateSettlement.AutoSize = true;
            this.lblDateSettlement.Location = new System.Drawing.Point(5, 83);
            this.lblDateSettlement.Name = "lblDateSettlement";
            this.lblDateSettlement.Size = new System.Drawing.Size(137, 13);
            this.lblDateSettlement.TabIndex = 14;
            this.lblDateSettlement.Text = "Дата и время заселения:";
            // 
            // lblEviction
            // 
            this.lblEviction.AutoSize = true;
            this.lblEviction.Location = new System.Drawing.Point(286, 83);
            this.lblEviction.Name = "lblEviction";
            this.lblEviction.Size = new System.Drawing.Size(66, 13);
            this.lblEviction.TabIndex = 17;
            this.lblEviction.Text = "выселения:";
            // 
            // dtpSettlement
            // 
            this.dtpSettlement.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpSettlement.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSettlement.Location = new System.Drawing.Point(148, 80);
            this.dtpSettlement.Name = "dtpSettlement";
            this.dtpSettlement.Size = new System.Drawing.Size(128, 20);
            this.dtpSettlement.TabIndex = 4;
            this.dtpSettlement.ValueChanged += new System.EventHandler(this.dtpSettlement_ValueChanged);
            // 
            // tbContactPhone
            // 
            this.tbContactPhone.Location = new System.Drawing.Point(365, 47);
            this.tbContactPhone.MaxLength = 20;
            this.tbContactPhone.Name = "tbContactPhone";
            this.tbContactPhone.Size = new System.Drawing.Size(111, 20);
            this.tbContactPhone.TabIndex = 3;
            // 
            // dtpEviction
            // 
            this.dtpEviction.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpEviction.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEviction.Location = new System.Drawing.Point(354, 80);
            this.dtpEviction.Name = "dtpEviction";
            this.dtpEviction.Size = new System.Drawing.Size(122, 20);
            this.dtpEviction.TabIndex = 5;
            this.dtpEviction.ValueChanged += new System.EventHandler(this.dtpEviction_ValueChanged);
            // 
            // FormBooking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tan;
            this.ClientSize = new System.Drawing.Size(485, 178);
            this.Controls.Add(this.dtpEviction);
            this.Controls.Add(this.tbContactPhone);
            this.Controls.Add(this.dtpSettlement);
            this.Controls.Add(this.lblEviction);
            this.Controls.Add(this.lblDateSettlement);
            this.Controls.Add(this.lblContactPhone);
            this.Controls.Add(this.cbNumber);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lblNumber);
            this.Controls.Add(this.lblName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormBooking";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление брони";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormBooking_FormClosing);
            this.Load += new System.EventHandler(this.FormBooking_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cbNumber;
        private System.Windows.Forms.Label lblContactPhone;
        private System.Windows.Forms.Label lblDateSettlement;
        private System.Windows.Forms.Label lblEviction;
        private System.Windows.Forms.DateTimePicker dtpSettlement;
        private System.Windows.Forms.TextBox tbContactPhone;
        private System.Windows.Forms.DateTimePicker dtpEviction;
    }
}
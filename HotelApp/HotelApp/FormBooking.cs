﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelApp
{
    public partial class FormBooking : Form
    {
        public FormBooking()
        {
            InitializeComponent();
        }

        static dbHotelEntities db = new dbHotelEntities();
        public int index = -1;
        private bool exit = false;

        private void FormBooking_Load(object sender, EventArgs e)
        {
            if (this.Text == "Изменение информации о бронировании")
            {
                var booking = db.BookingTables.FirstOrDefault(n => n.BookingId == index);
                tbName.Text = booking.Name;
                tbContactPhone.Text = booking.NumberPhone;
                dtpSettlement.Value = booking.CheckInDate;
                dtpEviction.Value = booking.CheckOutDate;
                cbNumber.Text = booking.RoomTable.RoomNumber;
                if(booking.CheckInDate <= DateTime.Now.AddMinutes(30))
                {
                    dtpSettlement.MinDate = booking.CheckInDate;
                } else
                {
                    dtpSettlement.MinDate = DateTime.Now.AddMinutes(30);
                }
            }
            else
            {
                dtpSettlement.MinDate = DateTime.Now.AddMinutes(30);
                dtpSettlement.Value = new DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month,
                    DateTime.Now.AddDays(1).Day, DateTime.Now.Hour, 00, 00);
                dtpEviction.Value = dtpSettlement.Value.AddDays(1);
                cbNumber.SelectedIndex = 0;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(cbNumber.Text) && !String.IsNullOrEmpty(tbName.Text) && !String.IsNullOrEmpty(tbContactPhone.Text))
            {
                if (dtpSettlement.Value > DateTime.Now)
                {
                    if (this.Text == "Добавление брони")
                    {
                        BookingTable booking = new BookingTable
                        {
                            RoomId = db.RoomTables.FirstOrDefault(n => n.RoomNumber == cbNumber.Text).RoomId,
                            Name = tbName.Text,
                            NumberPhone = tbContactPhone.Text,
                            CheckInDate = dtpSettlement.Value,
                            CheckOutDate = dtpEviction.Value
                        };
                        db.BookingTables.Add(booking);
                        db.SaveChanges();
                        MessageBox.Show("Бронь добавлена.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else if (this.Text == "Изменение информации о бронировании")
                    {
                        var booking = db.BookingTables.FirstOrDefault(n => n.BookingId == index);
                        booking.RoomId = db.RoomTables.FirstOrDefault(n => n.RoomNumber == cbNumber.Text).RoomId;
                        booking.Name = tbName.Text;
                        booking.NumberPhone = tbContactPhone.Text;
                        booking.CheckInDate = dtpSettlement.Value;
                        booking.CheckOutDate = dtpEviction.Value;
                        db.SaveChanges();
                        MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    this.DialogResult = DialogResult.OK;
                    exit = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Некорректно введена дата", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Заполните все поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FreeRoom()
        {
            DateTime dtStart = dtpSettlement.Value.AddHours(1);
            DateTime dtEnd = dtpEviction.Value.AddHours(1);
            DateTime dtNow = DateTime.Now.AddHours(-1);

            cbNumber.DataSource = db.RoomTables.Where(n => !db.HotelStayTables
            .Where(x => x.CheckOutDate == null && x.SettlementTable.PlannedСheckOutDate > dtStart)
            .Any(x => x.SettlementTable.RoomTable.RoomNumber == n.RoomNumber)
            && !db.BookingTables
            .Where(x => x.CheckInDate > dtNow && x.BookingId != index 
            && !(x.CheckOutDate < dtStart || x.CheckInDate > dtEnd))
            .Any(x => x.RoomTable.RoomNumber == n.RoomNumber))
                .Select(n => n.RoomNumber).ToList();
        }

        private void dtpSettlement_ValueChanged(object sender, EventArgs e)
        {
            dtpEviction.MinDate = dtpSettlement.Value.AddDays(1);
            FreeRoom();
        }

        private void dtpEviction_ValueChanged(object sender, EventArgs e)
        {
            FreeRoom();
        }

        private void FormBooking_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }
}

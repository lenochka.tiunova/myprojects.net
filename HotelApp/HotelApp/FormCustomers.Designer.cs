﻿namespace HotelApp
{
    partial class FormCustomers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCustomers));
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblMiddle = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.tbSurname = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbMiddle = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbPlaceBirth = new System.Windows.Forms.TextBox();
            this.cbGender = new System.Windows.Forms.ComboBox();
            this.lblSeries = new System.Windows.Forms.Label();
            this.lblDocType = new System.Windows.Forms.Label();
            this.lblCitizenship = new System.Windows.Forms.Label();
            this.lblContactPhone = new System.Windows.Forms.Label();
            this.lblRegistrationAddress = new System.Windows.Forms.Label();
            this.lblResidentialAddress = new System.Windows.Forms.Label();
            this.lblPlaceBirth = new System.Windows.Forms.Label();
            this.lblDateBirth = new System.Windows.Forms.Label();
            this.lblNumber = new System.Windows.Forms.Label();
            this.lblIssyedBy = new System.Windows.Forms.Label();
            this.lblDateOfIssue = new System.Windows.Forms.Label();
            this.dtpDateBirth = new System.Windows.Forms.DateTimePicker();
            this.tbRegAddress = new System.Windows.Forms.TextBox();
            this.tbResidentialAddress = new System.Windows.Forms.TextBox();
            this.cbCitizenship = new System.Windows.Forms.ComboBox();
            this.tbContactPhone = new System.Windows.Forms.TextBox();
            this.tbNumberDoc = new System.Windows.Forms.TextBox();
            this.dtpDateOfIssue = new System.Windows.Forms.DateTimePicker();
            this.tbIssuedBy = new System.Windows.Forms.TextBox();
            this.cbViewDoc = new System.Windows.Forms.ComboBox();
            this.tbSeries = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(72, 17);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(59, 13);
            this.lblSurname.TabIndex = 0;
            this.lblSurname.Text = "Фамилия:";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(99, 46);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(32, 13);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Имя:";
            // 
            // lblMiddle
            // 
            this.lblMiddle.AutoSize = true;
            this.lblMiddle.Location = new System.Drawing.Point(3, 77);
            this.lblMiddle.Name = "lblMiddle";
            this.lblMiddle.Size = new System.Drawing.Size(128, 13);
            this.lblMiddle.TabIndex = 2;
            this.lblMiddle.Text = "Отчество (при наличии):";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(101, 105);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(30, 13);
            this.lblGender.TabIndex = 3;
            this.lblGender.Text = "Пол:";
            // 
            // tbSurname
            // 
            this.tbSurname.Location = new System.Drawing.Point(139, 14);
            this.tbSurname.MaxLength = 50;
            this.tbSurname.Name = "tbSurname";
            this.tbSurname.Size = new System.Drawing.Size(305, 20);
            this.tbSurname.TabIndex = 0;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(139, 43);
            this.tbName.MaxLength = 50;
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(305, 20);
            this.tbName.TabIndex = 1;
            // 
            // tbMiddle
            // 
            this.tbMiddle.Location = new System.Drawing.Point(139, 74);
            this.tbMiddle.MaxLength = 50;
            this.tbMiddle.Name = "tbMiddle";
            this.tbMiddle.Size = new System.Drawing.Size(305, 20);
            this.tbMiddle.TabIndex = 2;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Sienna;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(189, 437);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbPlaceBirth
            // 
            this.tbPlaceBirth.Location = new System.Drawing.Point(139, 133);
            this.tbPlaceBirth.MaxLength = 100;
            this.tbPlaceBirth.Name = "tbPlaceBirth";
            this.tbPlaceBirth.Size = new System.Drawing.Size(305, 20);
            this.tbPlaceBirth.TabIndex = 5;
            // 
            // cbGender
            // 
            this.cbGender.BackColor = System.Drawing.Color.White;
            this.cbGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGender.FormattingEnabled = true;
            this.cbGender.Items.AddRange(new object[] {
            "мужской",
            "женский"});
            this.cbGender.Location = new System.Drawing.Point(139, 102);
            this.cbGender.Name = "cbGender";
            this.cbGender.Size = new System.Drawing.Size(93, 21);
            this.cbGender.TabIndex = 3;
            // 
            // lblSeries
            // 
            this.lblSeries.AutoSize = true;
            this.lblSeries.Location = new System.Drawing.Point(90, 330);
            this.lblSeries.Name = "lblSeries";
            this.lblSeries.Size = new System.Drawing.Size(41, 13);
            this.lblSeries.TabIndex = 7;
            this.lblSeries.Text = "Серия:";
            // 
            // lblDocType
            // 
            this.lblDocType.AutoSize = true;
            this.lblDocType.Location = new System.Drawing.Point(45, 295);
            this.lblDocType.Name = "lblDocType";
            this.lblDocType.Size = new System.Drawing.Size(86, 13);
            this.lblDocType.TabIndex = 8;
            this.lblDocType.Text = "Тип документа:";
            // 
            // lblCitizenship
            // 
            this.lblCitizenship.AutoSize = true;
            this.lblCitizenship.Location = new System.Drawing.Point(54, 262);
            this.lblCitizenship.Name = "lblCitizenship";
            this.lblCitizenship.Size = new System.Drawing.Size(77, 13);
            this.lblCitizenship.TabIndex = 9;
            this.lblCitizenship.Text = "Гражданство:";
            // 
            // lblContactPhone
            // 
            this.lblContactPhone.AutoSize = true;
            this.lblContactPhone.Location = new System.Drawing.Point(14, 224);
            this.lblContactPhone.Name = "lblContactPhone";
            this.lblContactPhone.Size = new System.Drawing.Size(117, 13);
            this.lblContactPhone.TabIndex = 10;
            this.lblContactPhone.Text = "Контактный телефон:";
            // 
            // lblRegistrationAddress
            // 
            this.lblRegistrationAddress.AutoSize = true;
            this.lblRegistrationAddress.Location = new System.Drawing.Point(23, 165);
            this.lblRegistrationAddress.Name = "lblRegistrationAddress";
            this.lblRegistrationAddress.Size = new System.Drawing.Size(108, 13);
            this.lblRegistrationAddress.TabIndex = 11;
            this.lblRegistrationAddress.Text = "Адрес регистрации:";
            // 
            // lblResidentialAddress
            // 
            this.lblResidentialAddress.AutoSize = true;
            this.lblResidentialAddress.Location = new System.Drawing.Point(25, 194);
            this.lblResidentialAddress.Name = "lblResidentialAddress";
            this.lblResidentialAddress.Size = new System.Drawing.Size(106, 13);
            this.lblResidentialAddress.TabIndex = 12;
            this.lblResidentialAddress.Text = "Адрес проживания:";
            // 
            // lblPlaceBirth
            // 
            this.lblPlaceBirth.AutoSize = true;
            this.lblPlaceBirth.Location = new System.Drawing.Point(36, 136);
            this.lblPlaceBirth.Name = "lblPlaceBirth";
            this.lblPlaceBirth.Size = new System.Drawing.Size(95, 13);
            this.lblPlaceBirth.TabIndex = 13;
            this.lblPlaceBirth.Text = "Место рождения:";
            // 
            // lblDateBirth
            // 
            this.lblDateBirth.AutoSize = true;
            this.lblDateBirth.Location = new System.Drawing.Point(255, 105);
            this.lblDateBirth.Name = "lblDateBirth";
            this.lblDateBirth.Size = new System.Drawing.Size(89, 13);
            this.lblDateBirth.TabIndex = 14;
            this.lblDateBirth.Text = "Дата рождения:";
            // 
            // lblNumber
            // 
            this.lblNumber.AutoSize = true;
            this.lblNumber.Location = new System.Drawing.Point(255, 330);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(44, 13);
            this.lblNumber.TabIndex = 15;
            this.lblNumber.Text = "Номер:";
            // 
            // lblIssyedBy
            // 
            this.lblIssyedBy.AutoSize = true;
            this.lblIssyedBy.Location = new System.Drawing.Point(65, 366);
            this.lblIssyedBy.Name = "lblIssyedBy";
            this.lblIssyedBy.Size = new System.Drawing.Size(66, 13);
            this.lblIssyedBy.TabIndex = 16;
            this.lblIssyedBy.Text = "Кем выдан:";
            // 
            // lblDateOfIssue
            // 
            this.lblDateOfIssue.AutoSize = true;
            this.lblDateOfIssue.Location = new System.Drawing.Point(55, 395);
            this.lblDateOfIssue.Name = "lblDateOfIssue";
            this.lblDateOfIssue.Size = new System.Drawing.Size(76, 13);
            this.lblDateOfIssue.TabIndex = 17;
            this.lblDateOfIssue.Text = "Дата выдачи:";
            // 
            // dtpDateBirth
            // 
            this.dtpDateBirth.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDateBirth.Location = new System.Drawing.Point(350, 103);
            this.dtpDateBirth.Name = "dtpDateBirth";
            this.dtpDateBirth.Size = new System.Drawing.Size(94, 20);
            this.dtpDateBirth.TabIndex = 4;
            this.dtpDateBirth.ValueChanged += new System.EventHandler(this.dtpDateBirth_ValueChanged);
            // 
            // tbRegAddress
            // 
            this.tbRegAddress.Location = new System.Drawing.Point(139, 162);
            this.tbRegAddress.MaxLength = 150;
            this.tbRegAddress.Name = "tbRegAddress";
            this.tbRegAddress.Size = new System.Drawing.Size(305, 20);
            this.tbRegAddress.TabIndex = 6;
            // 
            // tbResidentialAddress
            // 
            this.tbResidentialAddress.Location = new System.Drawing.Point(139, 191);
            this.tbResidentialAddress.MaxLength = 150;
            this.tbResidentialAddress.Name = "tbResidentialAddress";
            this.tbResidentialAddress.Size = new System.Drawing.Size(305, 20);
            this.tbResidentialAddress.TabIndex = 7;
            // 
            // cbCitizenship
            // 
            this.cbCitizenship.BackColor = System.Drawing.Color.White;
            this.cbCitizenship.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCitizenship.FormattingEnabled = true;
            this.cbCitizenship.ItemHeight = 13;
            this.cbCitizenship.Items.AddRange(new object[] {
            "Абхазия",
            "Австралия",
            "Австрия",
            "Азербайджан",
            "Албания",
            "Алжир",
            "Ангола",
            "Андорра",
            "Антигуа и Барбуда",
            "Аргентина",
            "Армения",
            "Афганистан",
            "Багамские Острова",
            "Бангладеш",
            "Барбадос",
            "Бахрейн",
            "Белиз",
            "Белоруссия",
            "Бельгия",
            "Бенин",
            "Болгария",
            "Боливия",
            "Босния и Герцеговина",
            "Ботсвана",
            "Бразилия",
            "Бруней",
            "Буркина-Фасо",
            "Бурунди",
            "Бутан",
            "Вануату",
            "Ватикан",
            "Великобритания",
            "Венгрия",
            "Венесуэла",
            "Восточный Тимор",
            "Вьетнам",
            "Габон",
            "Гаити",
            "Гайана",
            "Гамбия",
            "Гана",
            "Гватемала",
            "Гвинея",
            "Гвинея-Бисау",
            "Германия",
            "Гондурас",
            "Государство Палестина",
            "Гренада",
            "Греция",
            "Грузия",
            "Дания",
            "Джибути",
            "Доминика",
            "Доминиканская Республика",
            "ДР Конго",
            "Египет",
            "Замбия",
            "Зимбабве",
            "Израиль",
            "Индия",
            "Индонезия",
            "Иордания",
            "Ирак",
            "Иран",
            "Ирландия",
            "Исландия",
            "Испания",
            "Италия",
            "Йемен",
            "Кабо-Верде",
            "Казахстан",
            "Камбоджа",
            "Камерун",
            "Канада",
            "Катар",
            "Кения",
            "Кипр",
            "Киргизия",
            "Кирибати",
            "Китай",
            "КНДР",
            "Колумбия",
            "Коморские Острова",
            "Коста-Рика",
            "Кот-д\'Ивуар",
            "Куба",
            "Кувейт",
            "Лаос",
            "Латвия",
            "Лесото",
            "Либерия",
            "Ливан",
            "Ливия",
            "Литва",
            "Лихтенштейн",
            "Люксембург",
            "Маврикий",
            "Мавритания",
            "Мадагаскар",
            "Малави",
            "Малайзия",
            "Мали",
            "Мальдивские Острова",
            "Мальта",
            "Марокко",
            "Маршалловы Острова",
            "Мексика",
            "Мозамбик",
            "Молдавия",
            "Монако",
            "Монголия",
            "Мьянма",
            "Намибия",
            "Науру",
            "Непал",
            "Нигер",
            "Нигерия",
            "Нидерланды",
            "Никарагуа",
            "Новая Зеландия",
            "Норвегия",
            "ОАЭ",
            "Оман",
            "Пакистан",
            "Палау",
            "Панама",
            "Папуа - Новая Гвинея",
            "Парагвай",
            "Перу",
            "Польша",
            "Португалия",
            "Республика Конго",
            "Республика Корея",
            "Россия",
            "Руанда",
            "Румыния",
            "Сальвадор",
            "Самоа",
            "Сан-Марино",
            "Сан-Томе и Принсипи",
            "Саудовская Аравия",
            "Северная Македония",
            "Сейшельские Острова",
            "Сенегал",
            "Сент-Винсент и Гренадины",
            "Сент-Китс и Невис",
            "Сент-Люсия",
            "Сербия",
            "Сингапур",
            "Сирия",
            "Словакия",
            "Словения",
            "Соломоновы Острова",
            "Сомали",
            "Судан",
            "Суринам",
            "США",
            "Сьерра-Леоне",
            "Таджикистан",
            "Таиланд",
            "Танзания",
            "Того",
            "Тонга",
            "Тринидад и Тобаго",
            "Тувалу",
            "Тунис",
            "Туркмения",
            "Турция",
            "Уганда",
            "Узбекистан",
            "Украина",
            "Уругвай",
            "Федеративные Штаты Микронезии",
            "Фиджи",
            "Филиппины",
            "Финляндия",
            "Франция",
            "Хорватия",
            "ЦАР",
            "Чад",
            "Черногория",
            "Чехия",
            "Чили",
            "Швейцария",
            "Швеция",
            "Шри-Ланка",
            "Эквадор",
            "Экваториальная Гвинея",
            "Эритрея",
            "Эсватини",
            "Эстония",
            "Эфиопия",
            "ЮАР",
            "Южная Осетия",
            "Южный Судан",
            "Ямайка",
            "Япония"});
            this.cbCitizenship.Location = new System.Drawing.Point(139, 259);
            this.cbCitizenship.Name = "cbCitizenship";
            this.cbCitizenship.Size = new System.Drawing.Size(305, 21);
            this.cbCitizenship.TabIndex = 9;
            this.cbCitizenship.SelectedIndexChanged += new System.EventHandler(this.cbCitizenship_SelectedIndexChanged);
            // 
            // tbContactPhone
            // 
            this.tbContactPhone.Location = new System.Drawing.Point(139, 221);
            this.tbContactPhone.MaxLength = 20;
            this.tbContactPhone.Name = "tbContactPhone";
            this.tbContactPhone.Size = new System.Drawing.Size(179, 20);
            this.tbContactPhone.TabIndex = 8;
            // 
            // tbNumberDoc
            // 
            this.tbNumberDoc.Location = new System.Drawing.Point(305, 327);
            this.tbNumberDoc.MaxLength = 20;
            this.tbNumberDoc.Name = "tbNumberDoc";
            this.tbNumberDoc.Size = new System.Drawing.Size(139, 20);
            this.tbNumberDoc.TabIndex = 12;
            // 
            // dtpDateOfIssue
            // 
            this.dtpDateOfIssue.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDateOfIssue.Location = new System.Drawing.Point(139, 392);
            this.dtpDateOfIssue.Name = "dtpDateOfIssue";
            this.dtpDateOfIssue.Size = new System.Drawing.Size(94, 20);
            this.dtpDateOfIssue.TabIndex = 14;
            // 
            // tbIssuedBy
            // 
            this.tbIssuedBy.Location = new System.Drawing.Point(139, 363);
            this.tbIssuedBy.MaxLength = 150;
            this.tbIssuedBy.Name = "tbIssuedBy";
            this.tbIssuedBy.Size = new System.Drawing.Size(305, 20);
            this.tbIssuedBy.TabIndex = 13;
            // 
            // cbViewDoc
            // 
            this.cbViewDoc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbViewDoc.FormattingEnabled = true;
            this.cbViewDoc.ItemHeight = 13;
            this.cbViewDoc.Items.AddRange(new object[] {
            "Паспорт гражданина РФ",
            "Паспорт иностранного гражданина",
            "Удостоверение личности",
            "Свидетельство о рождении"});
            this.cbViewDoc.Location = new System.Drawing.Point(139, 292);
            this.cbViewDoc.Name = "cbViewDoc";
            this.cbViewDoc.Size = new System.Drawing.Size(305, 21);
            this.cbViewDoc.TabIndex = 10;
            this.cbViewDoc.SelectedIndexChanged += new System.EventHandler(this.cbViewDoc_SelectedIndexChanged);
            // 
            // tbSeries
            // 
            this.tbSeries.Location = new System.Drawing.Point(139, 327);
            this.tbSeries.MaxLength = 20;
            this.tbSeries.Name = "tbSeries";
            this.tbSeries.Size = new System.Drawing.Size(100, 20);
            this.tbSeries.TabIndex = 11;
            // 
            // FormCustomers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tan;
            this.ClientSize = new System.Drawing.Size(458, 483);
            this.Controls.Add(this.tbSeries);
            this.Controls.Add(this.cbViewDoc);
            this.Controls.Add(this.tbIssuedBy);
            this.Controls.Add(this.dtpDateOfIssue);
            this.Controls.Add(this.tbNumberDoc);
            this.Controls.Add(this.tbContactPhone);
            this.Controls.Add(this.cbCitizenship);
            this.Controls.Add(this.tbResidentialAddress);
            this.Controls.Add(this.tbRegAddress);
            this.Controls.Add(this.dtpDateBirth);
            this.Controls.Add(this.lblDateOfIssue);
            this.Controls.Add(this.lblIssyedBy);
            this.Controls.Add(this.lblNumber);
            this.Controls.Add(this.lblDateBirth);
            this.Controls.Add(this.lblPlaceBirth);
            this.Controls.Add(this.lblResidentialAddress);
            this.Controls.Add(this.lblRegistrationAddress);
            this.Controls.Add(this.lblContactPhone);
            this.Controls.Add(this.lblCitizenship);
            this.Controls.Add(this.lblDocType);
            this.Controls.Add(this.lblSeries);
            this.Controls.Add(this.cbGender);
            this.Controls.Add(this.tbPlaceBirth);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbMiddle);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.tbSurname);
            this.Controls.Add(this.lblGender);
            this.Controls.Add(this.lblMiddle);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblSurname);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormCustomers";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Регистрация клиента";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCustomer_FormClosing);
            this.Load += new System.EventHandler(this.FormCustomer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblMiddle;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.TextBox tbSurname;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbMiddle;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox tbPlaceBirth;
        private System.Windows.Forms.ComboBox cbGender;
        private System.Windows.Forms.Label lblSeries;
        private System.Windows.Forms.Label lblDocType;
        private System.Windows.Forms.Label lblCitizenship;
        private System.Windows.Forms.Label lblContactPhone;
        private System.Windows.Forms.Label lblRegistrationAddress;
        private System.Windows.Forms.Label lblResidentialAddress;
        private System.Windows.Forms.Label lblPlaceBirth;
        private System.Windows.Forms.Label lblDateBirth;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.Label lblIssyedBy;
        private System.Windows.Forms.Label lblDateOfIssue;
        private System.Windows.Forms.DateTimePicker dtpDateBirth;
        private System.Windows.Forms.TextBox tbRegAddress;
        private System.Windows.Forms.TextBox tbResidentialAddress;
        private System.Windows.Forms.ComboBox cbCitizenship;
        private System.Windows.Forms.TextBox tbContactPhone;
        private System.Windows.Forms.TextBox tbNumberDoc;
        private System.Windows.Forms.DateTimePicker dtpDateOfIssue;
        private System.Windows.Forms.TextBox tbIssuedBy;
        private System.Windows.Forms.ComboBox cbViewDoc;
        private System.Windows.Forms.TextBox tbSeries;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelApp
{
    public partial class FormCustomers : Form
    {
        public FormCustomers()
        {
            InitializeComponent();
        }

        static dbHotelEntities db = new dbHotelEntities();
        public int index = -1;
        private bool exit = false;

        private void FormCustomer_Load(object sender, EventArgs e)
        {
            cbCitizenship.DropDownHeight = 159;
            if (this.Text == "Редактирование данных о клиенте")
            {
                var customer = db.CustomerTables.FirstOrDefault(n => n.CustomerId == index);
                tbSurname.Text = customer.Surname;
                tbName.Text = customer.Name;
                tbMiddle.Text = customer.Middle;
                cbGender.Text = customer.Gender;
                dtpDateBirth.Value = customer.DateOfBirthday;
                tbPlaceBirth.Text = customer.PlaceBirth;
                tbRegAddress.Text = customer.RegistrationAddress;
                tbResidentialAddress.Text = customer.ResidentialAddress;
                tbContactPhone.Text = customer.ContactNumber;
                cbCitizenship.Text = customer.Сitizenship;
                cbViewDoc.Text = customer.DocumentType;
                tbSeries.Text = customer.Series;
                tbNumberDoc.Text = customer.Number;
                tbIssuedBy.Text = customer.IssuedBy;
                dtpDateOfIssue.Value = customer.DateOfIssue;
            }
            else
            {
                cbGender.SelectedIndex = 0;
                cbCitizenship.Text = "Россия";
                cbViewDoc.SelectedIndex = 0;
                dtpDateBirth.Value = new DateTime(DateTime.Now.AddYears(-30).Year,01,01);
                dtpDateOfIssue.Value = new DateTime(DateTime.Now.AddYears(-10).Year, 01, 01);
            }
            dtpDateBirth.MaxDate = DateTime.Now;
            dtpDateBirth.MinDate = DateTime.Now.AddYears(-150);
            dtpDateOfIssue.MaxDate = DateTime.Now;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tbName.Text)
                    && !String.IsNullOrEmpty(tbSurname.Text)
                    && !String.IsNullOrEmpty(tbPlaceBirth.Text)
                    && !String.IsNullOrEmpty(tbRegAddress.Text)
                    && !String.IsNullOrEmpty(tbResidentialAddress.Text)
                    && !String.IsNullOrEmpty(tbContactPhone.Text)
                    && !String.IsNullOrEmpty(tbNumberDoc.Text)
                    && !String.IsNullOrEmpty(tbIssuedBy.Text)
                    && !(cbCitizenship.Text == "Россия" && String.IsNullOrEmpty(tbSeries.Text)))
            {
                if (this.Text == "Регистрация клиента")
                {
                    CustomerTable customer = new CustomerTable
                    {
                        Surname = tbSurname.Text,
                        Name = tbName.Text,
                        Middle = String.IsNullOrEmpty(tbMiddle.Text) ? null : tbMiddle.Text,
                        Gender = cbGender.Text,
                        DateOfBirthday = dtpDateBirth.Value,
                        PlaceBirth = tbPlaceBirth.Text,
                        RegistrationAddress = tbRegAddress.Text,
                        ResidentialAddress = tbResidentialAddress.Text,
                        ContactNumber = tbContactPhone.Text,
                        Сitizenship = cbCitizenship.Text,
                        DocumentType = cbViewDoc.Text,
                        Series = String.IsNullOrEmpty(tbSeries.Text) ? null : tbSeries.Text,
                        Number = tbNumberDoc.Text,
                        IssuedBy = tbIssuedBy.Text,
                        DateOfIssue = dtpDateOfIssue.Value
                    };
                    db.CustomerTables.Add(customer);
                    db.SaveChanges();
                    MessageBox.Show("Клиент добавлен.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (this.Text == "Редактирование данных о клиенте")
                {
                    var customer = db.CustomerTables.FirstOrDefault(n => n.CustomerId == index);
                    customer.Surname = tbSurname.Text;
                    customer.Name = tbName.Text;
                    customer.Middle = String.IsNullOrEmpty(tbMiddle.Text) ? null : tbMiddle.Text;
                    customer.Gender = cbGender.Text;
                    customer.DateOfBirthday = dtpDateBirth.Value;
                    customer.PlaceBirth = tbPlaceBirth.Text;
                    customer.RegistrationAddress = tbRegAddress.Text;
                    customer.ResidentialAddress = tbResidentialAddress.Text;
                    customer.ContactNumber = tbContactPhone.Text;
                    customer.Сitizenship = cbCitizenship.Text;
                    customer.DocumentType = cbViewDoc.Text;
                    customer.Series = String.IsNullOrEmpty(tbSeries.Text) ? null : tbSeries.Text;
                    customer.Number = tbNumberDoc.Text;
                    customer.IssuedBy = tbIssuedBy.Text;
                    customer.DateOfIssue = dtpDateOfIssue.Value;
                    db.SaveChanges();
                    MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                this.DialogResult = DialogResult.OK;
                exit = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Заполните все поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtpDateBirth_ValueChanged(object sender, EventArgs e)
        {
            dtpDateOfIssue.MinDate = dtpDateBirth.Value;
        }

        private void cbCitizenship_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbCitizenship.Text == "Россия")
            {
                cbViewDoc.SelectedIndex = 0;
            }
            else if (cbViewDoc.SelectedIndex == 0)
            {
                cbViewDoc.SelectedIndex = 1;
            }
        }

        private void cbViewDoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbViewDoc.SelectedIndex == 0)
            {
                cbCitizenship.Text = "Россия";
            }
            else if (cbCitizenship.Text == "Россия")
            {
                cbCitizenship.SelectedIndex = 0;
            }
        }

        private void FormCustomer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }
}

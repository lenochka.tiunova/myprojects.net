﻿namespace HotelApp
{
    partial class FormHotelAccount
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHotelAccount));
            this.menuHotel = new System.Windows.Forms.MenuStrip();
            this.miFile = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.miCustomers = new System.Windows.Forms.ToolStripMenuItem();
            this.miRooms = new System.Windows.Forms.ToolStripMenuItem();
            this.miBooking = new System.Windows.Forms.ToolStripMenuItem();
            this.miHotelStay = new System.Windows.Forms.ToolStripMenuItem();
            this.miUsers = new System.Windows.Forms.ToolStripMenuItem();
            this.miPayment = new System.Windows.Forms.ToolStripMenuItem();
            this.miEditing = new System.Windows.Forms.ToolStripMenuItem();
            this.miAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.miEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.miDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.miExport = new System.Windows.Forms.ToolStripMenuItem();
            this.miExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.miWord = new System.Windows.Forms.ToolStripMenuItem();
            this.miReport = new System.Windows.Forms.ToolStripMenuItem();
            this.miReportProfit = new System.Windows.Forms.ToolStripMenuItem();
            this.miReportCount = new System.Windows.Forms.ToolStripMenuItem();
            this.miCheck = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvShowAccounting = new System.Windows.Forms.DataGridView();
            this.pnlFilterRoom = new System.Windows.Forms.Panel();
            this.gbFreeNumber = new System.Windows.Forms.GroupBox();
            this.cbFreeNumber = new System.Windows.Forms.CheckBox();
            this.lblFinish = new System.Windows.Forms.Label();
            this.dtpStartNumber = new System.Windows.Forms.DateTimePicker();
            this.dtpEndNumber = new System.Windows.Forms.DateTimePicker();
            this.lblStart = new System.Windows.Forms.Label();
            this.btnClearFilterRoom = new System.Windows.Forms.Button();
            this.gbTypeComfort = new System.Windows.Forms.GroupBox();
            this.chlbTypeComfort = new System.Windows.Forms.CheckedListBox();
            this.gbTypeAccommodation = new System.Windows.Forms.GroupBox();
            this.chlbTypeAccommodation = new System.Windows.Forms.CheckedListBox();
            this.gbPrice = new System.Windows.Forms.GroupBox();
            this.cbPrice = new System.Windows.Forms.ComboBox();
            this.pnlFilterCustomer = new System.Windows.Forms.Panel();
            this.btnClearFilterCustomer = new System.Windows.Forms.Button();
            this.gbFindName = new System.Windows.Forms.GroupBox();
            this.tbFullName = new System.Windows.Forms.TextBox();
            this.gbFindNumberPhone = new System.Windows.Forms.GroupBox();
            this.tbNumberPhone = new System.Windows.Forms.TextBox();
            this.saveExcel = new System.Windows.Forms.SaveFileDialog();
            this.saveWord = new System.Windows.Forms.SaveFileDialog();
            this.pnlFilterBooking = new System.Windows.Forms.Panel();
            this.gbNumber = new System.Windows.Forms.GroupBox();
            this.tbNumber = new System.Windows.Forms.TextBox();
            this.gbName = new System.Windows.Forms.GroupBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.gbPhone = new System.Windows.Forms.GroupBox();
            this.tbPhone = new System.Windows.Forms.TextBox();
            this.btnClearFilterBooking = new System.Windows.Forms.Button();
            this.gbPeriod = new System.Windows.Forms.GroupBox();
            this.lblEvictionEnd = new System.Windows.Forms.Label();
            this.dtpEvictionBegin = new System.Windows.Forms.DateTimePicker();
            this.dtpEvictionEnd = new System.Windows.Forms.DateTimePicker();
            this.lblEviction = new System.Windows.Forms.Label();
            this.rbEviction = new System.Windows.Forms.RadioButton();
            this.rbSettlement = new System.Windows.Forms.RadioButton();
            this.btnReset = new System.Windows.Forms.Button();
            this.lblSettlementEnd = new System.Windows.Forms.Label();
            this.dtpSettlementBegin = new System.Windows.Forms.DateTimePicker();
            this.dtpSettlementEnd = new System.Windows.Forms.DateTimePicker();
            this.lblSettlemet = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.pnlFilterHotelStay = new System.Windows.Forms.Panel();
            this.gbViewDoc = new System.Windows.Forms.GroupBox();
            this.cbViewDoc = new System.Windows.Forms.ComboBox();
            this.gbFilterDate = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpEvictionStart = new System.Windows.Forms.DateTimePicker();
            this.dtpEvictionFinish = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.rbEvictionDate = new System.Windows.Forms.RadioButton();
            this.rbSettlementDate = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpSettlementStart = new System.Windows.Forms.DateTimePicker();
            this.dtpSettlementFinish = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.rbHotelStayNow = new System.Windows.Forms.RadioButton();
            this.btnResetDate = new System.Windows.Forms.Button();
            this.gbNumberSearch = new System.Windows.Forms.GroupBox();
            this.tbNumberSearch = new System.Windows.Forms.TextBox();
            this.gbNameSearch = new System.Windows.Forms.GroupBox();
            this.tbNameSearch = new System.Windows.Forms.TextBox();
            this.btnClearFilter = new System.Windows.Forms.Button();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.lblCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.pnlFilterUsers = new System.Windows.Forms.Panel();
            this.gbLogin = new System.Windows.Forms.GroupBox();
            this.tbLogin = new System.Windows.Forms.TextBox();
            this.gbNameUser = new System.Windows.Forms.GroupBox();
            this.tbNameUser = new System.Windows.Forms.TextBox();
            this.btnResetAll = new System.Windows.Forms.Button();
            this.pnlFilterPayment = new System.Windows.Forms.Panel();
            this.gbPayer = new System.Windows.Forms.GroupBox();
            this.tbPayer = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbOperation = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.gbSumPay = new System.Windows.Forms.GroupBox();
            this.nudEnd = new System.Windows.Forms.NumericUpDown();
            this.nudBegin = new System.Windows.Forms.NumericUpDown();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.btnDropSum = new System.Windows.Forms.Button();
            this.gbDatePay = new System.Windows.Forms.GroupBox();
            this.lblEnd = new System.Windows.Forms.Label();
            this.dtpBegin = new System.Windows.Forms.DateTimePicker();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.lblBegin = new System.Windows.Forms.Label();
            this.btnDroppDate = new System.Windows.Forms.Button();
            this.gbCheckNumber = new System.Windows.Forms.GroupBox();
            this.tbCheckNumber = new System.Windows.Forms.TextBox();
            this.gbUsers = new System.Windows.Forms.GroupBox();
            this.tbUsers = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.saveWordCheck = new System.Windows.Forms.SaveFileDialog();
            this.menuHotel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShowAccounting)).BeginInit();
            this.pnlFilterRoom.SuspendLayout();
            this.gbFreeNumber.SuspendLayout();
            this.gbTypeComfort.SuspendLayout();
            this.gbTypeAccommodation.SuspendLayout();
            this.gbPrice.SuspendLayout();
            this.pnlFilterCustomer.SuspendLayout();
            this.gbFindName.SuspendLayout();
            this.gbFindNumberPhone.SuspendLayout();
            this.pnlFilterBooking.SuspendLayout();
            this.gbNumber.SuspendLayout();
            this.gbName.SuspendLayout();
            this.gbPhone.SuspendLayout();
            this.gbPeriod.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.pnlFilterHotelStay.SuspendLayout();
            this.gbViewDoc.SuspendLayout();
            this.gbFilterDate.SuspendLayout();
            this.gbNumberSearch.SuspendLayout();
            this.gbNameSearch.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.pnlFilterUsers.SuspendLayout();
            this.gbLogin.SuspendLayout();
            this.gbNameUser.SuspendLayout();
            this.pnlFilterPayment.SuspendLayout();
            this.gbPayer.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbSumPay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBegin)).BeginInit();
            this.gbDatePay.SuspendLayout();
            this.gbCheckNumber.SuspendLayout();
            this.gbUsers.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuHotel
            // 
            this.menuHotel.BackColor = System.Drawing.Color.Peru;
            this.menuHotel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFile,
            this.miEditing,
            this.miExport,
            this.miReport,
            this.miCheck,
            this.miExit});
            this.menuHotel.Location = new System.Drawing.Point(0, 0);
            this.menuHotel.Name = "menuHotel";
            this.menuHotel.Size = new System.Drawing.Size(1073, 24);
            this.menuHotel.TabIndex = 0;
            this.menuHotel.Text = "menuStrip1";
            // 
            // miFile
            // 
            this.miFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miOpen});
            this.miFile.Name = "miFile";
            this.miFile.Size = new System.Drawing.Size(48, 20);
            this.miFile.Text = "Файл";
            // 
            // miOpen
            // 
            this.miOpen.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCustomers,
            this.miRooms,
            this.miBooking,
            this.miHotelStay,
            this.miUsers,
            this.miPayment});
            this.miOpen.Name = "miOpen";
            this.miOpen.Size = new System.Drawing.Size(121, 22);
            this.miOpen.Text = "Открыть";
            // 
            // miCustomers
            // 
            this.miCustomers.Name = "miCustomers";
            this.miCustomers.Size = new System.Drawing.Size(211, 22);
            this.miCustomers.Text = "Данные о клиентах";
            this.miCustomers.Click += new System.EventHandler(this.miCustomers_Click);
            // 
            // miRooms
            // 
            this.miRooms.Name = "miRooms";
            this.miRooms.Size = new System.Drawing.Size(211, 22);
            this.miRooms.Text = "Стоимость номеров";
            this.miRooms.Click += new System.EventHandler(this.miRooms_Click);
            // 
            // miBooking
            // 
            this.miBooking.Name = "miBooking";
            this.miBooking.Size = new System.Drawing.Size(211, 22);
            this.miBooking.Text = "Учет бронирования";
            this.miBooking.Click += new System.EventHandler(this.miBooking_Click);
            // 
            // miHotelStay
            // 
            this.miHotelStay.Name = "miHotelStay";
            this.miHotelStay.Size = new System.Drawing.Size(211, 22);
            this.miHotelStay.Text = "Проживание";
            this.miHotelStay.Click += new System.EventHandler(this.miHotelStay_Click);
            // 
            // miUsers
            // 
            this.miUsers.Name = "miUsers";
            this.miUsers.Size = new System.Drawing.Size(211, 22);
            this.miUsers.Text = "Данные о пользователях";
            this.miUsers.Click += new System.EventHandler(this.miUsers_Click);
            // 
            // miPayment
            // 
            this.miPayment.Name = "miPayment";
            this.miPayment.Size = new System.Drawing.Size(211, 22);
            this.miPayment.Text = "Оплата";
            this.miPayment.Click += new System.EventHandler(this.miPayment_Click);
            // 
            // miEditing
            // 
            this.miEditing.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAdd,
            this.miEdit,
            this.miDelete});
            this.miEditing.Name = "miEditing";
            this.miEditing.Size = new System.Drawing.Size(108, 20);
            this.miEditing.Text = "Редактирование";
            // 
            // miAdd
            // 
            this.miAdd.Name = "miAdd";
            this.miAdd.Size = new System.Drawing.Size(154, 22);
            this.miAdd.Text = "Добавить";
            this.miAdd.Click += new System.EventHandler(this.miAdd_Click);
            // 
            // miEdit
            // 
            this.miEdit.Name = "miEdit";
            this.miEdit.Size = new System.Drawing.Size(154, 22);
            this.miEdit.Text = "Редактировать";
            this.miEdit.Click += new System.EventHandler(this.miEdit_Click);
            // 
            // miDelete
            // 
            this.miDelete.Name = "miDelete";
            this.miDelete.Size = new System.Drawing.Size(154, 22);
            this.miDelete.Text = "Удалить";
            this.miDelete.Click += new System.EventHandler(this.miDelete_Click);
            // 
            // miExport
            // 
            this.miExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miExcel,
            this.miWord});
            this.miExport.Name = "miExport";
            this.miExport.Size = new System.Drawing.Size(64, 20);
            this.miExport.Text = "Экспорт";
            // 
            // miExcel
            // 
            this.miExcel.Name = "miExcel";
            this.miExcel.Size = new System.Drawing.Size(133, 22);
            this.miExcel.Text = "В MS Excel";
            this.miExcel.Click += new System.EventHandler(this.miExcel_Click);
            // 
            // miWord
            // 
            this.miWord.Name = "miWord";
            this.miWord.Size = new System.Drawing.Size(133, 22);
            this.miWord.Text = "В MS Word";
            this.miWord.Click += new System.EventHandler(this.miWord_Click);
            // 
            // miReport
            // 
            this.miReport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miReportProfit,
            this.miReportCount});
            this.miReport.Name = "miReport";
            this.miReport.Size = new System.Drawing.Size(60, 20);
            this.miReport.Text = "Отчеты";
            // 
            // miReportProfit
            // 
            this.miReportProfit.Name = "miReportProfit";
            this.miReportProfit.Size = new System.Drawing.Size(189, 22);
            this.miReportProfit.Text = "Отчет по прибыли";
            this.miReportProfit.Click += new System.EventHandler(this.miReportProfit_Click);
            // 
            // miReportCount
            // 
            this.miReportCount.Name = "miReportCount";
            this.miReportCount.Size = new System.Drawing.Size(189, 22);
            this.miReportCount.Text = "Отчет по количеству";
            this.miReportCount.Click += new System.EventHandler(this.miReportCount_Click);
            // 
            // miCheck
            // 
            this.miCheck.Name = "miCheck";
            this.miCheck.Size = new System.Drawing.Size(130, 20);
            this.miCheck.Text = "Сформировать счёт";
            this.miCheck.Click += new System.EventHandler(this.miCheck_Click);
            // 
            // miExit
            // 
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(120, 20);
            this.miExit.Text = "Выйти из аккаунта";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // dgvShowAccounting
            // 
            this.dgvShowAccounting.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvShowAccounting.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvShowAccounting.BackgroundColor = System.Drawing.Color.BurlyWood;
            this.dgvShowAccounting.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvShowAccounting.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvShowAccounting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.Format = "f";
            dataGridViewCellStyle2.NullValue = null;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvShowAccounting.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvShowAccounting.Location = new System.Drawing.Point(287, 27);
            this.dgvShowAccounting.MultiSelect = false;
            this.dgvShowAccounting.Name = "dgvShowAccounting";
            this.dgvShowAccounting.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvShowAccounting.Size = new System.Drawing.Size(786, 720);
            this.dgvShowAccounting.TabIndex = 1;
            this.dgvShowAccounting.TabStop = false;
            this.dgvShowAccounting.DataSourceChanged += new System.EventHandler(this.dgvShowAccounting_DataSourceChanged);
            this.dgvShowAccounting.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvShowAccounting_CellFormatting);
            this.dgvShowAccounting.DoubleClick += new System.EventHandler(this.dgvShowAccounting_DoubleClick);
            // 
            // pnlFilterRoom
            // 
            this.pnlFilterRoom.Controls.Add(this.gbFreeNumber);
            this.pnlFilterRoom.Controls.Add(this.btnClearFilterRoom);
            this.pnlFilterRoom.Controls.Add(this.gbTypeComfort);
            this.pnlFilterRoom.Controls.Add(this.gbTypeAccommodation);
            this.pnlFilterRoom.Controls.Add(this.gbPrice);
            this.pnlFilterRoom.Location = new System.Drawing.Point(4, 27);
            this.pnlFilterRoom.Name = "pnlFilterRoom";
            this.pnlFilterRoom.Size = new System.Drawing.Size(277, 695);
            this.pnlFilterRoom.TabIndex = 4;
            // 
            // gbFreeNumber
            // 
            this.gbFreeNumber.Controls.Add(this.cbFreeNumber);
            this.gbFreeNumber.Controls.Add(this.lblFinish);
            this.gbFreeNumber.Controls.Add(this.dtpStartNumber);
            this.gbFreeNumber.Controls.Add(this.dtpEndNumber);
            this.gbFreeNumber.Controls.Add(this.lblStart);
            this.gbFreeNumber.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.gbFreeNumber.Location = new System.Drawing.Point(13, 372);
            this.gbFreeNumber.Name = "gbFreeNumber";
            this.gbFreeNumber.Size = new System.Drawing.Size(251, 146);
            this.gbFreeNumber.TabIndex = 16;
            this.gbFreeNumber.TabStop = false;
            this.gbFreeNumber.Text = "Подбор номера по дате";
            // 
            // cbFreeNumber
            // 
            this.cbFreeNumber.AutoSize = true;
            this.cbFreeNumber.Location = new System.Drawing.Point(28, 26);
            this.cbFreeNumber.Name = "cbFreeNumber";
            this.cbFreeNumber.Size = new System.Drawing.Size(197, 22);
            this.cbFreeNumber.TabIndex = 12;
            this.cbFreeNumber.Text = "Только свободные номера";
            this.cbFreeNumber.UseVisualStyleBackColor = true;
            this.cbFreeNumber.CheckedChanged += new System.EventHandler(this.cbFreeNumber_CheckedChanged);
            // 
            // lblFinish
            // 
            this.lblFinish.AutoSize = true;
            this.lblFinish.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFinish.Location = new System.Drawing.Point(15, 98);
            this.lblFinish.Name = "lblFinish";
            this.lblFinish.Size = new System.Drawing.Size(28, 18);
            this.lblFinish.TabIndex = 2;
            this.lblFinish.Text = "по:";
            // 
            // dtpStartNumber
            // 
            this.dtpStartNumber.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpStartNumber.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpStartNumber.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartNumber.Location = new System.Drawing.Point(56, 63);
            this.dtpStartNumber.Name = "dtpStartNumber";
            this.dtpStartNumber.Size = new System.Drawing.Size(164, 23);
            this.dtpStartNumber.TabIndex = 7;
            this.dtpStartNumber.ValueChanged += new System.EventHandler(this.dtpStartNumber_ValueChanged);
            // 
            // dtpEndNumber
            // 
            this.dtpEndNumber.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpEndNumber.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpEndNumber.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndNumber.Location = new System.Drawing.Point(56, 96);
            this.dtpEndNumber.Name = "dtpEndNumber";
            this.dtpEndNumber.Size = new System.Drawing.Size(164, 23);
            this.dtpEndNumber.TabIndex = 8;
            this.dtpEndNumber.ValueChanged += new System.EventHandler(this.dtpEndNumber_ValueChanged);
            // 
            // lblStart
            // 
            this.lblStart.AutoSize = true;
            this.lblStart.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStart.Location = new System.Drawing.Point(24, 65);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(18, 18);
            this.lblStart.TabIndex = 1;
            this.lblStart.Text = "с:";
            // 
            // btnClearFilterRoom
            // 
            this.btnClearFilterRoom.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnClearFilterRoom.Location = new System.Drawing.Point(80, 535);
            this.btnClearFilterRoom.Name = "btnClearFilterRoom";
            this.btnClearFilterRoom.Size = new System.Drawing.Size(107, 25);
            this.btnClearFilterRoom.TabIndex = 15;
            this.btnClearFilterRoom.Text = "Очистить всё";
            this.btnClearFilterRoom.UseVisualStyleBackColor = true;
            this.btnClearFilterRoom.Click += new System.EventHandler(this.btnClearFilterRoom_Click);
            // 
            // gbTypeComfort
            // 
            this.gbTypeComfort.Controls.Add(this.chlbTypeComfort);
            this.gbTypeComfort.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.gbTypeComfort.Location = new System.Drawing.Point(13, 237);
            this.gbTypeComfort.Name = "gbTypeComfort";
            this.gbTypeComfort.Size = new System.Drawing.Size(251, 126);
            this.gbTypeComfort.TabIndex = 9;
            this.gbTypeComfort.TabStop = false;
            this.gbTypeComfort.Text = "Выберите уровень комфорта";
            // 
            // chlbTypeComfort
            // 
            this.chlbTypeComfort.CheckOnClick = true;
            this.chlbTypeComfort.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chlbTypeComfort.FormattingEnabled = true;
            this.chlbTypeComfort.Location = new System.Drawing.Point(15, 25);
            this.chlbTypeComfort.Name = "chlbTypeComfort";
            this.chlbTypeComfort.Size = new System.Drawing.Size(221, 88);
            this.chlbTypeComfort.TabIndex = 6;
            this.chlbTypeComfort.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.chlbTypeComfort_ItemCheck);
            this.chlbTypeComfort.SelectedIndexChanged += new System.EventHandler(this.chlbTypeComfort_SelectedIndexChanged);
            // 
            // gbTypeAccommodation
            // 
            this.gbTypeAccommodation.Controls.Add(this.chlbTypeAccommodation);
            this.gbTypeAccommodation.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbTypeAccommodation.Location = new System.Drawing.Point(13, 105);
            this.gbTypeAccommodation.Name = "gbTypeAccommodation";
            this.gbTypeAccommodation.Size = new System.Drawing.Size(251, 123);
            this.gbTypeAccommodation.TabIndex = 8;
            this.gbTypeAccommodation.TabStop = false;
            this.gbTypeAccommodation.Text = "Выберите тип размещения";
            // 
            // chlbTypeAccommodation
            // 
            this.chlbTypeAccommodation.CheckOnClick = true;
            this.chlbTypeAccommodation.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chlbTypeAccommodation.FormattingEnabled = true;
            this.chlbTypeAccommodation.Location = new System.Drawing.Point(15, 25);
            this.chlbTypeAccommodation.Name = "chlbTypeAccommodation";
            this.chlbTypeAccommodation.Size = new System.Drawing.Size(221, 88);
            this.chlbTypeAccommodation.TabIndex = 7;
            this.chlbTypeAccommodation.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.chlbTypeAccommodation_ItemCheck);
            this.chlbTypeAccommodation.SelectedIndexChanged += new System.EventHandler(this.chlbTypeAccommodation_SelectedIndexChanged);
            // 
            // gbPrice
            // 
            this.gbPrice.Controls.Add(this.cbPrice);
            this.gbPrice.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbPrice.Location = new System.Drawing.Point(13, 7);
            this.gbPrice.Name = "gbPrice";
            this.gbPrice.Size = new System.Drawing.Size(251, 89);
            this.gbPrice.TabIndex = 4;
            this.gbPrice.TabStop = false;
            this.gbPrice.Text = "Сортировать по цене";
            // 
            // cbPrice
            // 
            this.cbPrice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPrice.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbPrice.FormattingEnabled = true;
            this.cbPrice.Items.AddRange(new object[] {
            "Хаотично",
            "Сначала бюджетные",
            "Сначала дорогие"});
            this.cbPrice.Location = new System.Drawing.Point(15, 37);
            this.cbPrice.Name = "cbPrice";
            this.cbPrice.Size = new System.Drawing.Size(221, 26);
            this.cbPrice.TabIndex = 2;
            this.cbPrice.SelectedIndexChanged += new System.EventHandler(this.cbPrice_SelectedIndexChanged);
            // 
            // pnlFilterCustomer
            // 
            this.pnlFilterCustomer.BackColor = System.Drawing.Color.BurlyWood;
            this.pnlFilterCustomer.Controls.Add(this.btnClearFilterCustomer);
            this.pnlFilterCustomer.Controls.Add(this.gbFindName);
            this.pnlFilterCustomer.Controls.Add(this.gbFindNumberPhone);
            this.pnlFilterCustomer.Location = new System.Drawing.Point(4, 27);
            this.pnlFilterCustomer.Name = "pnlFilterCustomer";
            this.pnlFilterCustomer.Size = new System.Drawing.Size(277, 695);
            this.pnlFilterCustomer.TabIndex = 5;
            this.pnlFilterCustomer.Visible = false;
            // 
            // btnClearFilterCustomer
            // 
            this.btnClearFilterCustomer.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnClearFilterCustomer.Location = new System.Drawing.Point(85, 174);
            this.btnClearFilterCustomer.Name = "btnClearFilterCustomer";
            this.btnClearFilterCustomer.Size = new System.Drawing.Size(107, 25);
            this.btnClearFilterCustomer.TabIndex = 10;
            this.btnClearFilterCustomer.Text = "Очистить всё";
            this.btnClearFilterCustomer.UseVisualStyleBackColor = true;
            this.btnClearFilterCustomer.Click += new System.EventHandler(this.btnClearFilterCustomer_Click);
            // 
            // gbFindName
            // 
            this.gbFindName.Controls.Add(this.tbFullName);
            this.gbFindName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbFindName.Location = new System.Drawing.Point(13, 73);
            this.gbFindName.Name = "gbFindName";
            this.gbFindName.Size = new System.Drawing.Size(251, 71);
            this.gbFindName.TabIndex = 1;
            this.gbFindName.TabStop = false;
            this.gbFindName.Text = "Введите имя";
            // 
            // tbFullName
            // 
            this.tbFullName.Location = new System.Drawing.Point(15, 29);
            this.tbFullName.Name = "tbFullName";
            this.tbFullName.Size = new System.Drawing.Size(221, 26);
            this.tbFullName.TabIndex = 1;
            this.tbFullName.TextChanged += new System.EventHandler(this.tbName_TextChanged);
            // 
            // gbFindNumberPhone
            // 
            this.gbFindNumberPhone.Controls.Add(this.tbNumberPhone);
            this.gbFindNumberPhone.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbFindNumberPhone.Location = new System.Drawing.Point(13, 4);
            this.gbFindNumberPhone.Name = "gbFindNumberPhone";
            this.gbFindNumberPhone.Size = new System.Drawing.Size(251, 60);
            this.gbFindNumberPhone.TabIndex = 0;
            this.gbFindNumberPhone.TabStop = false;
            this.gbFindNumberPhone.Text = "Введите номер телефона";
            // 
            // tbNumberPhone
            // 
            this.tbNumberPhone.Location = new System.Drawing.Point(15, 25);
            this.tbNumberPhone.Name = "tbNumberPhone";
            this.tbNumberPhone.Size = new System.Drawing.Size(221, 26);
            this.tbNumberPhone.TabIndex = 0;
            this.tbNumberPhone.TextChanged += new System.EventHandler(this.tbNumberPhone_TextChanged);
            // 
            // saveExcel
            // 
            this.saveExcel.Filter = "Книга Excel (*.xlsx)|*.xlsx";
            this.saveExcel.FileOk += new System.ComponentModel.CancelEventHandler(this.saveExcel_FileOk);
            // 
            // saveWord
            // 
            this.saveWord.Filter = "Документ Microsoft Word (*.docx)|*.docx";
            this.saveWord.FileOk += new System.ComponentModel.CancelEventHandler(this.saveWord_FileOk);
            // 
            // pnlFilterBooking
            // 
            this.pnlFilterBooking.BackColor = System.Drawing.Color.BurlyWood;
            this.pnlFilterBooking.Controls.Add(this.gbNumber);
            this.pnlFilterBooking.Controls.Add(this.gbName);
            this.pnlFilterBooking.Controls.Add(this.gbPhone);
            this.pnlFilterBooking.Controls.Add(this.btnClearFilterBooking);
            this.pnlFilterBooking.Controls.Add(this.gbPeriod);
            this.pnlFilterBooking.Location = new System.Drawing.Point(4, 27);
            this.pnlFilterBooking.Name = "pnlFilterBooking";
            this.pnlFilterBooking.Size = new System.Drawing.Size(277, 695);
            this.pnlFilterBooking.TabIndex = 7;
            this.pnlFilterBooking.Visible = false;
            // 
            // gbNumber
            // 
            this.gbNumber.Controls.Add(this.tbNumber);
            this.gbNumber.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbNumber.Location = new System.Drawing.Point(13, 156);
            this.gbNumber.Name = "gbNumber";
            this.gbNumber.Size = new System.Drawing.Size(251, 71);
            this.gbNumber.TabIndex = 13;
            this.gbNumber.TabStop = false;
            this.gbNumber.Text = " Введите номер";
            // 
            // tbNumber
            // 
            this.tbNumber.Location = new System.Drawing.Point(15, 29);
            this.tbNumber.Name = "tbNumber";
            this.tbNumber.Size = new System.Drawing.Size(221, 26);
            this.tbNumber.TabIndex = 1;
            this.tbNumber.TextChanged += new System.EventHandler(this.tbNumber_TextChanged);
            // 
            // gbName
            // 
            this.gbName.Controls.Add(this.tbName);
            this.gbName.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbName.Location = new System.Drawing.Point(13, 75);
            this.gbName.Name = "gbName";
            this.gbName.Size = new System.Drawing.Size(251, 71);
            this.gbName.TabIndex = 12;
            this.gbName.TabStop = false;
            this.gbName.Text = "Введите имя";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(15, 29);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(221, 26);
            this.tbName.TabIndex = 1;
            this.tbName.TextChanged += new System.EventHandler(this.tbName_TextChanged_1);
            // 
            // gbPhone
            // 
            this.gbPhone.Controls.Add(this.tbPhone);
            this.gbPhone.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbPhone.Location = new System.Drawing.Point(13, 6);
            this.gbPhone.Name = "gbPhone";
            this.gbPhone.Size = new System.Drawing.Size(251, 60);
            this.gbPhone.TabIndex = 11;
            this.gbPhone.TabStop = false;
            this.gbPhone.Text = "Введите номер телефона";
            // 
            // tbPhone
            // 
            this.tbPhone.Location = new System.Drawing.Point(15, 25);
            this.tbPhone.Name = "tbPhone";
            this.tbPhone.Size = new System.Drawing.Size(221, 26);
            this.tbPhone.TabIndex = 0;
            this.tbPhone.TextChanged += new System.EventHandler(this.tbPhone_TextChanged);
            // 
            // btnClearFilterBooking
            // 
            this.btnClearFilterBooking.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnClearFilterBooking.Location = new System.Drawing.Point(79, 511);
            this.btnClearFilterBooking.Name = "btnClearFilterBooking";
            this.btnClearFilterBooking.Size = new System.Drawing.Size(107, 25);
            this.btnClearFilterBooking.TabIndex = 10;
            this.btnClearFilterBooking.Text = "Очистить всё";
            this.btnClearFilterBooking.UseVisualStyleBackColor = true;
            this.btnClearFilterBooking.Click += new System.EventHandler(this.btnClearFilterBooking_Click);
            // 
            // gbPeriod
            // 
            this.gbPeriod.Controls.Add(this.lblEvictionEnd);
            this.gbPeriod.Controls.Add(this.dtpEvictionBegin);
            this.gbPeriod.Controls.Add(this.dtpEvictionEnd);
            this.gbPeriod.Controls.Add(this.lblEviction);
            this.gbPeriod.Controls.Add(this.rbEviction);
            this.gbPeriod.Controls.Add(this.rbSettlement);
            this.gbPeriod.Controls.Add(this.btnReset);
            this.gbPeriod.Controls.Add(this.lblSettlementEnd);
            this.gbPeriod.Controls.Add(this.dtpSettlementBegin);
            this.gbPeriod.Controls.Add(this.dtpSettlementEnd);
            this.gbPeriod.Controls.Add(this.lblSettlemet);
            this.gbPeriod.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.gbPeriod.Location = new System.Drawing.Point(13, 240);
            this.gbPeriod.Name = "gbPeriod";
            this.gbPeriod.Size = new System.Drawing.Size(251, 260);
            this.gbPeriod.TabIndex = 7;
            this.gbPeriod.TabStop = false;
            this.gbPeriod.Text = "Сортировать по дате";
            // 
            // lblEvictionEnd
            // 
            this.lblEvictionEnd.AutoSize = true;
            this.lblEvictionEnd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblEvictionEnd.Location = new System.Drawing.Point(25, 191);
            this.lblEvictionEnd.Name = "lblEvictionEnd";
            this.lblEvictionEnd.Size = new System.Drawing.Size(28, 18);
            this.lblEvictionEnd.TabIndex = 15;
            this.lblEvictionEnd.Text = "по:";
            // 
            // dtpEvictionBegin
            // 
            this.dtpEvictionBegin.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpEvictionBegin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpEvictionBegin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEvictionBegin.Location = new System.Drawing.Point(66, 156);
            this.dtpEvictionBegin.Name = "dtpEvictionBegin";
            this.dtpEvictionBegin.Size = new System.Drawing.Size(164, 23);
            this.dtpEvictionBegin.TabIndex = 16;
            this.dtpEvictionBegin.ValueChanged += new System.EventHandler(this.dtpEvictionBegin_ValueChanged);
            // 
            // dtpEvictionEnd
            // 
            this.dtpEvictionEnd.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpEvictionEnd.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpEvictionEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEvictionEnd.Location = new System.Drawing.Point(66, 189);
            this.dtpEvictionEnd.Name = "dtpEvictionEnd";
            this.dtpEvictionEnd.Size = new System.Drawing.Size(164, 23);
            this.dtpEvictionEnd.TabIndex = 17;
            this.dtpEvictionEnd.ValueChanged += new System.EventHandler(this.dtpEvictionEnd_ValueChanged);
            // 
            // lblEviction
            // 
            this.lblEviction.AutoSize = true;
            this.lblEviction.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblEviction.Location = new System.Drawing.Point(34, 158);
            this.lblEviction.Name = "lblEviction";
            this.lblEviction.Size = new System.Drawing.Size(18, 18);
            this.lblEviction.TabIndex = 14;
            this.lblEviction.Text = "с:";
            // 
            // rbEviction
            // 
            this.rbEviction.AutoSize = true;
            this.rbEviction.Location = new System.Drawing.Point(16, 122);
            this.rbEviction.Name = "rbEviction";
            this.rbEviction.Size = new System.Drawing.Size(100, 22);
            this.rbEviction.TabIndex = 13;
            this.rbEviction.TabStop = true;
            this.rbEviction.Text = "выселения:";
            this.rbEviction.UseVisualStyleBackColor = true;
            this.rbEviction.CheckedChanged += new System.EventHandler(this.rbEviction_CheckedChanged);
            // 
            // rbSettlement
            // 
            this.rbSettlement.AutoSize = true;
            this.rbSettlement.Location = new System.Drawing.Point(28, 24);
            this.rbSettlement.Name = "rbSettlement";
            this.rbSettlement.Size = new System.Drawing.Size(88, 22);
            this.rbSettlement.TabIndex = 12;
            this.rbSettlement.TabStop = true;
            this.rbSettlement.Text = "засления:";
            this.rbSettlement.UseVisualStyleBackColor = true;
            this.rbSettlement.CheckedChanged += new System.EventHandler(this.rbSettlement_CheckedChanged);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(72, 229);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(91, 25);
            this.btnReset.TabIndex = 11;
            this.btnReset.Text = "Сброс";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // lblSettlementEnd
            // 
            this.lblSettlementEnd.AutoSize = true;
            this.lblSettlementEnd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSettlementEnd.Location = new System.Drawing.Point(25, 88);
            this.lblSettlementEnd.Name = "lblSettlementEnd";
            this.lblSettlementEnd.Size = new System.Drawing.Size(28, 18);
            this.lblSettlementEnd.TabIndex = 2;
            this.lblSettlementEnd.Text = "по:";
            // 
            // dtpSettlementBegin
            // 
            this.dtpSettlementBegin.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpSettlementBegin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpSettlementBegin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSettlementBegin.Location = new System.Drawing.Point(66, 53);
            this.dtpSettlementBegin.Name = "dtpSettlementBegin";
            this.dtpSettlementBegin.Size = new System.Drawing.Size(164, 23);
            this.dtpSettlementBegin.TabIndex = 7;
            this.dtpSettlementBegin.ValueChanged += new System.EventHandler(this.dtpSettlementBegin_ValueChanged);
            // 
            // dtpSettlementEnd
            // 
            this.dtpSettlementEnd.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpSettlementEnd.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpSettlementEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSettlementEnd.Location = new System.Drawing.Point(66, 86);
            this.dtpSettlementEnd.Name = "dtpSettlementEnd";
            this.dtpSettlementEnd.Size = new System.Drawing.Size(164, 23);
            this.dtpSettlementEnd.TabIndex = 8;
            this.dtpSettlementEnd.ValueChanged += new System.EventHandler(this.dtpSettlementEnd_ValueChanged);
            // 
            // lblSettlemet
            // 
            this.lblSettlemet.AutoSize = true;
            this.lblSettlemet.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSettlemet.Location = new System.Drawing.Point(34, 55);
            this.lblSettlemet.Name = "lblSettlemet";
            this.lblSettlemet.Size = new System.Drawing.Size(18, 18);
            this.lblSettlemet.TabIndex = 1;
            this.lblSettlemet.Text = "с:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.textBox1);
            this.groupBox7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox7.Location = new System.Drawing.Point(402, 82);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(251, 60);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Введите имя";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(15, 25);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(221, 26);
            this.textBox1.TabIndex = 0;
            // 
            // pnlFilterHotelStay
            // 
            this.pnlFilterHotelStay.BackColor = System.Drawing.Color.BurlyWood;
            this.pnlFilterHotelStay.Controls.Add(this.gbViewDoc);
            this.pnlFilterHotelStay.Controls.Add(this.gbFilterDate);
            this.pnlFilterHotelStay.Controls.Add(this.gbNumberSearch);
            this.pnlFilterHotelStay.Controls.Add(this.gbNameSearch);
            this.pnlFilterHotelStay.Controls.Add(this.btnClearFilter);
            this.pnlFilterHotelStay.Location = new System.Drawing.Point(4, 27);
            this.pnlFilterHotelStay.Name = "pnlFilterHotelStay";
            this.pnlFilterHotelStay.Size = new System.Drawing.Size(277, 695);
            this.pnlFilterHotelStay.TabIndex = 11;
            this.pnlFilterHotelStay.Visible = false;
            // 
            // gbViewDoc
            // 
            this.gbViewDoc.Controls.Add(this.cbViewDoc);
            this.gbViewDoc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbViewDoc.Location = new System.Drawing.Point(10, 465);
            this.gbViewDoc.Name = "gbViewDoc";
            this.gbViewDoc.Size = new System.Drawing.Size(261, 71);
            this.gbViewDoc.TabIndex = 17;
            this.gbViewDoc.TabStop = false;
            this.gbViewDoc.Text = "Выберите тип документа";
            // 
            // cbViewDoc
            // 
            this.cbViewDoc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbViewDoc.FormattingEnabled = true;
            this.cbViewDoc.ItemHeight = 18;
            this.cbViewDoc.Items.AddRange(new object[] {
            "Выбрать все",
            "Паспорт иностранного гражданина"});
            this.cbViewDoc.Location = new System.Drawing.Point(5, 30);
            this.cbViewDoc.Name = "cbViewDoc";
            this.cbViewDoc.Size = new System.Drawing.Size(251, 26);
            this.cbViewDoc.TabIndex = 18;
            this.cbViewDoc.SelectedIndexChanged += new System.EventHandler(this.cbViewDoc_SelectedIndexChanged);
            // 
            // gbFilterDate
            // 
            this.gbFilterDate.Controls.Add(this.label3);
            this.gbFilterDate.Controls.Add(this.dtpEvictionStart);
            this.gbFilterDate.Controls.Add(this.dtpEvictionFinish);
            this.gbFilterDate.Controls.Add(this.label4);
            this.gbFilterDate.Controls.Add(this.rbEvictionDate);
            this.gbFilterDate.Controls.Add(this.rbSettlementDate);
            this.gbFilterDate.Controls.Add(this.label5);
            this.gbFilterDate.Controls.Add(this.dtpSettlementStart);
            this.gbFilterDate.Controls.Add(this.dtpSettlementFinish);
            this.gbFilterDate.Controls.Add(this.label6);
            this.gbFilterDate.Controls.Add(this.rbHotelStayNow);
            this.gbFilterDate.Controls.Add(this.btnResetDate);
            this.gbFilterDate.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.gbFilterDate.Location = new System.Drawing.Point(10, 172);
            this.gbFilterDate.Name = "gbFilterDate";
            this.gbFilterDate.Size = new System.Drawing.Size(261, 280);
            this.gbFilterDate.TabIndex = 16;
            this.gbFilterDate.TabStop = false;
            this.gbFilterDate.Text = "Сортировать по дате";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(25, 206);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 18);
            this.label3.TabIndex = 25;
            this.label3.Text = "по:";
            // 
            // dtpEvictionStart
            // 
            this.dtpEvictionStart.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpEvictionStart.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpEvictionStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEvictionStart.Location = new System.Drawing.Point(66, 172);
            this.dtpEvictionStart.Name = "dtpEvictionStart";
            this.dtpEvictionStart.Size = new System.Drawing.Size(164, 23);
            this.dtpEvictionStart.TabIndex = 26;
            this.dtpEvictionStart.ValueChanged += new System.EventHandler(this.dtpEvictionStart_ValueChanged);
            // 
            // dtpEvictionFinish
            // 
            this.dtpEvictionFinish.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpEvictionFinish.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpEvictionFinish.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEvictionFinish.Location = new System.Drawing.Point(66, 205);
            this.dtpEvictionFinish.Name = "dtpEvictionFinish";
            this.dtpEvictionFinish.Size = new System.Drawing.Size(164, 23);
            this.dtpEvictionFinish.TabIndex = 27;
            this.dtpEvictionFinish.ValueChanged += new System.EventHandler(this.dtpEvictionFinish_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(34, 173);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 18);
            this.label4.TabIndex = 24;
            this.label4.Text = "с:";
            // 
            // rbEvictionDate
            // 
            this.rbEvictionDate.AutoSize = true;
            this.rbEvictionDate.Location = new System.Drawing.Point(28, 144);
            this.rbEvictionDate.Name = "rbEvictionDate";
            this.rbEvictionDate.Size = new System.Drawing.Size(100, 22);
            this.rbEvictionDate.TabIndex = 23;
            this.rbEvictionDate.TabStop = true;
            this.rbEvictionDate.Text = "выселения:";
            this.rbEvictionDate.UseVisualStyleBackColor = true;
            this.rbEvictionDate.CheckedChanged += new System.EventHandler(this.rbEvictionDate_CheckedChanged);
            // 
            // rbSettlementDate
            // 
            this.rbSettlementDate.AutoSize = true;
            this.rbSettlementDate.Location = new System.Drawing.Point(28, 52);
            this.rbSettlementDate.Name = "rbSettlementDate";
            this.rbSettlementDate.Size = new System.Drawing.Size(96, 22);
            this.rbSettlementDate.TabIndex = 22;
            this.rbSettlementDate.TabStop = true;
            this.rbSettlementDate.Text = "заселения:";
            this.rbSettlementDate.UseVisualStyleBackColor = true;
            this.rbSettlementDate.CheckedChanged += new System.EventHandler(this.rbSettlementDate_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(25, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 18);
            this.label5.TabIndex = 19;
            this.label5.Text = "по:";
            // 
            // dtpSettlementStart
            // 
            this.dtpSettlementStart.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpSettlementStart.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpSettlementStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSettlementStart.Location = new System.Drawing.Point(66, 81);
            this.dtpSettlementStart.Name = "dtpSettlementStart";
            this.dtpSettlementStart.Size = new System.Drawing.Size(164, 23);
            this.dtpSettlementStart.TabIndex = 20;
            this.dtpSettlementStart.ValueChanged += new System.EventHandler(this.dtpSettlementStart_ValueChanged);
            // 
            // dtpSettlementFinish
            // 
            this.dtpSettlementFinish.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpSettlementFinish.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpSettlementFinish.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSettlementFinish.Location = new System.Drawing.Point(66, 114);
            this.dtpSettlementFinish.Name = "dtpSettlementFinish";
            this.dtpSettlementFinish.Size = new System.Drawing.Size(164, 23);
            this.dtpSettlementFinish.TabIndex = 21;
            this.dtpSettlementFinish.ValueChanged += new System.EventHandler(this.dtpSettlementFinish_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(34, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 18);
            this.label6.TabIndex = 18;
            this.label6.Text = "с:";
            // 
            // rbHotelStayNow
            // 
            this.rbHotelStayNow.AutoSize = true;
            this.rbHotelStayNow.Location = new System.Drawing.Point(28, 24);
            this.rbHotelStayNow.Name = "rbHotelStayNow";
            this.rbHotelStayNow.Size = new System.Drawing.Size(212, 22);
            this.rbHotelStayNow.TabIndex = 12;
            this.rbHotelStayNow.TabStop = true;
            this.rbHotelStayNow.Text = "только проживающие сейчас";
            this.rbHotelStayNow.UseVisualStyleBackColor = true;
            this.rbHotelStayNow.CheckedChanged += new System.EventHandler(this.rbHotelStayNow_CheckedChanged);
            // 
            // btnResetDate
            // 
            this.btnResetDate.Location = new System.Drawing.Point(79, 239);
            this.btnResetDate.Name = "btnResetDate";
            this.btnResetDate.Size = new System.Drawing.Size(91, 25);
            this.btnResetDate.TabIndex = 11;
            this.btnResetDate.Text = "Сброс";
            this.btnResetDate.UseVisualStyleBackColor = true;
            this.btnResetDate.Click += new System.EventHandler(this.btnResetDate_Click);
            // 
            // gbNumberSearch
            // 
            this.gbNumberSearch.Controls.Add(this.tbNumberSearch);
            this.gbNumberSearch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbNumberSearch.Location = new System.Drawing.Point(10, 90);
            this.gbNumberSearch.Name = "gbNumberSearch";
            this.gbNumberSearch.Size = new System.Drawing.Size(261, 71);
            this.gbNumberSearch.TabIndex = 15;
            this.gbNumberSearch.TabStop = false;
            this.gbNumberSearch.Text = " Введите номер";
            // 
            // tbNumberSearch
            // 
            this.tbNumberSearch.Location = new System.Drawing.Point(15, 29);
            this.tbNumberSearch.Name = "tbNumberSearch";
            this.tbNumberSearch.Size = new System.Drawing.Size(221, 26);
            this.tbNumberSearch.TabIndex = 1;
            this.tbNumberSearch.TextChanged += new System.EventHandler(this.tbNumberSearch_TextChanged);
            // 
            // gbNameSearch
            // 
            this.gbNameSearch.Controls.Add(this.tbNameSearch);
            this.gbNameSearch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbNameSearch.Location = new System.Drawing.Point(10, 7);
            this.gbNameSearch.Name = "gbNameSearch";
            this.gbNameSearch.Size = new System.Drawing.Size(261, 71);
            this.gbNameSearch.TabIndex = 14;
            this.gbNameSearch.TabStop = false;
            this.gbNameSearch.Text = "Введите имя";
            // 
            // tbNameSearch
            // 
            this.tbNameSearch.Location = new System.Drawing.Point(15, 29);
            this.tbNameSearch.Name = "tbNameSearch";
            this.tbNameSearch.Size = new System.Drawing.Size(221, 26);
            this.tbNameSearch.TabIndex = 1;
            this.tbNameSearch.TextChanged += new System.EventHandler(this.tbNameSearch_TextChanged);
            // 
            // btnClearFilter
            // 
            this.btnClearFilter.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnClearFilter.Location = new System.Drawing.Point(85, 552);
            this.btnClearFilter.Name = "btnClearFilter";
            this.btnClearFilter.Size = new System.Drawing.Size(107, 25);
            this.btnClearFilter.TabIndex = 10;
            this.btnClearFilter.Text = "Очистить всё";
            this.btnClearFilter.UseVisualStyleBackColor = true;
            this.btnClearFilter.Click += new System.EventHandler(this.btnClearFilter_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.statusStrip.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.statusStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblCount});
            this.statusStrip.Location = new System.Drawing.Point(0, 728);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(139, 22);
            this.statusStrip.TabIndex = 12;
            this.statusStrip.Text = "statusStrip";
            // 
            // lblCount
            // 
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(122, 17);
            this.lblCount.Text = "Количество записей:";
            // 
            // pnlFilterUsers
            // 
            this.pnlFilterUsers.BackColor = System.Drawing.Color.BurlyWood;
            this.pnlFilterUsers.Controls.Add(this.gbLogin);
            this.pnlFilterUsers.Controls.Add(this.gbNameUser);
            this.pnlFilterUsers.Controls.Add(this.btnResetAll);
            this.pnlFilterUsers.Location = new System.Drawing.Point(4, 27);
            this.pnlFilterUsers.Name = "pnlFilterUsers";
            this.pnlFilterUsers.Size = new System.Drawing.Size(277, 618);
            this.pnlFilterUsers.TabIndex = 13;
            this.pnlFilterUsers.Visible = false;
            // 
            // gbLogin
            // 
            this.gbLogin.Controls.Add(this.tbLogin);
            this.gbLogin.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbLogin.Location = new System.Drawing.Point(13, 90);
            this.gbLogin.Name = "gbLogin";
            this.gbLogin.Size = new System.Drawing.Size(251, 71);
            this.gbLogin.TabIndex = 15;
            this.gbLogin.TabStop = false;
            this.gbLogin.Text = "Введите логин";
            // 
            // tbLogin
            // 
            this.tbLogin.Location = new System.Drawing.Point(15, 29);
            this.tbLogin.Name = "tbLogin";
            this.tbLogin.Size = new System.Drawing.Size(221, 26);
            this.tbLogin.TabIndex = 1;
            this.tbLogin.TextChanged += new System.EventHandler(this.tbLogin_TextChanged);
            // 
            // gbNameUser
            // 
            this.gbNameUser.Controls.Add(this.tbNameUser);
            this.gbNameUser.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbNameUser.Location = new System.Drawing.Point(13, 7);
            this.gbNameUser.Name = "gbNameUser";
            this.gbNameUser.Size = new System.Drawing.Size(251, 71);
            this.gbNameUser.TabIndex = 14;
            this.gbNameUser.TabStop = false;
            this.gbNameUser.Text = "Введите имя";
            // 
            // tbNameUser
            // 
            this.tbNameUser.Location = new System.Drawing.Point(15, 29);
            this.tbNameUser.Name = "tbNameUser";
            this.tbNameUser.Size = new System.Drawing.Size(221, 26);
            this.tbNameUser.TabIndex = 1;
            this.tbNameUser.TextChanged += new System.EventHandler(this.tbNameUser_TextChanged);
            // 
            // btnResetAll
            // 
            this.btnResetAll.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnResetAll.Location = new System.Drawing.Point(85, 175);
            this.btnResetAll.Name = "btnResetAll";
            this.btnResetAll.Size = new System.Drawing.Size(107, 25);
            this.btnResetAll.TabIndex = 10;
            this.btnResetAll.Text = "Очистить всё";
            this.btnResetAll.UseVisualStyleBackColor = true;
            this.btnResetAll.Click += new System.EventHandler(this.btnResetAll_Click);
            // 
            // pnlFilterPayment
            // 
            this.pnlFilterPayment.BackColor = System.Drawing.Color.BurlyWood;
            this.pnlFilterPayment.Controls.Add(this.gbPayer);
            this.pnlFilterPayment.Controls.Add(this.groupBox1);
            this.pnlFilterPayment.Controls.Add(this.gbSumPay);
            this.pnlFilterPayment.Controls.Add(this.gbDatePay);
            this.pnlFilterPayment.Controls.Add(this.gbCheckNumber);
            this.pnlFilterPayment.Controls.Add(this.gbUsers);
            this.pnlFilterPayment.Controls.Add(this.btnClear);
            this.pnlFilterPayment.Location = new System.Drawing.Point(4, 27);
            this.pnlFilterPayment.Name = "pnlFilterPayment";
            this.pnlFilterPayment.Size = new System.Drawing.Size(277, 650);
            this.pnlFilterPayment.TabIndex = 16;
            this.pnlFilterPayment.Visible = false;
            // 
            // gbPayer
            // 
            this.gbPayer.Controls.Add(this.tbPayer);
            this.gbPayer.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbPayer.Location = new System.Drawing.Point(13, 162);
            this.gbPayer.Name = "gbPayer";
            this.gbPayer.Size = new System.Drawing.Size(251, 71);
            this.gbPayer.TabIndex = 20;
            this.gbPayer.TabStop = false;
            this.gbPayer.Text = "Введите имя плательщика";
            // 
            // tbPayer
            // 
            this.tbPayer.Location = new System.Drawing.Point(15, 29);
            this.tbPayer.Name = "tbPayer";
            this.tbPayer.Size = new System.Drawing.Size(221, 26);
            this.tbPayer.TabIndex = 1;
            this.tbPayer.TextChanged += new System.EventHandler(this.tbPayer_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbOperation);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.groupBox1.Location = new System.Drawing.Point(13, 527);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(251, 78);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Сортировать по типу операции";
            // 
            // cbOperation
            // 
            this.cbOperation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOperation.FormattingEnabled = true;
            this.cbOperation.Items.AddRange(new object[] {
            "Все операции",
            "Зачисление",
            "Возврат"});
            this.cbOperation.Location = new System.Drawing.Point(23, 32);
            this.cbOperation.Name = "cbOperation";
            this.cbOperation.Size = new System.Drawing.Size(201, 26);
            this.cbOperation.TabIndex = 12;
            this.cbOperation.SelectedIndexChanged += new System.EventHandler(this.cbOperation_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(73, 134);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 25);
            this.button1.TabIndex = 11;
            this.button1.Text = "Сброс";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // gbSumPay
            // 
            this.gbSumPay.Controls.Add(this.nudEnd);
            this.gbSumPay.Controls.Add(this.nudBegin);
            this.gbSumPay.Controls.Add(this.lblTo);
            this.gbSumPay.Controls.Add(this.lblFrom);
            this.gbSumPay.Controls.Add(this.btnDropSum);
            this.gbSumPay.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.gbSumPay.Location = new System.Drawing.Point(13, 380);
            this.gbSumPay.Name = "gbSumPay";
            this.gbSumPay.Size = new System.Drawing.Size(251, 138);
            this.gbSumPay.TabIndex = 18;
            this.gbSumPay.TabStop = false;
            this.gbSumPay.Text = "Сортировать по сумме оплаты";
            // 
            // nudEnd
            // 
            this.nudEnd.DecimalPlaces = 2;
            this.nudEnd.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudEnd.Location = new System.Drawing.Point(66, 62);
            this.nudEnd.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudEnd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudEnd.Name = "nudEnd";
            this.nudEnd.Size = new System.Drawing.Size(103, 26);
            this.nudEnd.TabIndex = 84;
            this.nudEnd.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudEnd.ValueChanged += new System.EventHandler(this.nudEnd_ValueChanged);
            // 
            // nudBegin
            // 
            this.nudBegin.DecimalPlaces = 2;
            this.nudBegin.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudBegin.Location = new System.Drawing.Point(66, 30);
            this.nudBegin.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudBegin.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudBegin.Name = "nudBegin";
            this.nudBegin.Size = new System.Drawing.Size(103, 26);
            this.nudBegin.TabIndex = 83;
            this.nudBegin.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudBegin.ValueChanged += new System.EventHandler(this.nudBegin_ValueChanged);
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTo.Location = new System.Drawing.Point(24, 66);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(28, 18);
            this.lblTo.TabIndex = 19;
            this.lblTo.Text = "до:";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFrom.Location = new System.Drawing.Point(26, 31);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(26, 18);
            this.lblFrom.TabIndex = 18;
            this.lblFrom.Text = "от:";
            // 
            // btnDropSum
            // 
            this.btnDropSum.Location = new System.Drawing.Point(73, 99);
            this.btnDropSum.Name = "btnDropSum";
            this.btnDropSum.Size = new System.Drawing.Size(91, 25);
            this.btnDropSum.TabIndex = 11;
            this.btnDropSum.Text = "Сброс";
            this.btnDropSum.UseVisualStyleBackColor = true;
            this.btnDropSum.Click += new System.EventHandler(this.btnDropSum_Click);
            // 
            // gbDatePay
            // 
            this.gbDatePay.Controls.Add(this.lblEnd);
            this.gbDatePay.Controls.Add(this.dtpBegin);
            this.gbDatePay.Controls.Add(this.dtpEnd);
            this.gbDatePay.Controls.Add(this.lblBegin);
            this.gbDatePay.Controls.Add(this.btnDroppDate);
            this.gbDatePay.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.gbDatePay.Location = new System.Drawing.Point(13, 236);
            this.gbDatePay.Name = "gbDatePay";
            this.gbDatePay.Size = new System.Drawing.Size(251, 138);
            this.gbDatePay.TabIndex = 17;
            this.gbDatePay.TabStop = false;
            this.gbDatePay.Text = "Сортировать по дате оплаты";
            // 
            // lblEnd
            // 
            this.lblEnd.AutoSize = true;
            this.lblEnd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblEnd.Location = new System.Drawing.Point(25, 68);
            this.lblEnd.Name = "lblEnd";
            this.lblEnd.Size = new System.Drawing.Size(28, 18);
            this.lblEnd.TabIndex = 19;
            this.lblEnd.Text = "по:";
            // 
            // dtpBegin
            // 
            this.dtpBegin.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpBegin.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpBegin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBegin.Location = new System.Drawing.Point(66, 33);
            this.dtpBegin.Name = "dtpBegin";
            this.dtpBegin.Size = new System.Drawing.Size(164, 23);
            this.dtpBegin.TabIndex = 20;
            this.dtpBegin.ValueChanged += new System.EventHandler(this.dtpBegin_ValueChanged);
            // 
            // dtpEnd
            // 
            this.dtpEnd.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpEnd.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEnd.Location = new System.Drawing.Point(66, 63);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(164, 23);
            this.dtpEnd.TabIndex = 21;
            this.dtpEnd.ValueChanged += new System.EventHandler(this.dtpEnd_ValueChanged);
            // 
            // lblBegin
            // 
            this.lblBegin.AutoSize = true;
            this.lblBegin.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblBegin.Location = new System.Drawing.Point(34, 35);
            this.lblBegin.Name = "lblBegin";
            this.lblBegin.Size = new System.Drawing.Size(18, 18);
            this.lblBegin.TabIndex = 18;
            this.lblBegin.Text = "с:";
            // 
            // btnDroppDate
            // 
            this.btnDroppDate.Location = new System.Drawing.Point(73, 96);
            this.btnDroppDate.Name = "btnDroppDate";
            this.btnDroppDate.Size = new System.Drawing.Size(91, 25);
            this.btnDroppDate.TabIndex = 11;
            this.btnDroppDate.Text = "Сброс";
            this.btnDroppDate.UseVisualStyleBackColor = true;
            this.btnDroppDate.Click += new System.EventHandler(this.btnDroppDate_Click);
            // 
            // gbCheckNumber
            // 
            this.gbCheckNumber.Controls.Add(this.tbCheckNumber);
            this.gbCheckNumber.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbCheckNumber.Location = new System.Drawing.Point(13, 84);
            this.gbCheckNumber.Name = "gbCheckNumber";
            this.gbCheckNumber.Size = new System.Drawing.Size(251, 71);
            this.gbCheckNumber.TabIndex = 15;
            this.gbCheckNumber.TabStop = false;
            this.gbCheckNumber.Text = "Введите номер счета";
            // 
            // tbCheckNumber
            // 
            this.tbCheckNumber.Location = new System.Drawing.Point(15, 29);
            this.tbCheckNumber.Name = "tbCheckNumber";
            this.tbCheckNumber.Size = new System.Drawing.Size(221, 26);
            this.tbCheckNumber.TabIndex = 1;
            this.tbCheckNumber.TextChanged += new System.EventHandler(this.tbCheckNumber_TextChanged);
            this.tbCheckNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCheckNumber_KeyPress);
            // 
            // gbUsers
            // 
            this.gbUsers.Controls.Add(this.tbUsers);
            this.gbUsers.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbUsers.Location = new System.Drawing.Point(13, 7);
            this.gbUsers.Name = "gbUsers";
            this.gbUsers.Size = new System.Drawing.Size(251, 71);
            this.gbUsers.TabIndex = 14;
            this.gbUsers.TabStop = false;
            this.gbUsers.Text = "Введите имя cотрудника";
            // 
            // tbUsers
            // 
            this.tbUsers.Location = new System.Drawing.Point(15, 29);
            this.tbUsers.Name = "tbUsers";
            this.tbUsers.Size = new System.Drawing.Size(221, 26);
            this.tbUsers.TabIndex = 0;
            this.tbUsers.TextChanged += new System.EventHandler(this.tbUsers_TextChanged);
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnClear.Location = new System.Drawing.Point(75, 613);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(107, 25);
            this.btnClear.TabIndex = 10;
            this.btnClear.Text = "Очистить всё";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // saveWordCheck
            // 
            this.saveWordCheck.Filter = "Документ Microsoft Word (*.docx)|*.docx";
            this.saveWordCheck.FileOk += new System.ComponentModel.CancelEventHandler(this.saveWordCheck_FileOk);
            // 
            // FormHotelAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.BurlyWood;
            this.ClientSize = new System.Drawing.Size(1073, 749);
            this.Controls.Add(this.pnlFilterPayment);
            this.Controls.Add(this.pnlFilterUsers);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.pnlFilterHotelStay);
            this.Controls.Add(this.pnlFilterBooking);
            this.Controls.Add(this.pnlFilterCustomer);
            this.Controls.Add(this.pnlFilterRoom);
            this.Controls.Add(this.dgvShowAccounting);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.menuHotel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuHotel;
            this.Name = "FormHotelAccount";
            this.Text = "Учет оплаты услуг гостиницы";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormHotelAccount_FormClosed);
            this.Load += new System.EventHandler(this.FormHotelAccount_Load);
            this.menuHotel.ResumeLayout(false);
            this.menuHotel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShowAccounting)).EndInit();
            this.pnlFilterRoom.ResumeLayout(false);
            this.gbFreeNumber.ResumeLayout(false);
            this.gbFreeNumber.PerformLayout();
            this.gbTypeComfort.ResumeLayout(false);
            this.gbTypeAccommodation.ResumeLayout(false);
            this.gbPrice.ResumeLayout(false);
            this.pnlFilterCustomer.ResumeLayout(false);
            this.gbFindName.ResumeLayout(false);
            this.gbFindName.PerformLayout();
            this.gbFindNumberPhone.ResumeLayout(false);
            this.gbFindNumberPhone.PerformLayout();
            this.pnlFilterBooking.ResumeLayout(false);
            this.gbNumber.ResumeLayout(false);
            this.gbNumber.PerformLayout();
            this.gbName.ResumeLayout(false);
            this.gbName.PerformLayout();
            this.gbPhone.ResumeLayout(false);
            this.gbPhone.PerformLayout();
            this.gbPeriod.ResumeLayout(false);
            this.gbPeriod.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.pnlFilterHotelStay.ResumeLayout(false);
            this.gbViewDoc.ResumeLayout(false);
            this.gbFilterDate.ResumeLayout(false);
            this.gbFilterDate.PerformLayout();
            this.gbNumberSearch.ResumeLayout(false);
            this.gbNumberSearch.PerformLayout();
            this.gbNameSearch.ResumeLayout(false);
            this.gbNameSearch.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.pnlFilterUsers.ResumeLayout(false);
            this.gbLogin.ResumeLayout(false);
            this.gbLogin.PerformLayout();
            this.gbNameUser.ResumeLayout(false);
            this.gbNameUser.PerformLayout();
            this.pnlFilterPayment.ResumeLayout(false);
            this.gbPayer.ResumeLayout(false);
            this.gbPayer.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.gbSumPay.ResumeLayout(false);
            this.gbSumPay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBegin)).EndInit();
            this.gbDatePay.ResumeLayout(false);
            this.gbDatePay.PerformLayout();
            this.gbCheckNumber.ResumeLayout(false);
            this.gbCheckNumber.PerformLayout();
            this.gbUsers.ResumeLayout(false);
            this.gbUsers.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuHotel;
        private System.Windows.Forms.ToolStripMenuItem miFile;
        private System.Windows.Forms.DataGridView dgvShowAccounting;
        private System.Windows.Forms.ToolStripMenuItem miEditing;
        private System.Windows.Forms.ToolStripMenuItem miEdit;
        private System.Windows.Forms.ToolStripMenuItem miAdd;
        private System.Windows.Forms.Panel pnlFilterRoom;
        private System.Windows.Forms.GroupBox gbPrice;
        private System.Windows.Forms.GroupBox gbTypeAccommodation;
        private System.Windows.Forms.Panel pnlFilterCustomer;
        private System.Windows.Forms.GroupBox gbFindName;
        private System.Windows.Forms.GroupBox gbFindNumberPhone;
        private System.Windows.Forms.TextBox tbNumberPhone;
        private System.Windows.Forms.GroupBox gbTypeComfort;
        private System.Windows.Forms.ToolStripMenuItem miExport;
        private System.Windows.Forms.SaveFileDialog saveExcel;
        private System.Windows.Forms.Button btnClearFilterRoom;
        private System.Windows.Forms.Button btnClearFilterCustomer;
        private System.Windows.Forms.ToolStripMenuItem miExcel;
        private System.Windows.Forms.ToolStripMenuItem miWord;
        private System.Windows.Forms.SaveFileDialog saveWord;
        private System.Windows.Forms.ToolStripMenuItem miReport;
        private System.Windows.Forms.ToolStripMenuItem miDelete;
        private System.Windows.Forms.TextBox tbFullName;
        private System.Windows.Forms.Panel pnlFilterBooking;
        private System.Windows.Forms.Button btnClearFilterBooking;
        private System.Windows.Forms.GroupBox gbPeriod;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label lblSettlementEnd;
        private System.Windows.Forms.DateTimePicker dtpSettlementBegin;
        private System.Windows.Forms.DateTimePicker dtpSettlementEnd;
        private System.Windows.Forms.Label lblSettlemet;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel pnlFilterHotelStay;
        private System.Windows.Forms.Button btnClearFilter;
        private System.Windows.Forms.CheckedListBox chlbTypeComfort;
        private System.Windows.Forms.CheckedListBox chlbTypeAccommodation;
        private System.Windows.Forms.ComboBox cbPrice;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lblCount;
        private System.Windows.Forms.GroupBox gbNumber;
        private System.Windows.Forms.TextBox tbNumber;
        private System.Windows.Forms.GroupBox gbName;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.GroupBox gbPhone;
        private System.Windows.Forms.TextBox tbPhone;
        private System.Windows.Forms.Label lblEvictionEnd;
        private System.Windows.Forms.DateTimePicker dtpEvictionBegin;
        private System.Windows.Forms.DateTimePicker dtpEvictionEnd;
        private System.Windows.Forms.Label lblEviction;
        private System.Windows.Forms.RadioButton rbEviction;
        private System.Windows.Forms.RadioButton rbSettlement;
        private System.Windows.Forms.GroupBox gbNumberSearch;
        private System.Windows.Forms.TextBox tbNumberSearch;
        private System.Windows.Forms.GroupBox gbNameSearch;
        private System.Windows.Forms.TextBox tbNameSearch;
        private System.Windows.Forms.GroupBox gbFilterDate;
        private System.Windows.Forms.RadioButton rbHotelStayNow;
        private System.Windows.Forms.Button btnResetDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpEvictionStart;
        private System.Windows.Forms.DateTimePicker dtpEvictionFinish;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rbEvictionDate;
        private System.Windows.Forms.RadioButton rbSettlementDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpSettlementStart;
        private System.Windows.Forms.DateTimePicker dtpSettlementFinish;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel pnlFilterUsers;
        private System.Windows.Forms.GroupBox gbNameUser;
        private System.Windows.Forms.TextBox tbNameUser;
        private System.Windows.Forms.Button btnResetAll;
        private System.Windows.Forms.GroupBox gbLogin;
        private System.Windows.Forms.TextBox tbLogin;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.ToolStripMenuItem miOpen;
        private System.Windows.Forms.ToolStripMenuItem miCustomers;
        private System.Windows.Forms.ToolStripMenuItem miRooms;
        private System.Windows.Forms.ToolStripMenuItem miBooking;
        private System.Windows.Forms.ToolStripMenuItem miHotelStay;
        private System.Windows.Forms.ToolStripMenuItem miUsers;
        private System.Windows.Forms.ToolStripMenuItem miPayment;
        private System.Windows.Forms.Panel pnlFilterPayment;
        private System.Windows.Forms.GroupBox gbCheckNumber;
        private System.Windows.Forms.TextBox tbCheckNumber;
        private System.Windows.Forms.GroupBox gbUsers;
        private System.Windows.Forms.TextBox tbUsers;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.GroupBox gbSumPay;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Button btnDropSum;
        private System.Windows.Forms.GroupBox gbDatePay;
        private System.Windows.Forms.Label lblEnd;
        private System.Windows.Forms.DateTimePicker dtpBegin;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.Label lblBegin;
        private System.Windows.Forms.Button btnDroppDate;
        private System.Windows.Forms.NumericUpDown nudEnd;
        private System.Windows.Forms.NumericUpDown nudBegin;
        private System.Windows.Forms.ToolStripMenuItem miCheck;
        private System.Windows.Forms.SaveFileDialog saveWordCheck;
        private System.Windows.Forms.ToolStripMenuItem miReportProfit;
        private System.Windows.Forms.ToolStripMenuItem miReportCount;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbOperation;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox gbPayer;
        private System.Windows.Forms.TextBox tbPayer;
        private System.Windows.Forms.GroupBox gbFreeNumber;
        private System.Windows.Forms.CheckBox cbFreeNumber;
        private System.Windows.Forms.Label lblFinish;
        private System.Windows.Forms.DateTimePicker dtpStartNumber;
        private System.Windows.Forms.DateTimePicker dtpEndNumber;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.GroupBox gbViewDoc;
        private System.Windows.Forms.ComboBox cbViewDoc;
    }
}


﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using Word = Microsoft.Office.Interop.Word;

namespace HotelApp
{
    public partial class FormHotelAccount : Form
    {
        static dbHotelEntities db = new dbHotelEntities();
        private  string table;
        private bool privileged;
        private int userId = -1;

        public FormHotelAccount(bool privileged, int indexUser)
        {
            InitializeComponent();
            this.privileged = privileged;
            this.userId = indexUser;
            miHotelStay_Click(null, null);
        }

        private void FormHotelAccount_Load(object sender, EventArgs e)
        {
            if (!privileged)
            {
                miUsers.Visible = false;
            }
        }

        private void VisibleFilters(string pnlName)
        {
            pnlFilterCustomer.Visible = pnlFilterCustomer.Name == pnlName;
            pnlFilterRoom.Visible = pnlFilterRoom.Name == pnlName;
            pnlFilterBooking.Visible = pnlFilterBooking.Name == pnlName;
            pnlFilterHotelStay.Visible = pnlFilterHotelStay.Name == pnlName;
            pnlFilterUsers.Visible = pnlFilterUsers.Name == pnlName;
            pnlFilterPayment.Visible = pnlFilterPayment.Name == pnlName;
            miCheck.Visible = pnlFilterPayment.Visible;
        }

        private void miCustomers_Click(object sender, EventArgs e)
        {
            this.Text = "Учет оплаты услуг отеля. Данные о клиентах";
            miAdd.Text = "Зарегистрировать клиента";
            miEdit.Text = "Редактировать данные о клиенте";
            miDelete.Text = "Удалить данные о клиенте";
            table = "customers";
            miAdd.Visible = true;
            miEdit.Visible = true;
            if (!privileged)
            {
                miDelete.Visible = false;
            }
            VisibleFilters(pnlFilterCustomer.Name);
            btnClearFilterCustomer_Click(null, null);
            FilterCustomers();
        }

        private void miRooms_Click(object sender, EventArgs e)
        {
            this.Text = "Учет оплаты услуг отеля. Стоимость номеров";
            miAdd.Text = "Добавить номер";
            miEdit.Text = "Изменить информацию о номере";
            miDelete.Text = "Удалить информацию о номере";
            table = "rooms";
            if (!privileged)
            {
                miAdd.Visible = false;
                miEdit.Visible = false;
                miDelete.Visible = false;
            }
            VisibleFilters(pnlFilterRoom.Name);
            AddFilterRooms();
            btnClearFilterRoom_Click(null, null);
            FilterRooms();
        }

        private void AddFilterRooms()
        {
            cbPrice.SelectedIndex = 0;

            var accomodation = db.TypeAccommodationTables.Select(n => n.Accommodation).ToList();
            accomodation.Insert(0, "Выбрать все");
            chlbTypeAccommodation.Items.Clear();
            chlbTypeAccommodation.Items.AddRange(accomodation.ToArray());

            var comfort = db.TypeComfortTables.Select(n => n.Comfort).ToList();
            comfort.Insert(0, "Выбрать все");
            chlbTypeComfort.Items.Clear();
            chlbTypeComfort.Items.AddRange(comfort.ToArray());
        }

        private void miBooking_Click(object sender, EventArgs e)
        {
            this.Text = "Учет оплаты услуг отеля. Учет бронирования";
            miAdd.Text = "Добавить бронь";
            miEdit.Text = "Изменить информацию о бронировании";
            miDelete.Text = "Отменить бронь";
            table = "booking";
            miAdd.Visible = true;
            miEdit.Visible = true;
            miDelete.Visible = true;
            VisibleFilters(pnlFilterBooking.Name);
            btnClearFilterBooking_Click(null,null);
            FilterBooking();
        }

        private void miHotelStay_Click(object sender, EventArgs e)
        {
            this.Text = "Учет оплаты услуг отеля. Проживание";
            miAdd.Text = "Добавить информацию о проживании";
            miEdit.Text = "Изменить информацию о проживании";
            miDelete.Text = "Удалить информацию о проживании";
            table = "hotelStay";
            miAdd.Visible = true;
            miEdit.Visible = true;
            if (!privileged)
            {
                miDelete.Visible = false;
            }
            VisibleFilters(pnlFilterHotelStay.Name);
            btnClearFilter_Click(null, null);
            FilterHotelStay();
        }

        private void miUsers_Click(object sender, EventArgs e)
        {
            this.Text = "Учет оплаты услуг отеля. Пользователи";
            miAdd.Text = "Зарегистрировать пользователя";
            miEdit.Text = "Редактировать данные о пользователе";
            miDelete.Text = "Удалить данные о пользователе";
            table = "users";
            VisibleFilters(pnlFilterUsers.Name);
            btnResetAll_Click(null, null);
            FilterUsers();
        }

        private void miPayment_Click(object sender, EventArgs e)
        {
            this.Text = "Учет оплаты услуг отеля. Оплата";
            miAdd.Text = "Добавить данные об оплате";
            miEdit.Text = "Редактировать данные об оплате";
            miDelete.Text = "Удалить данные об оплате";
            table = "payment";
            miAdd.Visible = true;
            miEdit.Visible = true;
            if (!privileged)
            {
                miDelete.Visible = false;
            }
            VisibleFilters(pnlFilterPayment.Name);
            btnClear_Click(null, null);
            FilterPayment();
        }

        private void FilterRooms()
        {
            var search = db.RoomTables
                .Select(n => new
                {
                    n.RoomId,
                    n.RoomNumber,
                    n.TypeAccommodationTable.Accommodation,
                    n.TypeComfortTable.Comfort,
                    n.PriceperDay
                });

            if (cbFreeNumber.Checked)
            {
                DateTime dtStart = dtpStartNumber.Value.AddHours(1);
                DateTime dtEnd = dtpEndNumber.Value.AddHours(1);

                search = search.Where(n => !db.HotelStayTables
                .Where(x => x.CheckOutDate == null && x.SettlementTable.PlannedСheckOutDate > dtStart)
                .Any(x => x.SettlementTable.RoomTable.RoomNumber == n.RoomNumber));

                search = search.Where(n => !db.BookingTables
                .Where(x => x.CheckInDate > DateTime.Now && !(x.CheckOutDate < dtStart || x.CheckInDate > dtEnd))
                .Any(x => x.RoomTable.RoomNumber == n.RoomNumber));
            }

            List<string> list = new List<string>();

            if (chlbTypeAccommodation.CheckedItems.Count != 0)
            {
                for (int i = 0; i < chlbTypeAccommodation.Items.Count; i++)
                {
                    if (chlbTypeAccommodation.GetItemCheckState(i) == CheckState.Checked)
                    {
                        list.Add(chlbTypeAccommodation.Items[i].ToString());
                    }
                }

                search = search
               .Where(n => list.Any(m => m.ToString() == n.Accommodation));
            }

            List<string> listComfort = new List<string>();

            if (chlbTypeComfort.CheckedItems.Count != 0)
            {
                for (int i = 0; i < chlbTypeComfort.Items.Count; i++)
                {
                    if (chlbTypeComfort.GetItemCheckState(i) == CheckState.Checked)
                    {
                        listComfort.Add(chlbTypeComfort.Items[i].ToString());
                    }
                }

                search = search
                       .Where(n => listComfort.Any(m => m.ToString() == n.Comfort));
            }

            if (cbPrice.SelectedIndex == 1)
            {
                search = search
                       .OrderBy(n => n.PriceperDay);
            } 
            else if(cbPrice.SelectedIndex == 2)
            {
                search = search
                       .OrderByDescending(n => n.PriceperDay);
            }

            dgvShowAccounting.DataSource = search.ToList();
            InterfaceDataGridViewForRoom();
        }

        public void InterfaceDataGridViewForRoom()
        {
            dgvShowAccounting.Columns[0].Visible = false;
            dgvShowAccounting.Columns[1].HeaderText = "Номер";
            dgvShowAccounting.Columns[2].HeaderText = "Тип размещения";
            dgvShowAccounting.Columns[3].HeaderText = "Комфорт";
            dgvShowAccounting.Columns[4].HeaderText = "Цена в рублях";
            dgvShowAccounting.Columns[4].DefaultCellStyle.Format = "F2";
            dgvShowAccounting.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void chlbTypeAccommodation_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (e.Index == 0)
            {
                bool flag = e.NewValue == CheckState.Checked;
                for (int i = 1; i < chlbTypeAccommodation.Items.Count; i++)
                {
                    chlbTypeAccommodation.SetItemChecked(i, flag);
                }
            }
        }

        private void chlbTypeComfort_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (e.Index == 0)
            {
                bool flag = e.NewValue == CheckState.Checked;
                for (int i = 1; i < chlbTypeComfort.Items.Count; i++)
                {
                    chlbTypeComfort.SetItemChecked(i, flag);
                }
            }
        }

        private void cbPrice_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterRooms();
        }

        private void chlbTypeAccommodation_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterRooms();
        }

        private void chlbTypeComfort_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterRooms();
        }

        private void cbFreeNumber_CheckedChanged(object sender, EventArgs e)
        {
            dtpStartNumber.Enabled = cbFreeNumber.Checked;
            dtpEndNumber.Enabled = cbFreeNumber.Checked;
            FilterRooms();
        }

        private void dtpStartNumber_ValueChanged(object sender, EventArgs e)
        {
            dtpEndNumber.MinDate = dtpStartNumber.Value.AddDays(1);
            FilterRooms();
        }

        private void dtpEndNumber_ValueChanged(object sender, EventArgs e)
        {
            FilterRooms();
        }

        private void btnClearFilterRoom_Click(object sender, EventArgs e)
        {
            cbPrice.SelectedIndex = 0;
            cbFreeNumber.Checked = false;
            dtpStartNumber.MinDate = DateTime.Now;

            for (int i = 0; i < chlbTypeAccommodation.Items.Count; i++)
            {
                chlbTypeAccommodation.SetItemChecked(i, false);
            }
            chlbTypeAccommodation.SelectedIndex = -1;

            for (int i = 0; i < chlbTypeComfort.Items.Count; i++)
            {
                chlbTypeComfort.SetItemChecked(i, false);
            }
            chlbTypeComfort.SelectedIndex = -1;
        }

        private void FilterCustomers()
        {
            var search = db.CustomerTables
                .Select(n => new
                {
                    n.CustomerId,
                    n.Surname,
                    n.Name,
                    n.Middle,
                    n.Gender,
                    n.DateOfBirthday,
                    n.PlaceBirth,
                    n.RegistrationAddress,
                    n.ResidentialAddress,
                    n.ContactNumber,
                    n.Сitizenship,
                    n.DocumentType,
                    n.Series,
                    n.Number,
                    n.IssuedBy,
                    n.DateOfIssue
                });

            search = search
           .Where(n => n.ContactNumber.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").StartsWith(tbNumberPhone.Text)
           || n.ContactNumber.Replace("+7", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").StartsWith(tbNumberPhone.Text)
           || n.ContactNumber.Replace("+7", "8").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").StartsWith(tbNumberPhone.Text)
           || n.ContactNumber.Remove(0,1).Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").StartsWith(tbNumberPhone.Text)
           || n.ContactNumber.Remove(0, 1).Insert(0, "+7").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").StartsWith(tbNumberPhone.Text));

            search = search.Where(n => String.Concat(n.Surname, " ", n.Name, " ", n.Middle).Contains(tbFullName.Text));

            dgvShowAccounting.Columns.Clear();
            dgvShowAccounting.DataSource = search.OrderByDescending(n => n.CustomerId).ToList();
            InterfaceDataGridViewForCustomer();
        }

        public void InterfaceDataGridViewForCustomer()
        {
            dgvShowAccounting.Columns[0].Visible = false;
            dgvShowAccounting.Columns[1].HeaderText = "Фамилия";
            dgvShowAccounting.Columns[2].HeaderText = "Имя";
            dgvShowAccounting.Columns[3].HeaderText = "Отчество";
            dgvShowAccounting.Columns[4].HeaderText = "Пол";
            dgvShowAccounting.Columns[5].HeaderText = "Дата рождения";
            dgvShowAccounting.Columns[5].DefaultCellStyle.Format = "dd.MM.yyyy";
            dgvShowAccounting.Columns[6].HeaderText = "Место рождения";
            dgvShowAccounting.Columns[7].HeaderText = "Адрес регистрации";
            dgvShowAccounting.Columns[8].HeaderText = "Адрес проживания";
            dgvShowAccounting.Columns[9].HeaderText = "Телефон";
            dgvShowAccounting.Columns[10].HeaderText = "Гражданство";
            dgvShowAccounting.Columns[11].HeaderText = "Тип документа";
            dgvShowAccounting.Columns[12].HeaderText = "Серия";
            dgvShowAccounting.Columns[13].HeaderText = "Номер";
            dgvShowAccounting.Columns[14].HeaderText = "Кем выдан";
            dgvShowAccounting.Columns[15].HeaderText = "Дата выдачи";
            dgvShowAccounting.Columns[15].DefaultCellStyle.Format = "dd.MM.yyyy";
            dgvShowAccounting.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        }

        private void tbNumberPhone_TextChanged(object sender, EventArgs e)
        {
            FilterCustomers();
        }

        private void tbName_TextChanged(object sender, EventArgs e)
        {
            FilterCustomers();
        }

        private void btnClearFilterCustomer_Click(object sender, EventArgs e)
        {
            tbFullName.Clear();
            tbNumberPhone.Clear();
        }

        private void FilterBooking()
        {
            var search = db.BookingTables
                .Select(n => new
                {
                    n.BookingId,
                    n.RoomTable.RoomNumber,
                    n.Name,
                    n.NumberPhone,
                    n.CheckInDate,
                    n.CheckOutDate
                });

            search = search
           .Where(n => n.NumberPhone.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").StartsWith(tbPhone.Text)
           || n.NumberPhone.Replace("+7", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").StartsWith(tbPhone.Text)
           || n.NumberPhone.Replace("+7", "8").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").StartsWith(tbPhone.Text)
           || n.NumberPhone.Remove(0, 1).Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").StartsWith(tbPhone.Text)
           || n.NumberPhone.Remove(0, 1).Insert(0, "+7").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").StartsWith(tbPhone.Text));

            search = search.Where(n => n.Name.Contains(tbName.Text));

            search = search.Where(n => n.RoomNumber.Contains(tbNumber.Text));

            if (rbSettlement.Checked)
            {
                search = search.Where(n => n.CheckInDate >= dtpSettlementBegin.Value 
                && n.CheckInDate <= dtpSettlementEnd.Value).OrderBy(n => n.CheckInDate);
            }
            else
            {
                search = search.Where(n => n.CheckOutDate >= dtpEvictionBegin.Value 
                && n.CheckOutDate <= dtpEvictionEnd.Value).OrderBy(n => n.CheckOutDate);
            }

            dgvShowAccounting.Columns.Clear();
            dgvShowAccounting.DataSource = search.ToList();
            InterfaceDataGridViewForBooking();
        }

        public void InterfaceDataGridViewForBooking()
        {
            dgvShowAccounting.Columns[0].Visible = false;
            dgvShowAccounting.Columns[1].HeaderText = "Номер";
            dgvShowAccounting.Columns[2].HeaderText = "Имя";
            dgvShowAccounting.Columns[3].HeaderText = "Телефон";
            dgvShowAccounting.Columns[4].HeaderText = "Дата заселения";
            dgvShowAccounting.Columns[4].DefaultCellStyle.Format = "dd MMMM yyyy г. HH:mm";
            dgvShowAccounting.Columns[5].HeaderText = "Дата выселения";
            dgvShowAccounting.Columns[5].DefaultCellStyle.Format = "dd MMMM yyyy г. HH:mm";
            dgvShowAccounting.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void tbPhone_TextChanged(object sender, EventArgs e)
        {
            FilterBooking();
        }

        private void tbName_TextChanged_1(object sender, EventArgs e)
        {
            FilterBooking();
        }

        private void tbNumber_TextChanged(object sender, EventArgs e)
        {
            FilterBooking();
        }

        private void dtpEnabled(bool flag)
        {
            dtpEvictionBegin.Enabled = !flag;
            dtpEvictionEnd.Enabled = !flag;
            dtpSettlementBegin.Enabled = flag;
            dtpSettlementEnd.Enabled = flag;
        }

        private void rbSettlement_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSettlement.Checked)
            {
                dtpEnabled(true);
                btnReset_Click(null,null);
                FilterBooking();
            }
        }

        private void rbEviction_CheckedChanged(object sender, EventArgs e)
        {
            if (rbEviction.Checked)
            {
                dtpEnabled(false);
                btnReset_Click(null, null);
                FilterBooking();
            }
        }

        private void dtpSettlementBegin_ValueChanged(object sender, EventArgs e)
        {
            FilterBooking();
            dtpSettlementEnd.MinDate = dtpSettlementBegin.Value;
        }

        private void dtpSettlementEnd_ValueChanged(object sender, EventArgs e)
        {
            FilterBooking();
        }

        private void dtpEvictionBegin_ValueChanged(object sender, EventArgs e)
        {
            FilterBooking();
            dtpEvictionEnd.MinDate = dtpEvictionBegin.Value;
        }

        private void dtpEvictionEnd_ValueChanged(object sender, EventArgs e)
        {
            FilterBooking();
        } 

        private void btnReset_Click(object sender, EventArgs e)
        {
            dtpSettlementBegin.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 00, 00);
            dtpSettlementEnd.Value = new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(1).Month, DateTime.Now.AddMonths(1).Day, DateTime.Now.Hour, 00, 00);
            dtpEvictionBegin.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 00, 00);
            dtpEvictionEnd.Value = new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(1).Month, DateTime.Now.AddMonths(1).Day, DateTime.Now.Hour, 00, 00);
        }

        private void btnClearFilterBooking_Click(object sender, EventArgs e)
        {
            tbPhone.Clear();
            tbName.Clear();
            tbNumber.Clear();
            rbSettlement.Checked = true;
            btnReset_Click(null, null);
        }

        private void FilterHotelStay()
        {
            var search = db.HotelStayTables
                .Select(n => new
                {
                    n.HotelStayId,
                    n.SettlementTable.RoomTable.RoomNumber,
                    n.CustomerTable.Surname,
                    n.CustomerTable.Name,
                    n.CustomerTable.Middle,
                    n.CustomerTable.DocumentType,
                    n.SettlementDate,
                    n.SettlementTable.PlannedСheckOutDate,
                    n.CheckOutDate
                });

            if (cbViewDoc.SelectedIndex == 1)
            {
                search = search.Where(n => n.DocumentType == cbViewDoc.Text);
            }

            search = search.Where(n => String.Concat(n.Surname, " ", n.Name, " ", n.Middle)
            .Contains(tbNameSearch.Text));

            if (!String.IsNullOrEmpty(tbNumberSearch.Text))
            {
                search = search.Where(n => n.RoomNumber == tbNumberSearch.Text);
            }

            if (rbHotelStayNow.Checked)
            {
                search = search.Where(n => n.CheckOutDate.ToString() == "")
                    .OrderBy(n => n.PlannedСheckOutDate);
            }
            else if(rbSettlementDate.Checked)
            {
                search = search.Where(n => n.SettlementDate >= dtpSettlementStart.Value
                && n.SettlementDate <= dtpSettlementFinish.Value).OrderByDescending(n => n.SettlementDate);
            } 
            else if(rbEvictionDate.Checked)
            {
                search = search.Where(n => n.CheckOutDate >= dtpEvictionStart.Value
                && n.CheckOutDate <= dtpEvictionFinish.Value).OrderByDescending(n => n.CheckOutDate);
            }

            dgvShowAccounting.Columns.Clear();
            dgvShowAccounting.DataSource = search.Select(n => new {
                n.HotelStayId,
                n.RoomNumber,
                n.Surname,
                n.Name,
                n.Middle,
                n.SettlementDate,
                n.PlannedСheckOutDate,
                date = n.CheckOutDate.ToString()
            }).ToList();
            InterfaceForHotelStay();
        }

        public void InterfaceForHotelStay()
        {
            dgvShowAccounting.Columns[0].Visible = false;
            dgvShowAccounting.Columns[1].HeaderText = "Номер";
            dgvShowAccounting.Columns[2].HeaderText = "Фамилия";
            dgvShowAccounting.Columns[3].HeaderText = "Имя";
            dgvShowAccounting.Columns[4].HeaderText = "Отчество";
            dgvShowAccounting.Columns[5].HeaderText = "Дата заселения";
            dgvShowAccounting.Columns[5].DefaultCellStyle.Format = "dd MMMM yyyy г. HH:mm";
            dgvShowAccounting.Columns[6].HeaderText = "Планируемая дата выселения";
            dgvShowAccounting.Columns[6].DefaultCellStyle.Format = "dd MMMM yyyy г. HH:mm";
            dgvShowAccounting.Columns[7].HeaderText = "Дата выселения";
            dgvShowAccounting.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void tbNameSearch_TextChanged(object sender, EventArgs e)
        {
            FilterHotelStay();
        }

        private void tbNumberSearch_TextChanged(object sender, EventArgs e)
        {
            FilterHotelStay();
        }

        private void cbViewDoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterHotelStay();
        }

        private void dtpEnabledDate(bool flagFirst, bool flagSecond)
        {
            dtpEvictionStart.Enabled = flagFirst;
            dtpEvictionFinish.Enabled = flagFirst;
            dtpSettlementStart.Enabled = flagSecond;
            dtpSettlementFinish.Enabled = flagSecond;
        }

        private void rbHotelStayNow_CheckedChanged(object sender, EventArgs e)
        {
            if (rbHotelStayNow.Checked)
            {
                dtpEnabledDate(false, false);
                btnResetDate_Click(null, null);
                FilterHotelStay();
            }
        }

        private void rbSettlementDate_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSettlementDate.Checked)
            {
                dtpEnabledDate(false,true);
                btnResetDate_Click(null, null);
                FilterHotelStay();
            }
        }

        private void rbEvictionDate_CheckedChanged(object sender, EventArgs e)
        {
            if (rbEvictionDate.Checked)
            {
                dtpEnabledDate(true,false);
                btnResetDate_Click(null, null);
                FilterHotelStay();
            }
        }

        private void dtpSettlementStart_ValueChanged(object sender, EventArgs e)
        {
            FilterHotelStay();
            dtpSettlementFinish.MinDate = dtpSettlementStart.Value;
        }

        private void dtpSettlementFinish_ValueChanged(object sender, EventArgs e)
        {
            FilterHotelStay();
        }

        private void dtpEvictionStart_ValueChanged(object sender, EventArgs e)
        {
            FilterHotelStay();
            dtpEvictionFinish.MinDate = dtpEvictionStart.Value;
        }

        private void dtpEvictionFinish_ValueChanged(object sender, EventArgs e)
        {
            FilterHotelStay();
        }

        private void btnResetDate_Click(object sender, EventArgs e)
        {
            dtpSettlementStart.Value = DateTime.Now.AddMonths(-1);
            dtpSettlementFinish.Value = DateTime.Now;
            dtpEvictionStart.Value = DateTime.Now.AddMonths(-1);
            dtpEvictionFinish.Value = DateTime.Now;
        }

        private void btnClearFilter_Click(object sender, EventArgs e)
        {
            tbNameSearch.Clear();
            tbNumberSearch.Clear();
            rbHotelStayNow.Checked = true;
            btnResetDate_Click(null, null);
            cbViewDoc.SelectedIndex = 0;
        }

        private void FilterUsers()
        {
            var search = db.UserTables
                .Select(n => new
                {
                    n.UserId,
                    n.Login,
                    n.Password,
                    n.Surname,
                    n.Name,
                    n.Middle,
                    n.RoleTable.Role
                });

            search = search.Where(n => String.Concat(n.Surname, " ", n.Name, " ", n.Middle)
            .Contains(tbNameUser.Text));

            search = search.Where(n => n.Login.Contains(tbLogin.Text));

            dgvShowAccounting.Columns.Clear();
            dgvShowAccounting.DataSource = search.ToList();
            InterfaceForUsers();
        }

        public void InterfaceForUsers()
        {
            dgvShowAccounting.Columns[0].Visible = false;
            dgvShowAccounting.Columns[1].HeaderText = "Логин";
            dgvShowAccounting.Columns[2].HeaderText = "Пароль";
            dgvShowAccounting.Columns[3].HeaderText = "Фамилия";
            dgvShowAccounting.Columns[4].HeaderText = "Имя";
            dgvShowAccounting.Columns[5].HeaderText = "Отчество";
            dgvShowAccounting.Columns[6].HeaderText = "Должность";
            dgvShowAccounting.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void tbNameUser_TextChanged(object sender, EventArgs e)
        {
            FilterUsers();
        }

        private void tbLogin_TextChanged(object sender, EventArgs e)
        {
            FilterUsers();
        }

        private void btnResetAll_Click(object sender, EventArgs e)
        {
            tbNameUser.Clear();
            tbLogin.Clear();
        }

        private void FilterPayment()
        {
            var search = db.PaymentTables
                .Select(n => new
                {
                    n.PaymentId,
                    n.CheckNumber,
                    n.HotelStayTable.SettlementTable.RoomTable.RoomNumber,
                    SettlementDate = db.HotelStayTables
                    .Where(x => x.SettlementId == n.HotelStayTable.SettlementId).Min(x => x.SettlementDate),
                    n.HotelStayTable.SettlementTable.PlannedСheckOutDate,
                    user = String.Concat(n.UserTable.Surname, " ", 
                    n.UserTable.Name, " ", n.UserTable.Middle),
                    payer = String.Concat(n.HotelStayTable.CustomerTable.Surname, " ",
                    n.HotelStayTable.CustomerTable.Name, " ", n.HotelStayTable.CustomerTable.Middle),
                    n.Sum,
                    n.DateTimePay
                });

            search = search.Where(n => n.user.Contains(tbUsers.Text));
            search = search.Where(n => n.payer.Contains(tbPayer.Text));

            if (cbOperation.SelectedIndex == 1)
            {
                search = search.Where(n => !n.Sum.ToString().Contains("-"));
            }
            else if(cbOperation.SelectedIndex == 2)
            {
                search = search.Where(n => n.Sum.ToString().Contains("-"));
            }

            search = search.Where(n => n.CheckNumber.ToString().Contains(tbCheckNumber.Text));

            search = search.Where(n => n.DateTimePay >= dtpBegin.Value
                && n.DateTimePay <= dtpEnd.Value).OrderByDescending(n => n.DateTimePay);

            search = search.Where(n => Math.Abs(n.Sum) >= nudBegin.Value && Math.Abs(n.Sum) <= nudEnd.Value);

            dgvShowAccounting.Columns.Clear();
            dgvShowAccounting.DataSource = search.ToList();
            InterfaceForPayment();
        }

        public void InterfaceForPayment()
        {
            dgvShowAccounting.Columns[0].Visible = false;
            dgvShowAccounting.Columns[1].HeaderText = "Номер счета";
            dgvShowAccounting.Columns[1].DefaultCellStyle.Format = "d";
            dgvShowAccounting.Columns[2].HeaderText = "Номер";
            dgvShowAccounting.Columns[3].HeaderText = "Дата заселения";
            dgvShowAccounting.Columns[4].HeaderText = "Дата выселения";
            dgvShowAccounting.Columns[5].HeaderText = "Сотрудник";
            dgvShowAccounting.Columns[6].HeaderText = "Плательщик";
            dgvShowAccounting.Columns[7].HeaderText = "Сумма";
            dgvShowAccounting.Columns[7].DefaultCellStyle.Format = "F2";
            dgvShowAccounting.Columns[8].HeaderText = "Дата и время оплаты";
            dgvShowAccounting.Columns[8].DefaultCellStyle.Format = "dd MMMM yyyy г. HH:mm";
            dgvShowAccounting.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void tbUsers_TextChanged(object sender, EventArgs e)
        {
            FilterPayment();
        }

        private void tbCheckNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(8))
            {
                e.Handled = true;
            }
        }

        private void tbCheckNumber_TextChanged(object sender, EventArgs e)
        {
            FilterPayment();
        }

        private void tbPayer_TextChanged(object sender, EventArgs e)
        {
            FilterPayment();
        }

        private void dtpBegin_ValueChanged(object sender, EventArgs e)
        {
            FilterPayment();
            dtpEnd.MinDate = dtpBegin.Value;
        }

        private void dtpEnd_ValueChanged(object sender, EventArgs e)
        {
            FilterPayment();
        }

        private void nudBegin_ValueChanged(object sender, EventArgs e)
        {
            FilterPayment();
            nudEnd.Minimum = nudBegin.Value;
        }

        private void nudEnd_ValueChanged(object sender, EventArgs e)
        {
            FilterPayment();
        }

        private void cbOperation_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterPayment();
        }

        private void btnDroppDate_Click(object sender, EventArgs e)
        {
            dtpBegin.Value = DateTime.Now.AddMonths(-1);
            dtpEnd.Value = DateTime.Now;
        }

        private void btnDropSum_Click(object sender, EventArgs e)
        {
            nudBegin.Value = db.PaymentTables.Min(n => Math.Abs(n.Sum));
            nudEnd.Value = db.PaymentTables.Max(n => n.Sum);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tbUsers.Text = "";
            tbCheckNumber.Text = "";
            btnDroppDate_Click(null, null);
            btnDropSum_Click(null, null);
            cbOperation.SelectedIndex = 0;
        }

        private void miAdd_Click(object sender, EventArgs e)
        {
            int idAdd = 0;
            if (table == "rooms")
            {
                FormRooms rooms = new FormRooms();
                rooms.Text = "Добавление номера";
                rooms.ShowDialog();
                if(rooms.DialogResult == DialogResult.OK)
                {
                    miRooms_Click(null, null);
                    idAdd =db.RoomTables.Max(n => n.RoomId);
                }
            }
            else if (table == "customers")
            {
                FormCustomers customers = new FormCustomers();
                customers.Text = "Регистрация клиента";
                customers.ShowDialog();
                if (customers.DialogResult == DialogResult.OK)
                {
                    miCustomers_Click(null, null);
                    idAdd = db.CustomerTables.Max(n => n.CustomerId);
                }
            }
            else if (table == "booking")
            {
                FormBooking booking = new FormBooking();
                booking.Text = "Добавление брони";
                booking.ShowDialog();
                if (booking.DialogResult == DialogResult.OK)
                {
                    miBooking_Click(null, null); 
                    idAdd = db.BookingTables.Max(n => n.BookingId);
                }
            }
            else if (table == "hotelStay")
            {
                FormHotelStay hotelStay = new FormHotelStay();
                hotelStay.Text = "Добавление информации о проживании";
                hotelStay.ShowDialog();
                if (hotelStay.DialogResult == DialogResult.OK)
                {
                    miHotelStay_Click(null, null);
                    idAdd = db.HotelStayTables.Max(n => n.HotelStayId);
                    if (MessageBox.Show("Внести информацию об оплате?", "Внимание!",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        FormPayment payment = new FormPayment();
                        payment.Text = "Добавление информации об оплате";
                        payment.userId = this.userId;
                        payment.indexHotelStay = db.HotelStayTables.Max(n => n.HotelStayId);
                        payment.ShowDialog();
                        if (payment.DialogResult == DialogResult.OK)
                        {
                            miPayment_Click(null, null); 
                            idAdd = db.PaymentTables.Max(n => n.PaymentId);
                        }
                    }
                }
            }
            else if (table == "users")
            {
                FormUsers users = new FormUsers();
                users.Text = "Регистрация пользователя";
                users.ShowDialog();
                if (users.DialogResult == DialogResult.OK)
                {
                    miUsers_Click(null, null);
                    idAdd = db.UserTables.Max(n => n.UserId);
                }
            }
            else if(table == "payment")
            {
                FormPayment payment = new FormPayment();
                payment.Text = "Добавление информации об оплате";
                payment.userId = this.userId;
                payment.ShowDialog(); 
                if (payment.DialogResult == DialogResult.OK)
                {
                    miPayment_Click(null, null);
                    idAdd = db.PaymentTables.Max(n => n.PaymentId);
                }
            }
            if (idAdd != 0)
            {
                for (int i = 0; i < dgvShowAccounting.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dgvShowAccounting[0, i].Value) == idAdd)
                    {
                        dgvShowAccounting.CurrentCell = dgvShowAccounting[1, i];
                    }
                }
            }
        }

        private void miEdit_Click(object sender, EventArgs e)
        {
            int currRow = dgvShowAccounting.CurrentRow.Index;
            if (table == "rooms")
            {
                FormRooms rooms = new FormRooms();
                rooms.Text = "Изменение информации о номере";
                rooms.index = int.Parse(dgvShowAccounting[0, dgvShowAccounting.CurrentRow.Index].Value.ToString());
                rooms.ShowDialog();
                miRooms_Click(null, null);
            }
            else if (table == "customers")
            {
                FormCustomers customers = new FormCustomers();
                customers.Text = "Редактирование данных о клиенте";
                customers.index = int.Parse(dgvShowAccounting[0, dgvShowAccounting.CurrentRow.Index].Value.ToString());
                customers.ShowDialog();
                if (customers.DialogResult == DialogResult.OK)
                {
                    miCustomers_Click(null, null);
                }
            }
            else if (table == "booking")
            {
                if (Convert.ToDateTime(dgvShowAccounting[4, dgvShowAccounting.CurrentRow.Index].Value.ToString()) > DateTime.Now.AddDays(-1))
                {
                    FormBooking booking = new FormBooking();
                    booking.Text = "Изменение информации о бронировании";
                    booking.index = int.Parse(dgvShowAccounting[0, dgvShowAccounting.CurrentRow.Index].Value.ToString());
                    booking.ShowDialog();
                    if (booking.DialogResult == DialogResult.OK)
                    {
                        miBooking_Click(null, null);
                    };
                }
                else
                {
                    MessageBox.Show("Запись редактировать нельзя", "Внимание!",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else if (table == "hotelStay")
            {
                if (String.IsNullOrEmpty(dgvShowAccounting[7, dgvShowAccounting.CurrentRow.Index].Value.ToString()))
                {
                    FormHotelStay hotelStay = new FormHotelStay();
                    hotelStay.Text = "Изменение информации о проживании";
                    int idxHotelStay = int.Parse(dgvShowAccounting[0, dgvShowAccounting.CurrentRow.Index].Value.ToString());
                    hotelStay.index = idxHotelStay;
                    hotelStay.ShowDialog();
                    if (hotelStay.DialogResult == DialogResult.OK)
                    {
                        miHotelStay_Click(null, null);
                        db.SaveChanges();
                        if (MessageBox.Show("Внести информацию об оплате?", "Внимание!",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            FormPayment payment = new FormPayment();
                            payment.Text = "Добавление информации об оплате";
                            payment.userId = this.userId;
                            payment.indexHotelStay = idxHotelStay;
                            payment.ShowDialog();
                            if (payment.DialogResult == DialogResult.OK)
                            {
                                miPayment_Click(null, null);
                            }
                        }
                    };
                }
                else
                {
                    MessageBox.Show("Запись редактировать нельзя", "Внимание!",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else if (table == "users")
            {
                FormUsers users = new FormUsers();
                users.Text = "Редактирование данных о пользователе";
                users.index = int.Parse(dgvShowAccounting[0, dgvShowAccounting.CurrentRow.Index].Value.ToString());
                users.ShowDialog();
                miUsers_Click(null, null);
            }
            else if (table == "payment")
            {
                int indexPay = int.Parse(dgvShowAccounting[0, dgvShowAccounting.CurrentRow.Index].Value.ToString());
                DateTime dt = DateTime.Now.AddHours(-1);
                if (db.HotelStayTables.Where(n => n.SettlementId ==
                db.PaymentTables.FirstOrDefault(x => x.PaymentId == indexPay).HotelStayTable.SettlementId
                && (n.CheckOutDate == null || n.CheckOutDate > dt)).ToList().Count != 0)
                {
                    FormPayment payment = new FormPayment();
                    payment.Text = "Редактирование информации об оплате";
                    payment.userId = this.userId;
                    payment.index = indexPay;
                    payment.ShowDialog();
                    miPayment_Click(null, null);
                }
                else
                {
                    MessageBox.Show("Запись редактировать нельзя", "Внимание!",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            dgvShowAccounting.CurrentCell = dgvShowAccounting[1, currRow];
        }

        private void miDelete_Click(object sender, EventArgs e)
        {
            var index = int.Parse(dgvShowAccounting[0, dgvShowAccounting.CurrentRow.Index].Value.ToString());


            if (table == "rooms")
            {
                if (db.SettlementTables.FirstOrDefault(x => x.RoomId == index) != null)
                {
                    MessageBox.Show("Удаление записи невозможно.\r\nПо данной записи выполнено заселение.", "Внимание!",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                } 
                else if (db.BookingTables.FirstOrDefault(x => x.RoomId == index) != null)
                {
                    MessageBox.Show("Удаление записи невозможно.\r\nПо данной записи выполнено бронирование.", "Внимание!",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (MessageBox.Show("Вы уверены, что хотите Удалить запись?", "Внимание!",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var rec = db.RoomTables.FirstOrDefault(n => n.RoomId == index);
                        db.RoomTables.Remove(rec);
                        db.SaveChanges();
                        miRooms_Click(null, null);
                    }
                }
            }
            else if (table == "customers")
            {
                if (db.HotelStayTables.FirstOrDefault(x => x.CustomerId == index) != null)
                {
                    MessageBox.Show("Удаление записи невозможно.\r\nПо данной записи выполнено заселение.", "Внимание!",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (MessageBox.Show("Вы уверены, что хотите Удалить запись?", "Внимание!",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var rec = db.CustomerTables.FirstOrDefault(n => n.CustomerId == index);
                        db.CustomerTables.Remove(rec);
                        db.SaveChanges();
                        miCustomers_Click(null, null);
                    }
                }
            }
            else if (table == "booking")
            {
                if (db.SettlementTables.FirstOrDefault(x => x.BookingId == index) != null)
                {
                    MessageBox.Show("Удаление записи невозможно.\r\nПо данной записи выполнено заселение.", "Внимание!",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (MessageBox.Show("Вы уверены, что хотите Удалить запись?", "Внимание!", 
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var rec = db.BookingTables.FirstOrDefault(n => n.BookingId == index);
                        db.BookingTables.Remove(rec);
                        db.SaveChanges();
                        miBooking_Click(null, null);
                    }
                }
            }
            else if (table == "hotelStay")
            {
                if (db.PaymentTables.FirstOrDefault(x => x.HotelStayId == index) != null)
                {
                    MessageBox.Show("Удаление записи невозможно.\r\nПо данной записи выполнена оплата.", "Внимание!",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (MessageBox.Show("Вы уверены, что хотите Удалить запись?", "Внимание!",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var rec = db.HotelStayTables.FirstOrDefault(n => n.HotelStayId == index);
                        int settlement = db.HotelStayTables.FirstOrDefault(n => n.HotelStayId == index).SettlementId;
                        db.HotelStayTables.Remove(rec);
                        db.SaveChanges();
                        if (db.HotelStayTables.FirstOrDefault(n => n.SettlementId == settlement) == null)
                        {
                            var set = db.SettlementTables.FirstOrDefault(n => n.SettlementId == settlement);
                            db.SettlementTables.Remove(set);
                            db.SaveChanges();
                        }
                        miHotelStay_Click(null, null);
                    }
                }
            }
            else if (table == "users")
            {
                if (db.PaymentTables.FirstOrDefault(x => x.UserId == index) != null)
                {
                    MessageBox.Show("Удаление записи невозможно.\r\nПо данной записи выполнена оплата.", "Внимание!",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (MessageBox.Show("Вы уверены, что хотите Удалить запись?", "Внимание!",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        var rec = db.UserTables.FirstOrDefault(n => n.UserId == index);
                        db.UserTables.Remove(rec);
                        db.SaveChanges();
                        miUsers_Click(null, null);
                    }
                }
            }
            else if (table == "payment")
            {
                var rec = db.PaymentTables.FirstOrDefault(n => n.PaymentId == index);
                db.PaymentTables.Remove(rec);
                db.SaveChanges();
                miPayment_Click(null, null);
            }
        }

        private void miExcel_Click(object sender, EventArgs e)
        {
            saveExcel.FileName = this.Text + ". " + DateTime.Now.ToShortDateString();
            if (dgvShowAccounting.Rows.Count > 0)
            {
                saveExcel.ShowDialog();
            }
            else
            {
                MessageBox.Show("Нет данных для экспорта", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void saveExcel_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                Excel.Application exApp = new Excel.Application();
                exApp.Application.Visible = false;
                exApp.Workbooks.Add();
                Worksheet worksheet = (Worksheet)exApp.ActiveSheet;

                for (int j = 1; j < dgvShowAccounting.ColumnCount; j++)
                {
                    worksheet.Cells[1, j] = dgvShowAccounting.Columns[j].HeaderCell.Value.ToString();
                    worksheet.Cells[1, j].HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                }

                for (int i = 1; i <= dgvShowAccounting.RowCount; i++)
                {
                    for (int j = 1; j < dgvShowAccounting.ColumnCount; j++)
                    {
                        worksheet.Cells[i + 1, j] = dgvShowAccounting[j, i - 1].FormattedValue;
                    }
                }

                worksheet.Columns.AutoFit();
                worksheet.get_Range("A1", (char)(dgvShowAccounting.ColumnCount - 2 + 'A')  + "1")
                    .Font.Bold = true;
                var cells = worksheet.get_Range("A1", (char)(dgvShowAccounting.ColumnCount - 2 + 'A') 
                    + (dgvShowAccounting.RowCount + 1).ToString());
                cells.Borders[XlBordersIndex.xlInsideVertical].LineStyle = XlLineStyle.xlContinuous;
                cells.Borders[XlBordersIndex.xlInsideHorizontal].LineStyle = XlLineStyle.xlContinuous;
                cells.Borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;
                cells.Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;
                cells.Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
                cells.Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;

                worksheet.SaveAs(saveExcel.FileName);

                exApp.Quit();
            }
            catch
            {

            }
        }

        private void miWord_Click(object sender, EventArgs e)
        {
            if (dgvShowAccounting.Rows.Count > 0)
            {
                saveWord.FileName = this.Text + ". " + DateTime.Now.ToShortDateString();
                saveWord.ShowDialog();
            }
            else
            {
                MessageBox.Show("Нет данных для экспорта", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void saveWord_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                Word.Document Odoc = new Word.Document();
                Odoc.Application.Visible = false;

                object start = 0;
                object end = 0;
                Word.Range tableLocation = Odoc.Range(ref start, ref end);

                if (dgvShowAccounting.Columns.Count > 7)
                {
                    tableLocation.PageSetup.Orientation = Word.WdOrientation.wdOrientLandscape;
                }

                Odoc.Tables.Add(tableLocation, dgvShowAccounting.RowCount + 1, dgvShowAccounting.ColumnCount - 1);

                Odoc.Tables[1].Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;
                Odoc.Tables[1].Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;

                for (int j = 1; j < dgvShowAccounting.ColumnCount; j++)
                {
                    Odoc.Tables[1].Cell(1, j).Range.Text = dgvShowAccounting.Columns[j].HeaderCell.Value.ToString();
                }

                Odoc.Application.Selection.Tables[1].Rows[1].Range.Paragraphs.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                Odoc.Application.Selection.Tables[1].Rows[1].Range.Bold = 1;

                for (int i = 1; i <= dgvShowAccounting.RowCount; i++)
                {
                    for (int j = 1; j < dgvShowAccounting.ColumnCount; j++)
                    {
                        Odoc.Tables[1].Cell(i + 1, j).Range.Text = dgvShowAccounting[j, i - 1].FormattedValue.ToString();

                    }
                }

                if (dgvShowAccounting.Columns.Count > 7)
                {
                    Odoc.Application.Selection.Tables[1].Range.Font.Size = 7;
                }

                Odoc.Application.Selection.Tables[1].Columns.AutoFit();
                Odoc.SaveAs(saveWord.FileName);
                Odoc.Close();
            }
            catch
            {

            }
        }

        private void miCheck_Click(object sender, EventArgs e)
        {
            int indexPay = int.Parse(dgvShowAccounting[0, dgvShowAccounting.CurrentRow.Index].Value.ToString());
            DateTime dt = DateTime.Now.AddHours(-1);
            if (db.HotelStayTables.Where(n => n.SettlementId == 
            db.PaymentTables.FirstOrDefault(x => x.PaymentId == indexPay).HotelStayTable.SettlementId 
            && (n.CheckOutDate == null || n.CheckOutDate > dt)).ToList().Count != 0)
            {
                if (dgvShowAccounting.Rows.Count > 0)
                {
                    saveWordCheck.FileName = "Счет №" + (db.PaymentTables
                        .FirstOrDefault(n => n.PaymentId == indexPay).CheckNumber).ToString();
                    saveWordCheck.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Нет данных для экспорта", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Счет сформировать нельзя", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void saveWordCheck_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                int index = int.Parse(dgvShowAccounting[0, dgvShowAccounting.CurrentRow.Index].Value.ToString());
                var payment = db.PaymentTables.FirstOrDefault(n => n.PaymentId == index);

                Word.Document Odoc = new Word.Document();
                Odoc.Application.Visible = false;

                object start = 0;
                object end = 0;
                Word.Range range = Odoc.Range(ref start, ref end);
                range.InsertAfter(db.HotelInfoTables.First().NameHotel + "\r\n");
                range.InsertAfter(db.HotelInfoTables.First().IE + "\r\n");
                range.InsertAfter(db.HotelInfoTables.First().Address + "\r\n");
                range.InsertAfter("ИНН/INN: " + db.HotelInfoTables.First().INN + "\r\n");
                range.InsertAfter(db.HotelInfoTables.First().Bank + " БИК/BIK: "
                    + db.HotelInfoTables.First().BIK + "\r\n");
                range.InsertAfter("р/сч. " + db.HotelInfoTables.First().CheckingAccount + " к/сч. "
                    + db.HotelInfoTables.First().CorrespondentAccount + "\r\n");
                range.InsertAfter("тел./tel: " + db.HotelInfoTables.First().NumberPhone + ", факс/fax: "
                    + db.HotelInfoTables.First().Fax + "\r\n\r\n");

                var Paragraph2 = Odoc.Paragraphs.Add();
                var checkNumberLocation = Paragraph2.Range;
                Odoc.Tables.Add(checkNumberLocation, 1, 1);

                Odoc.Tables[1].Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;
                Odoc.Tables[1].Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;

                Odoc.Tables[1].Cell(1, 1).Range.Text = "СЧЁТ № " + payment.CheckNumber + "/ " + DateTime.Now.ToShortDateString();
                Odoc.Tables[1].Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;

                DateTime dateEnd = payment.HotelStayTable.CheckOutDate == null ? payment.HotelStayTable.SettlementTable.PlannedСheckOutDate
                        : Convert.ToDateTime(payment.HotelStayTable.CheckOutDate);

                var Paragraph3 = Odoc.Paragraphs.Add();
                var stayInfo = Paragraph3.Range;
                stayInfo.InsertAfter("Гость: " + String.Concat(payment.HotelStayTable.CustomerTable.Surname, " "
                    , payment.HotelStayTable.CustomerTable.Name, " ", payment.HotelStayTable.CustomerTable.Middle) + "\r\n");
                stayInfo.InsertAfter("Комната: " + payment.HotelStayTable.SettlementTable.RoomTable.RoomNumber + "\r\n");
                stayInfo.InsertAfter("Заезд: " + payment.HotelStayTable.SettlementDate.ToString("dd.MM.yyyy (HH:mm)")
                    + ", Выезд: " + dateEnd.ToString("dd.MM.yyyy (HH:mm)") + "\r\n\r\n");

                var Paragraph4 = Odoc.Paragraphs.Add();
                var tableRange = Paragraph4.Range;
                var pay = db.PaymentTables.Where(n => n.HotelStayId == payment.HotelStayId).ToList();
                Odoc.Tables.Add(tableRange, pay.Count + 2, 2);

                Odoc.Tables[2].Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;
                Odoc.Tables[2].Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;

                Odoc.Tables[2].Cell(1, 1).Range.Text = "Услуга: " + payment.HotelStayTable.SettlementTable.RoomTable.RoomNumber
                    + " номер с " + db.HotelStayTables.Where(n => n.HotelStayId == payment.HotelStayId)
                    .Min(n => n.SettlementDate).ToString("dd.MM.yyyy (HH:mm)") + " по "
                    + payment.HotelStayTable.SettlementTable.PlannedСheckOutDate.ToString("dd.MM.yyyy (HH:mm)");
                Odoc.Tables[2].Rows[1].Cells[1].Merge(Odoc.Tables[2].Rows[1].Cells[2]);


                Odoc.Tables[2].Cell(2, 1).Range.Text = "Дата операции";
                Odoc.Tables[2].Cell(2, 2).Range.Text = "Сумма операции";

                for (int i = 0; i < pay.Count; i++)
                {
                    Odoc.Tables[2].Cell(i + 3, 1).Range.Text = pay[i].DateTimePay.ToShortDateString();
                    Odoc.Tables[2].Cell(i + 3, 2).Range.Text = pay[i].Sum.ToString("F2");
                }

                Odoc.Tables[2].Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                Odoc.Tables[2].AutoFitBehavior(Word.WdAutoFitBehavior.wdAutoFitWindow);

                var Paragraph5 = Odoc.Paragraphs.Add();
                var signatureLocation = Paragraph5.Range;

                signatureLocation.InsertAfter("\r\n\r\nГость: " + String.Concat(payment.HotelStayTable.CustomerTable.Surname, " "
                    , payment.HotelStayTable.CustomerTable.Name, " ",
                    payment.HotelStayTable.CustomerTable.Middle, " ____________________") + "\r\n");
                signatureLocation.InsertAfter("Администратор: " + String.Concat(payment.UserTable.Surname, " "
                    , payment.HotelStayTable.CustomerTable.Name.Substring(0, 1), ".",
                    payment.HotelStayTable.CustomerTable.Middle.Substring(0, 1), ".", " ____________________") + "\r\n");

                signatureLocation.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;

                Odoc.Paragraphs.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceSingle;
                Odoc.Paragraphs.SpaceAfter = 0;
                signatureLocation.Paragraphs.SpaceAfter = 8;

                Odoc.SaveAs(saveWordCheck.FileName);
                Odoc.Close();
            }
            catch
            {

            }
        }

        private void miReportProfit_Click(object sender, EventArgs e)
        {
            FormReportProfit report = new FormReportProfit();
            report.ShowDialog();
        }

        private void miReportCount_Click(object sender, EventArgs e)
        {
            FormReportCount report = new FormReportCount();
            report.ShowDialog();
        }

        private void dgvShowAccounting_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (table == "hotelStay")
            {
                if (e.ColumnIndex == 7)
                {
                    if (!String.IsNullOrEmpty(e.Value.ToString()))
                    {
                        DateTime date = Convert.ToDateTime(e.Value);
                        e.Value = date.ToString("dd MMMM yyyy г. HH:mm");
                        e.FormattingApplied = true;
                    }
                }
            }
        }

        private void dgvShowAccounting_DoubleClick(object sender, EventArgs e)
        {
            if (table == "booking")
            {
                int bkIndex = Convert.ToInt32(dgvShowAccounting[0,
                    dgvShowAccounting.CurrentRow.Index].Value.ToString());
                var hotelBooking = db.HotelStayTables
                    .FirstOrDefault(n => n.SettlementTable.BookingId == bkIndex);
                if (hotelBooking != null)
                {
                    miHotelStay_Click(null, null);
                    rbSettlementDate.Checked = true;
                    if (hotelBooking.SettlementDate < DateTime.Now.AddMonths(-1))
                    {
                        dtpSettlementStart.Value = hotelBooking.SettlementDate;
                    }
                    for (int i = 0; i < dgvShowAccounting.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(dgvShowAccounting[0, i].Value) == hotelBooking.HotelStayId)
                        {
                            dgvShowAccounting.CurrentCell = dgvShowAccounting[1, i];
                        }
                    }
                }
                else
                {
                    if (Convert.ToDateTime(dgvShowAccounting[4, dgvShowAccounting.CurrentRow.Index].Value)
                        > DateTime.Now.AddHours(-1))
                    {
                        int idAdd = 0;
                        FormHotelStay hotelStay = new FormHotelStay();
                        hotelStay.bookingIndex = bkIndex;
                        hotelStay.ShowDialog();
                        if (hotelStay.DialogResult == DialogResult.OK)
                        {
                            miHotelStay_Click(null, null);
                            idAdd = db.HotelStayTables.Max(n => n.HotelStayId);
                                for (int i = 0; i < dgvShowAccounting.Rows.Count; i++)
                                {
                                    if (Convert.ToInt32(dgvShowAccounting[0, i].Value) == idAdd)
                                    {
                                        dgvShowAccounting.CurrentCell = dgvShowAccounting[1, i];
                                    }
                                }
                            db.SaveChanges();
                            if (MessageBox.Show("Внести информацию об оплате?", "Внимание!",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                FormPayment payment = new FormPayment();
                                payment.Text = "Добавление информации об оплате";
                                payment.userId = this.userId;
                                payment.indexHotelStay = idAdd;
                                payment.ShowDialog();
                                if (payment.DialogResult == DialogResult.OK)
                                {
                                    miPayment_Click(null, null);
                                }
                            }
                        }
                    }
                }
            }
            else if (table == "hotelStay")
            {
                if (String.IsNullOrEmpty(dgvShowAccounting[7, dgvShowAccounting.CurrentRow.Index].Value.ToString()) ||
                    Convert.ToDateTime(dgvShowAccounting[7, dgvShowAccounting.CurrentRow.Index].Value)
                    > DateTime.Now.AddDays(-1))
                {
                    if (MessageBox.Show("Внести информацию об оплате?", "Внимание!",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        FormPayment payment = new FormPayment();
                        payment.Text = "Добавление информации об оплате";
                        payment.userId = this.userId;
                        payment.indexHotelStay = Convert.ToInt32(dgvShowAccounting[0,
                         dgvShowAccounting.CurrentRow.Index].Value.ToString());
                        payment.ShowDialog();
                        if (payment.DialogResult == DialogResult.OK)
                        {
                            miPayment_Click(null, null);
                        }
                    }
                }
                else
                {
                    int hotelStayIndex = Convert.ToInt32(dgvShowAccounting[0,
                         dgvShowAccounting.CurrentRow.Index].Value.ToString());
                    var payment = db.PaymentTables
                        .FirstOrDefault(n => n.HotelStayId == hotelStayIndex);
                    if (payment != null)
                    {
                        miPayment_Click(null, null);
                        if (payment.DateTimePay < DateTime.Now.AddMonths(-1))
                        {
                            dtpBegin.Value = payment.DateTimePay;
                        }
                        for (int i = 0; i < dgvShowAccounting.Rows.Count; i++)
                        {
                            if (Convert.ToInt32(dgvShowAccounting[0, i].Value) == payment.PaymentId)
                            {
                                dgvShowAccounting.CurrentCell = dgvShowAccounting[1, i];
                            }
                        }
                    }
                }
            }
        }

        private void dgvShowAccounting_DataSourceChanged(object sender, EventArgs e)
        {
            if (dgvShowAccounting.Rows.Count != 0)
            {
                dgvShowAccounting.CurrentCell = dgvShowAccounting[1, 0];
                miEdit.Enabled = true;
                miDelete.Enabled = true;
            }
            else
            {
                miEdit.Enabled = false;
                miDelete.Enabled = false;
            }
            lblCount.Text = "Количество записей: " + dgvShowAccounting.Rows.Count;
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Restart();
        }

        private void FormHotelAccount_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            System.Windows.Forms.Application.Exit();
        }
    }
}
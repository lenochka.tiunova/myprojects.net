﻿namespace HotelApp
{
    partial class FormHotelStay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHotelStay));
            this.btnSave = new System.Windows.Forms.Button();
            this.cbNumber = new System.Windows.Forms.ComboBox();
            this.lblNumber = new System.Windows.Forms.Label();
            this.dtpSettlement = new System.Windows.Forms.DateTimePicker();
            this.lblDateSettlement = new System.Windows.Forms.Label();
            this.lblCountCustomer = new System.Windows.Forms.Label();
            this.nudCountCustomer = new System.Windows.Forms.NumericUpDown();
            this.lblCustomer1 = new System.Windows.Forms.Label();
            this.tbCustomer = new System.Windows.Forms.TextBox();
            this.btnSearchCustomer = new System.Windows.Forms.Button();
            this.dtpEviction = new System.Windows.Forms.DateTimePicker();
            this.lblEviction = new System.Windows.Forms.Label();
            this.btnEviction = new System.Windows.Forms.Button();
            this.dtpPlannedСheckOut = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.btnEvictionAll = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudCountCustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Sienna;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(204, 145);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbNumber
            // 
            this.cbNumber.BackColor = System.Drawing.Color.White;
            this.cbNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNumber.FormattingEnabled = true;
            this.cbNumber.Location = new System.Drawing.Point(54, 12);
            this.cbNumber.Name = "cbNumber";
            this.cbNumber.Size = new System.Drawing.Size(182, 21);
            this.cbNumber.TabIndex = 1;
            // 
            // lblNumber
            // 
            this.lblNumber.AutoSize = true;
            this.lblNumber.Location = new System.Drawing.Point(6, 15);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(44, 13);
            this.lblNumber.TabIndex = 77;
            this.lblNumber.Text = "Номер:";
            // 
            // dtpSettlement
            // 
            this.dtpSettlement.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpSettlement.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSettlement.Location = new System.Drawing.Point(145, 50);
            this.dtpSettlement.Name = "dtpSettlement";
            this.dtpSettlement.Size = new System.Drawing.Size(122, 20);
            this.dtpSettlement.TabIndex = 80;
            this.dtpSettlement.TabStop = false;
            // 
            // lblDateSettlement
            // 
            this.lblDateSettlement.AutoSize = true;
            this.lblDateSettlement.Location = new System.Drawing.Point(6, 53);
            this.lblDateSettlement.Name = "lblDateSettlement";
            this.lblDateSettlement.Size = new System.Drawing.Size(137, 13);
            this.lblDateSettlement.TabIndex = 79;
            this.lblDateSettlement.Text = "Дата и время заселения:";
            // 
            // lblCountCustomer
            // 
            this.lblCountCustomer.AutoSize = true;
            this.lblCountCustomer.Location = new System.Drawing.Point(255, 15);
            this.lblCountCustomer.Name = "lblCountCustomer";
            this.lblCountCustomer.Size = new System.Drawing.Size(135, 13);
            this.lblCountCustomer.TabIndex = 81;
            this.lblCountCustomer.Text = "Количество заселяемых:";
            // 
            // nudCountCustomer
            // 
            this.nudCountCustomer.Location = new System.Drawing.Point(397, 13);
            this.nudCountCustomer.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nudCountCustomer.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCountCustomer.Name = "nudCountCustomer";
            this.nudCountCustomer.Size = new System.Drawing.Size(72, 20);
            this.nudCountCustomer.TabIndex = 2;
            this.nudCountCustomer.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCountCustomer.ValueChanged += new System.EventHandler(this.nudCountCustomer_ValueChanged);
            // 
            // lblCustomer1
            // 
            this.lblCustomer1.AutoSize = true;
            this.lblCustomer1.Location = new System.Drawing.Point(8, 118);
            this.lblCustomer1.Name = "lblCustomer1";
            this.lblCustomer1.Size = new System.Drawing.Size(46, 13);
            this.lblCustomer1.TabIndex = 83;
            this.lblCustomer1.Text = "Клиент:";
            // 
            // tbCustomer
            // 
            this.tbCustomer.BackColor = System.Drawing.SystemColors.Control;
            this.tbCustomer.Location = new System.Drawing.Point(58, 115);
            this.tbCustomer.MaxLength = 150;
            this.tbCustomer.Name = "tbCustomer";
            this.tbCustomer.ReadOnly = true;
            this.tbCustomer.Size = new System.Drawing.Size(268, 20);
            this.tbCustomer.TabIndex = 84;
            this.tbCustomer.TabStop = false;
            // 
            // btnSearchCustomer
            // 
            this.btnSearchCustomer.Location = new System.Drawing.Point(337, 114);
            this.btnSearchCustomer.Name = "btnSearchCustomer";
            this.btnSearchCustomer.Size = new System.Drawing.Size(132, 23);
            this.btnSearchCustomer.TabIndex = 5;
            this.btnSearchCustomer.Text = "Выбрать клиента";
            this.btnSearchCustomer.UseVisualStyleBackColor = true;
            this.btnSearchCustomer.Click += new System.EventHandler(this.btnSearchCustomer_Click);
            // 
            // dtpEviction
            // 
            this.dtpEviction.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpEviction.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEviction.Location = new System.Drawing.Point(346, 53);
            this.dtpEviction.Name = "dtpEviction";
            this.dtpEviction.Size = new System.Drawing.Size(123, 20);
            this.dtpEviction.TabIndex = 87;
            this.dtpEviction.TabStop = false;
            this.dtpEviction.Visible = false;
            // 
            // lblEviction
            // 
            this.lblEviction.AutoSize = true;
            this.lblEviction.Location = new System.Drawing.Point(277, 56);
            this.lblEviction.Margin = new System.Windows.Forms.Padding(0);
            this.lblEviction.Name = "lblEviction";
            this.lblEviction.Size = new System.Drawing.Size(66, 13);
            this.lblEviction.TabIndex = 86;
            this.lblEviction.Text = "выселения:";
            this.lblEviction.Visible = false;
            // 
            // btnEviction
            // 
            this.btnEviction.Location = new System.Drawing.Point(337, 49);
            this.btnEviction.Name = "btnEviction";
            this.btnEviction.Size = new System.Drawing.Size(132, 23);
            this.btnEviction.TabIndex = 4;
            this.btnEviction.Text = "Выселить клиента";
            this.btnEviction.UseVisualStyleBackColor = true;
            this.btnEviction.Visible = false;
            this.btnEviction.Click += new System.EventHandler(this.btnEviction_Click);
            // 
            // dtpPlannedСheckOut
            // 
            this.dtpPlannedСheckOut.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpPlannedСheckOut.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPlannedСheckOut.Location = new System.Drawing.Point(235, 81);
            this.dtpPlannedСheckOut.Name = "dtpPlannedСheckOut";
            this.dtpPlannedСheckOut.Size = new System.Drawing.Size(122, 20);
            this.dtpPlannedСheckOut.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(223, 13);
            this.label1.TabIndex = 90;
            this.label1.Text = "Планируемая дата освобождения номера:";
            // 
            // btnEvictionAll
            // 
            this.btnEvictionAll.Location = new System.Drawing.Point(337, 11);
            this.btnEvictionAll.Name = "btnEvictionAll";
            this.btnEvictionAll.Size = new System.Drawing.Size(132, 23);
            this.btnEvictionAll.TabIndex = 2;
            this.btnEvictionAll.Text = "Освободить номер";
            this.btnEvictionAll.UseVisualStyleBackColor = true;
            this.btnEvictionAll.Visible = false;
            this.btnEvictionAll.Click += new System.EventHandler(this.btnEvictionAll_Click);
            // 
            // FormHotelStay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tan;
            this.ClientSize = new System.Drawing.Size(476, 181);
            this.Controls.Add(this.btnEvictionAll);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpPlannedСheckOut);
            this.Controls.Add(this.dtpEviction);
            this.Controls.Add(this.lblEviction);
            this.Controls.Add(this.btnEviction);
            this.Controls.Add(this.btnSearchCustomer);
            this.Controls.Add(this.tbCustomer);
            this.Controls.Add(this.lblCustomer1);
            this.Controls.Add(this.nudCountCustomer);
            this.Controls.Add(this.lblCountCustomer);
            this.Controls.Add(this.dtpSettlement);
            this.Controls.Add(this.lblDateSettlement);
            this.Controls.Add(this.cbNumber);
            this.Controls.Add(this.lblNumber);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormHotelStay";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление информации о проживании";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormHotelStay_FormClosing);
            this.Load += new System.EventHandler(this.FormHotelStay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudCountCustomer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cbNumber;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.DateTimePicker dtpSettlement;
        private System.Windows.Forms.Label lblDateSettlement;
        private System.Windows.Forms.Label lblCountCustomer;
        private System.Windows.Forms.NumericUpDown nudCountCustomer;
        private System.Windows.Forms.Label lblCustomer1;
        private System.Windows.Forms.TextBox tbCustomer;
        private System.Windows.Forms.Button btnSearchCustomer;
        private System.Windows.Forms.DateTimePicker dtpEviction;
        private System.Windows.Forms.Label lblEviction;
        private System.Windows.Forms.Button btnEviction;
        private System.Windows.Forms.DateTimePicker dtpPlannedСheckOut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEvictionAll;
    }
}
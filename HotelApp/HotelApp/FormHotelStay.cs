﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelApp
{
    public partial class FormHotelStay : Form
    {
        public FormHotelStay()
        {
            InitializeComponent();
        }

        static dbHotelEntities db = new dbHotelEntities();

        private List<int> listCustomer = new List<int>();
        private List<int> listHotelStay = new List<int>();
        public int index = -1;
        private bool exit = false;
        private decimal nudValue = 0;
        public int bookingIndex = -1;

        private void FormHotelStay_Load(object sender, EventArgs e)
        {
            cbNumber.DataSource = db.RoomTables.Select(n => n.RoomNumber).ToList();
            if (cbNumber.DropDownHeight > 80)
            {
                cbNumber.DropDownHeight = 80;
            }
            listHotelStay.Clear();
            nudValue = nudCountCustomer.Value;
            listCustomer.Clear();
            listCustomer.Add(-1);

            if (this.Text == "Изменение информации о проживании")
            {
                nudCountCustomer.Visible = false;
                lblCountCustomer.Visible = false;
                var hotelStay = db.HotelStayTables.FirstOrDefault(n => n.HotelStayId == index);
                if(hotelStay.CheckOutDate == null)
                {
                    btnEviction.Visible = true;
                    btnEvictionAll.Visible = true;
                }
                else
                {
                    lblEviction.Visible = true;
                    dtpEviction.Visible = true;
                    dtpEviction.Value = (DateTime)hotelStay.CheckOutDate;
                }
                cbNumber.Text = hotelStay.SettlementTable.RoomTable.RoomNumber;
                dtpSettlement.Value = hotelStay.SettlementDate;
                dtpPlannedСheckOut.Value = hotelStay.SettlementTable.PlannedСheckOutDate;
                tbCustomer.Text = String.Concat(hotelStay.CustomerTable.Surname, " ", hotelStay.CustomerTable.Name, 
                    " ", hotelStay.CustomerTable.Middle);
                cbNumber.Enabled = false;
                btnSearchCustomer.Visible = false;
                if (hotelStay.SettlementTable.BookingId != null)
                {
                    bookingIndex = (int)hotelStay.SettlementTable.BookingId;
                }
                listHotelStay.Add(index);
                listCustomer[0] = hotelStay.CustomerId;
            }
            else
            {
                cbNumber.SelectedIndex = 0;
                if(bookingIndex > 0)
                {
                    dtpPlannedСheckOut.Value = db.BookingTables.FirstOrDefault(n => n.BookingId 
                    == bookingIndex).CheckOutDate;
                    cbNumber.Text = db.BookingTables.FirstOrDefault(n => n.BookingId
                    == bookingIndex).RoomTable.RoomNumber;
                }else
                {
                    dtpPlannedСheckOut.Value = dtpSettlement.Value.AddDays(1);
                }
            }

            dtpSettlement.Enabled = false;
            dtpPlannedСheckOut.MinDate = dtpSettlement.Value;
            dtpEviction.Enabled = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var hotelStayRoom = db.HotelStayTables.Where(n => n.SettlementTable.RoomTable.RoomNumber
                == cbNumber.Text && n.CheckOutDate == null).ToList();
            string information = "";
            if (this.Text == "Добавление информации о проживании" && hotelStayRoom.Count != 0)
            {
                information = "В выбранном номере проживают на данный промежуток времени.";
            }
            else
            {
                var bookingRoom = db.BookingTables.Where(n => n.RoomTable.RoomNumber == cbNumber.Text
                && n.BookingId != bookingIndex).ToList();

                foreach (BookingTable str in bookingRoom)
                {
                    if (!(str.CheckOutDate <= dtpSettlement.Value.AddHours(-1).AddMinutes(1)) &&
                        !(str.CheckInDate >= dtpPlannedСheckOut.Value.AddHours(1).AddMinutes(-1)))
                    {
                        information = "Выбранный номер забронирован на данный промежуток времени.";
                    }
                }
            }


            if (!listCustomer.Contains(-1))
            {
                if (String.IsNullOrEmpty(information) || !String.IsNullOrEmpty(information) &&
                MessageBox.Show(information + " Продолжить?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Information) ==
                DialogResult.Yes)
                {
                    if (this.Text == "Добавление информации о проживании")
                    {
                        if (hotelStayRoom.Count == 0)
                        {
                            int? bkIndex;
                            if (bookingIndex >= 0)
                            {
                                bkIndex = bookingIndex;
                            }
                            else
                            {
                                bkIndex = null;
                            }
                            SettlementTable settlement = new SettlementTable
                            {
                                RoomId = db.RoomTables.FirstOrDefault(n => n.RoomNumber == cbNumber.Text).RoomId,
                                BookingId = bkIndex,
                                PlannedСheckOutDate = dtpPlannedСheckOut.Value
                            };
                            db.SettlementTables.Add(settlement);
                            db.SaveChanges();
                        }
                        else
                        {
                            if (hotelStayRoom.First().SettlementTable.PlannedСheckOutDate < dtpPlannedСheckOut.Value)
                            {
                                int settlementId = hotelStayRoom.First().SettlementId;
                                var settlement = db.SettlementTables.FirstOrDefault(n => n.SettlementId == settlementId);
                                settlement.PlannedСheckOutDate = dtpPlannedСheckOut.Value;
                                db.SaveChanges();
                            }
                        }

                        for (int i = 0; i < nudCountCustomer.Value; i++)
                        {
                            HotelStayTable hotelStay = new HotelStayTable
                            {
                                SettlementId = hotelStayRoom.Count == 0 ? db.SettlementTables.Max(n => n.SettlementId) :
                                hotelStayRoom.First().SettlementId,
                                CustomerId = listCustomer[i],
                                SettlementDate = dtpSettlement.Value
                            };
                            db.HotelStayTables.Add(hotelStay);
                            db.SaveChanges();
                        }
                        MessageBox.Show("Информация о проживании добавлена.", "Внимание!",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.DialogResult = DialogResult.OK;
                        exit = true;
                        this.Close();
                    }
                    else if (this.Text == "Изменение информации о проживании")
                    {
                        for (int i = 0; i < listHotelStay.Count; i++)
                        {
                            int id = listHotelStay[i];
                            var hotelStay = db.HotelStayTables.FirstOrDefault(n => n.HotelStayId == id);
                            hotelStay.SettlementTable.PlannedСheckOutDate = dtpPlannedСheckOut.Value;
                            if (dtpEviction.Visible)
                            {
                                hotelStay.CheckOutDate = dtpEviction.Value;
                            }
                        }

                        db.SaveChanges();
                        MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.DialogResult = DialogResult.OK;
                        exit = true;
                        this.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show("Заполните все поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void nudCountCustomer_ValueChanged(object sender, EventArgs e)
        {
            if (nudValue > nudCountCustomer.Value)
            {
                for (decimal i = nudValue; i > nudCountCustomer.Value; i--)
                {
                    this.Controls["btnSearchCustomer" + i].Dispose();
                    this.Controls["tbCustomer" + i].Dispose();
                    this.Controls["lblCustomer" + i].Dispose();
                    btnSave.Location = new System.Drawing.Point(btnSave.Location.X, btnSave.Location.Y - 28);
                    this.Height = btnSave.Location.Y + 75;
                    listCustomer.RemoveAt(listCustomer.Count - 1);
                }
            }
            else
            {
                for (decimal i = nudValue; i < nudCountCustomer.Value; i++)
                {
                    Label lblNewCustomer = new Label();
                    lblNewCustomer.AutoSize = true;
                    lblNewCustomer.Location = new System.Drawing.Point(lblCustomer1.Location.X,
                        (int)(lblCustomer1.Location.Y + lblCustomer1.Height * i + 15 * i));
                    lblNewCustomer.Size = new System.Drawing.Size(46, 13);
                    lblNewCustomer.Text = "Клиент:";
                    lblNewCustomer.Name = "lblCustomer" + (i + 1);
                    Controls.Add(lblNewCustomer);

                    TextBox tbNewCustomer = new TextBox();
                    tbNewCustomer.BackColor = System.Drawing.SystemColors.Window;
                    tbNewCustomer.Location = new System.Drawing.Point(tbCustomer.Location.X,
                        lblNewCustomer.Location.Y - 3);
                    tbNewCustomer.MaxLength = 150;
                    tbNewCustomer.Name = "tbCustomer" + (i + 1);
                    tbNewCustomer.ReadOnly = true;
                    tbNewCustomer.Size = new System.Drawing.Size(268, 20);
                    Controls.Add(tbNewCustomer);

                    Button btnNewCustomer = new Button();
                    btnNewCustomer.Location = new System.Drawing.Point(btnSearchCustomer.Location.X,
                        lblNewCustomer.Location.Y - 4);
                    btnNewCustomer.Name = "btnSearchCustomer" + (i + 1);
                    btnNewCustomer.Size = new System.Drawing.Size(132, 23);
                    btnNewCustomer.Text = "Выбрать клиента";
                    btnNewCustomer.UseVisualStyleBackColor = true;
                    btnNewCustomer.Click += new System.EventHandler(this.btnSearchCustomer_Click);
                    Controls.Add(btnNewCustomer);

                    btnSave.Location = new System.Drawing.Point(btnSave.Location.X,
                        btnSave.Location.Y + 28);
                    this.Height = btnSave.Location.Y + 75;
                    listCustomer.Add(-1);
                }
            }
            nudValue = nudCountCustomer.Value;
        }

        private void btnSearchCustomer_Click(object sender, EventArgs e)
        {
            var button = (Button)sender;
            FormSearchCustomer searchCustomer = new FormSearchCustomer();
            if (button != null)
            {
                if (!String.IsNullOrEmpty(button.Name.Replace("btnSearchCustomer", "")))
                {
                    searchCustomer.customerIndex = listCustomer[Convert.ToInt32
                        (button.Name.Replace("btnSearchCustomer", "")) - 1];
                }
                else
                {
                    searchCustomer.customerIndex = listCustomer[0];
                }
            }
            searchCustomer.ShowDialog();
            int indexCustomer = searchCustomer.ReturnData();
            if (searchCustomer.DialogResult == DialogResult.OK)
            {
                if (button != null)
                {
                    var customer = db.CustomerTables.FirstOrDefault(n => n.CustomerId == indexCustomer);
                    if (listCustomer.Contains(customer.CustomerId))
                    {
                        MessageBox.Show("Данный клиент уже выбран", "Ошибка", 
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(button.Name.Replace("btnSearchCustomer", "")))
                        {
                            int index = Convert.ToInt32(button.Name.Replace("btnSearchCustomer", "")) - 1;
                            listCustomer[index] = customer.CustomerId;
                        }
                        else
                        {
                            listCustomer[0] = customer.CustomerId;
                        }
                        this.Controls["tbCustomer" + button.Name.Replace("btnSearchCustomer", "")].Text
                                = String.Concat(customer.Surname, " ", customer.Name, " ", customer.Middle);
                    }
                }
            }
        }

        private void btnEviction_Click(object sender, EventArgs e)
        {
            lblEviction.Visible = true;
            dtpEviction.Visible = true;
            btnEviction.Visible = false;
            dtpPlannedСheckOut.Enabled = false;
            dtpEviction.Value = DateTime.Now;
        }

        private void btnEvictionAll_Click(object sender, EventArgs e)
        {
            btnEviction_Click(null,null);
            var settlementHotelStay = db.HotelStayTables
                .FirstOrDefault(n => n.HotelStayId == index).SettlementId;
            var customersSettlement = db.HotelStayTables.Where(n => n.SettlementId == settlementHotelStay && n.HotelStayId != index).ToList();
            nudCountCustomer.Value = customersSettlement.Count + 1;
            int i = 2;
            foreach (HotelStayTable hst in customersSettlement)
            {
                this.Controls["btnSearchCustomer" + i].Visible = false;
                this.Controls["tbCustomer" + i].Text
                                = String.Concat(hst.CustomerTable.Surname, " ", 
                                hst.CustomerTable.Name, " ", hst.CustomerTable.Middle);
                listHotelStay.Add(hst.HotelStayId);
                i++;
            }
        }

        private void FormHotelStay_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }
}

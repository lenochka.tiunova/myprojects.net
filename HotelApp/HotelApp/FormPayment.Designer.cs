﻿namespace HotelApp
{
    partial class FormPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPayment));
            this.btnSave = new System.Windows.Forms.Button();
            this.cbProcedure = new System.Windows.Forms.ComboBox();
            this.lblProcedure = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblSum = new System.Windows.Forms.Label();
            this.nudSum = new System.Windows.Forms.NumericUpDown();
            this.lblNumber = new System.Windows.Forms.Label();
            this.tbNumber = new System.Windows.Forms.TextBox();
            this.btnSearchHotelStay = new System.Windows.Forms.Button();
            this.lblNumberCheck = new System.Windows.Forms.Label();
            this.tbNumberCheck = new System.Windows.Forms.TextBox();
            this.dtpEviction = new System.Windows.Forms.DateTimePicker();
            this.lblEviction = new System.Windows.Forms.Label();
            this.dtpSettlement = new System.Windows.Forms.DateTimePicker();
            this.lblDateSettlement = new System.Windows.Forms.Label();
            this.gbHotelStay = new System.Windows.Forms.GroupBox();
            this.cbPayer = new System.Windows.Forms.ComboBox();
            this.lblPayer = new System.Windows.Forms.Label();
            this.lblSumPay = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblCountDays = new System.Windows.Forms.Label();
            this.lblNameUser = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudSum)).BeginInit();
            this.gbHotelStay.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Sienna;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(255, 234);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbProcedure
            // 
            this.cbProcedure.BackColor = System.Drawing.Color.White;
            this.cbProcedure.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProcedure.FormattingEnabled = true;
            this.cbProcedure.Items.AddRange(new object[] {
            "Зачисление",
            "Возврат"});
            this.cbProcedure.Location = new System.Drawing.Point(285, 9);
            this.cbProcedure.Name = "cbProcedure";
            this.cbProcedure.Size = new System.Drawing.Size(137, 21);
            this.cbProcedure.TabIndex = 1;
            // 
            // lblProcedure
            // 
            this.lblProcedure.AutoSize = true;
            this.lblProcedure.Location = new System.Drawing.Point(219, 12);
            this.lblProcedure.Name = "lblProcedure";
            this.lblProcedure.Size = new System.Drawing.Size(60, 13);
            this.lblProcedure.TabIndex = 77;
            this.lblProcedure.Text = "Операция:";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(15, 211);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(83, 13);
            this.lblUser.TabIndex = 79;
            this.lblUser.Text = "Пользователь:";
            // 
            // lblSum
            // 
            this.lblSum.AutoSize = true;
            this.lblSum.Location = new System.Drawing.Point(448, 13);
            this.lblSum.Name = "lblSum";
            this.lblSum.Size = new System.Drawing.Size(44, 13);
            this.lblSum.TabIndex = 81;
            this.lblSum.Text = "Сумма:";
            // 
            // nudSum
            // 
            this.nudSum.DecimalPlaces = 2;
            this.nudSum.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudSum.Location = new System.Drawing.Point(501, 10);
            this.nudSum.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudSum.Name = "nudSum";
            this.nudSum.Size = new System.Drawing.Size(84, 20);
            this.nudSum.TabIndex = 2;
            this.nudSum.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // lblNumber
            // 
            this.lblNumber.AutoSize = true;
            this.lblNumber.Location = new System.Drawing.Point(40, 22);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(44, 13);
            this.lblNumber.TabIndex = 83;
            this.lblNumber.Text = "Номер:";
            // 
            // tbNumber
            // 
            this.tbNumber.BackColor = System.Drawing.SystemColors.Window;
            this.tbNumber.Enabled = false;
            this.tbNumber.Location = new System.Drawing.Point(90, 19);
            this.tbNumber.MaxLength = 150;
            this.tbNumber.Name = "tbNumber";
            this.tbNumber.ReadOnly = true;
            this.tbNumber.Size = new System.Drawing.Size(101, 20);
            this.tbNumber.TabIndex = 84;
            this.tbNumber.TabStop = false;
            // 
            // btnSearchHotelStay
            // 
            this.btnSearchHotelStay.Location = new System.Drawing.Point(216, 80);
            this.btnSearchHotelStay.Name = "btnSearchHotelStay";
            this.btnSearchHotelStay.Size = new System.Drawing.Size(132, 23);
            this.btnSearchHotelStay.TabIndex = 3;
            this.btnSearchHotelStay.Text = "Выбрать услугу";
            this.btnSearchHotelStay.UseVisualStyleBackColor = true;
            this.btnSearchHotelStay.Click += new System.EventHandler(this.btnSearchHotelStay_Click);
            // 
            // lblNumberCheck
            // 
            this.lblNumberCheck.AutoSize = true;
            this.lblNumberCheck.Location = new System.Drawing.Point(15, 12);
            this.lblNumberCheck.Name = "lblNumberCheck";
            this.lblNumberCheck.Size = new System.Drawing.Size(70, 13);
            this.lblNumberCheck.TabIndex = 90;
            this.lblNumberCheck.Text = "Номер чека:";
            // 
            // tbNumberCheck
            // 
            this.tbNumberCheck.Enabled = false;
            this.tbNumberCheck.Location = new System.Drawing.Point(91, 9);
            this.tbNumberCheck.Name = "tbNumberCheck";
            this.tbNumberCheck.Size = new System.Drawing.Size(101, 20);
            this.tbNumberCheck.TabIndex = 92;
            this.tbNumberCheck.TabStop = false;
            // 
            // dtpEviction
            // 
            this.dtpEviction.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpEviction.Enabled = false;
            this.dtpEviction.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEviction.Location = new System.Drawing.Point(416, 19);
            this.dtpEviction.Name = "dtpEviction";
            this.dtpEviction.Size = new System.Drawing.Size(123, 20);
            this.dtpEviction.TabIndex = 97;
            this.dtpEviction.TabStop = false;
            // 
            // lblEviction
            // 
            this.lblEviction.AutoSize = true;
            this.lblEviction.Location = new System.Drawing.Point(391, 22);
            this.lblEviction.Margin = new System.Windows.Forms.Padding(0);
            this.lblEviction.Name = "lblEviction";
            this.lblEviction.Size = new System.Drawing.Size(22, 13);
            this.lblEviction.TabIndex = 96;
            this.lblEviction.Text = "по:";
            // 
            // dtpSettlement
            // 
            this.dtpSettlement.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpSettlement.Enabled = false;
            this.dtpSettlement.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSettlement.Location = new System.Drawing.Point(237, 19);
            this.dtpSettlement.Name = "dtpSettlement";
            this.dtpSettlement.Size = new System.Drawing.Size(122, 20);
            this.dtpSettlement.TabIndex = 95;
            this.dtpSettlement.TabStop = false;
            // 
            // lblDateSettlement
            // 
            this.lblDateSettlement.AutoSize = true;
            this.lblDateSettlement.Location = new System.Drawing.Point(213, 22);
            this.lblDateSettlement.Name = "lblDateSettlement";
            this.lblDateSettlement.Size = new System.Drawing.Size(16, 13);
            this.lblDateSettlement.TabIndex = 94;
            this.lblDateSettlement.Text = "с:";
            // 
            // gbHotelStay
            // 
            this.gbHotelStay.Controls.Add(this.cbPayer);
            this.gbHotelStay.Controls.Add(this.lblPayer);
            this.gbHotelStay.Controls.Add(this.lblSumPay);
            this.gbHotelStay.Controls.Add(this.lblPrice);
            this.gbHotelStay.Controls.Add(this.lblCountDays);
            this.gbHotelStay.Controls.Add(this.dtpEviction);
            this.gbHotelStay.Controls.Add(this.lblNumber);
            this.gbHotelStay.Controls.Add(this.lblEviction);
            this.gbHotelStay.Controls.Add(this.tbNumber);
            this.gbHotelStay.Controls.Add(this.dtpSettlement);
            this.gbHotelStay.Controls.Add(this.btnSearchHotelStay);
            this.gbHotelStay.Controls.Add(this.lblDateSettlement);
            this.gbHotelStay.Location = new System.Drawing.Point(18, 39);
            this.gbHotelStay.Name = "gbHotelStay";
            this.gbHotelStay.Size = new System.Drawing.Size(567, 156);
            this.gbHotelStay.TabIndex = 98;
            this.gbHotelStay.TabStop = false;
            this.gbHotelStay.Text = "Услуга";
            // 
            // cbPayer
            // 
            this.cbPayer.BackColor = System.Drawing.Color.White;
            this.cbPayer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPayer.FormattingEnabled = true;
            this.cbPayer.Location = new System.Drawing.Point(92, 119);
            this.cbPayer.Name = "cbPayer";
            this.cbPayer.Size = new System.Drawing.Size(267, 21);
            this.cbPayer.TabIndex = 102;
            this.cbPayer.SelectedIndexChanged += new System.EventHandler(this.cbPayer_SelectedIndexChanged);
            // 
            // lblPayer
            // 
            this.lblPayer.AutoSize = true;
            this.lblPayer.Location = new System.Drawing.Point(12, 122);
            this.lblPayer.Name = "lblPayer";
            this.lblPayer.Size = new System.Drawing.Size(74, 13);
            this.lblPayer.TabIndex = 101;
            this.lblPayer.Text = "Плательщик:";
            // 
            // lblSumPay
            // 
            this.lblSumPay.AutoSize = true;
            this.lblSumPay.Location = new System.Drawing.Point(391, 55);
            this.lblSumPay.Name = "lblSumPay";
            this.lblSumPay.Size = new System.Drawing.Size(82, 13);
            this.lblSumPay.TabIndex = 100;
            this.lblSumPay.Text = "Уже оплачено:";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(15, 55);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(69, 13);
            this.lblPrice.TabIndex = 99;
            this.lblPrice.Text = "Цена/сутки:";
            // 
            // lblCountDays
            // 
            this.lblCountDays.AutoSize = true;
            this.lblCountDays.Location = new System.Drawing.Point(213, 55);
            this.lblCountDays.Name = "lblCountDays";
            this.lblCountDays.Size = new System.Drawing.Size(100, 13);
            this.lblCountDays.TabIndex = 98;
            this.lblCountDays.Text = "Количество суток:";
            // 
            // lblNameUser
            // 
            this.lblNameUser.AutoSize = true;
            this.lblNameUser.Location = new System.Drawing.Point(107, 211);
            this.lblNameUser.Name = "lblNameUser";
            this.lblNameUser.Size = new System.Drawing.Size(0, 13);
            this.lblNameUser.TabIndex = 99;
            // 
            // FormPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tan;
            this.ClientSize = new System.Drawing.Size(601, 268);
            this.Controls.Add(this.lblNameUser);
            this.Controls.Add(this.gbHotelStay);
            this.Controls.Add(this.tbNumberCheck);
            this.Controls.Add(this.lblNumberCheck);
            this.Controls.Add(this.nudSum);
            this.Controls.Add(this.lblSum);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.cbProcedure);
            this.Controls.Add(this.lblProcedure);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormPayment";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление информации об оплате";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPayment_FormClosing);
            this.Load += new System.EventHandler(this.FormPayment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudSum)).EndInit();
            this.gbHotelStay.ResumeLayout(false);
            this.gbHotelStay.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cbProcedure;
        private System.Windows.Forms.Label lblProcedure;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblSum;
        private System.Windows.Forms.NumericUpDown nudSum;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.TextBox tbNumber;
        private System.Windows.Forms.Button btnSearchHotelStay;
        private System.Windows.Forms.Label lblNumberCheck;
        private System.Windows.Forms.TextBox tbNumberCheck;
        private System.Windows.Forms.DateTimePicker dtpEviction;
        private System.Windows.Forms.Label lblEviction;
        private System.Windows.Forms.DateTimePicker dtpSettlement;
        private System.Windows.Forms.Label lblDateSettlement;
        private System.Windows.Forms.GroupBox gbHotelStay;
        private System.Windows.Forms.Label lblNameUser;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblCountDays;
        private System.Windows.Forms.Label lblSumPay;
        private System.Windows.Forms.ComboBox cbPayer;
        private System.Windows.Forms.Label lblPayer;
    }
}
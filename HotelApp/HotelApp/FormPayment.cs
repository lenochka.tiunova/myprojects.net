﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelApp
{
    public partial class FormPayment : Form
    {
        public FormPayment()
        {
            InitializeComponent();
        }

        static dbHotelEntities db = new dbHotelEntities();
        public int index = -1;
        public int userId = -1;
        private int indexSettlement = -1;
        public int indexHotelStay = -1;
        private string payer = "";
        private bool exit = false;

        private void FormPayment_Load(object sender, EventArgs e)
        {
            if (this.Text == "Редактирование информации об оплате")
            {
                var payment = db.PaymentTables.FirstOrDefault(n => n.PaymentId == index);
                tbNumberCheck.Text = payment.CheckNumber.ToString();
                cbProcedure.SelectedIndex = payment.Sum.ToString().Contains("-") ? 1 : 0;
                nudSum.Value = Math.Abs(payment.Sum);
                indexHotelStay = payment.HotelStayId;
                indexSettlement = payment.HotelStayTable.SettlementId;
                var customer = db.HotelStayTables
                        .FirstOrDefault(n => n.HotelStayId == indexHotelStay).CustomerTable;
                payer = String.Concat(customer.Surname, " ", customer.Name, " ", customer.Middle);
                EnterData();
                lblNameUser.Text = String.Concat(payment.UserTable.Surname, " ", 
                    payment.UserTable.Name, " ", payment.UserTable.Middle);
                if (!db.UserTables.FirstOrDefault(n => n.UserId == userId).RoleTable.Privileged)
                {
                    btnSearchHotelStay.Enabled = false;
                    cbProcedure.Enabled = false;
                    nudSum.Enabled = false;
                }
            }
            else
            {   tbNumberCheck.Text = (db.HotelInfoTables.First().CheckNumber + 1).ToString();
                cbProcedure.SelectedIndex = 0;
                if(indexHotelStay != -1)
                {
                    indexSettlement = db.HotelStayTables
                        .FirstOrDefault(n => n.HotelStayId == indexHotelStay).SettlementId;
                    var customer = db.HotelStayTables
                        .FirstOrDefault(n => n.HotelStayId == indexHotelStay).CustomerTable;
                    payer = String.Concat(customer.Surname, " ", customer.Name, " ", customer.Middle);
                    EnterData();
                }

                var user = db.UserTables.FirstOrDefault(n => n.UserId == userId);
                lblNameUser.Text = String.Concat(user.Surname, " ", user.Name, " ", user.Middle);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (nudSum.Value != 0)
            {
                if (this.Text == "Добавление информации об оплате")
                {
                    if (!String.IsNullOrEmpty(tbNumber.Text))
                    {
                        PaymentTable payment = new PaymentTable
                        {
                            CheckNumber = Convert.ToInt32(tbNumberCheck.Text),
                            HotelStayId = indexHotelStay,
                            UserId = userId,
                            Sum = cbProcedure.SelectedIndex == 0 ? nudSum.Value : -nudSum.Value,
                            DateTimePay = DateTime.Now
                        };
                        db.PaymentTables.Add(payment);

                        if (db.HotelInfoTables.First().CheckNumber < Convert.ToInt32(tbNumberCheck.Text))
                        {
                            var hotelInfoTable = db.HotelInfoTables.First();
                            hotelInfoTable.CheckNumber += 1;
                        }
                        db.SaveChanges();

                        MessageBox.Show("Информация об оплате добавлена.", "Внимание!",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.DialogResult = DialogResult.OK;
                        exit = true;
                        this.Close();

                    }
                    else
                    {
                        MessageBox.Show("Заполните все поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (this.Text == "Редактирование информации об оплате")
                {
                    var payment = db.PaymentTables.FirstOrDefault(n => n.PaymentId == index);
                    payment.Sum = cbProcedure.SelectedIndex == 0 ? nudSum.Value : -nudSum.Value;
                    payment.CheckNumber = Convert.ToInt32(tbNumberCheck.Text);
                    payment.HotelStayId = indexHotelStay; 
                    if (db.HotelInfoTables.First().CheckNumber < Convert.ToInt32(tbNumberCheck.Text))
                    {
                        var hotelInfoTable = db.HotelInfoTables.First();
                        hotelInfoTable.CheckNumber += 1;
                    }
                    db.SaveChanges();
                    MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.OK;
                    exit = true;
                    this.Close();
                }
            } else
            {
                MessageBox.Show("Измените сумму операции.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void EnterData()
        {
            var settlement = db.SettlementTables.FirstOrDefault(n => n.SettlementId == indexSettlement);
            tbNumber.Text = settlement.RoomTable.RoomNumber;
            dtpSettlement.Value = db.HotelStayTables.Where(n => n.SettlementId == settlement.SettlementId)
                .Min(n => n.SettlementDate);
            dtpEviction.Value = settlement.PlannedСheckOutDate;
            lblPrice.Text = "Цена/сутки: " + settlement.RoomTable.PriceperDay.ToString("N");
            lblCountDays.Text = "Количество суток: " + Math.Round((dtpEviction.Value - dtpSettlement.Value).TotalDays);
            decimal paySum = db.PaymentTables.Where(n => n.HotelStayTable.SettlementTable.SettlementId == settlement.SettlementId).ToList().Count == 0 ? 0 :
                db.PaymentTables.Where(n => n.HotelStayTable.SettlementTable.SettlementId == settlement.SettlementId).Sum(n => n.Sum);

            if (this.Text == "Добавление информации об оплате")
            {
                if (settlement.RoomTable.PriceperDay
               * (decimal)Math.Round((dtpEviction.Value - dtpSettlement.Value).TotalDays) - paySum < 0)
                {
                    nudSum.Value = Math.Abs(settlement.RoomTable.PriceperDay
                    * (decimal)Math.Round((dtpEviction.Value - dtpSettlement.Value).TotalDays) - paySum);
                    cbProcedure.SelectedIndex = 1;
                }
                else
                {
                    nudSum.Value = settlement.RoomTable.PriceperDay
                        * (decimal)Math.Round((dtpEviction.Value - dtpSettlement.Value).TotalDays) - paySum;
                    cbProcedure.SelectedIndex = 0;
                }
            }
            lblSumPay.Text = "Уже оплачено: " + paySum.ToString("N2");
            DateTime dt = DateTime.Now.AddHours(-1);
            cbPayer.DataSource = db.HotelStayTables.Where(n => n.SettlementId == settlement.SettlementId &&
            (n.CheckOutDate == null || n.CheckOutDate > dt))
                .Select(n => String.Concat(n.CustomerTable.Surname, " ", 
                n.CustomerTable.Name, " ", n.CustomerTable.Middle)).ToList();
            if (String.IsNullOrEmpty(payer))
            {
                cbPayer.SelectedIndex = 0;
            }
            else
            {
                cbPayer.SelectedItem = payer;
            }
        }

        private void cbPayer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbPayer.DataSource != null)
            {
                indexHotelStay = db.HotelStayTables.FirstOrDefault(n => n.SettlementId == indexSettlement
                && String.Concat(n.CustomerTable.Surname, " ", n.CustomerTable.Name, " ", n.CustomerTable.Middle)
                == cbPayer.Text.Trim()).HotelStayId;
                if (db.PaymentTables.FirstOrDefault(n => n.HotelStayId == indexHotelStay) == null)
                {
                    tbNumberCheck.Text = (db.HotelInfoTables.First().CheckNumber + 1).ToString();
                }
                else
                {
                    tbNumberCheck.Text = db.PaymentTables.FirstOrDefault(n => n.HotelStayId == indexHotelStay).CheckNumber.ToString();
                }
            }
            else
            {
                indexHotelStay = -1;
                tbNumberCheck.Clear();
            }
        }

        private void btnSearchHotelStay_Click(object sender, EventArgs e)
        {
            FormSearchHotelStay searchHotelStay = new FormSearchHotelStay();
            if(indexSettlement != -1)
            {
                searchHotelStay.settlementIndex = index;
            }
            searchHotelStay.ShowDialog();
            if (searchHotelStay.DialogResult == DialogResult.OK)
            {
                indexHotelStay = -1;
                indexSettlement = searchHotelStay.ReturnData();
                EnterData();
            }
        }

        private void FormPayment_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }
}

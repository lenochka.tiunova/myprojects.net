﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelApp
{
    public partial class FormReportCount : Form
    {
        private bool choice = false;
        public FormReportCount()
        {
            InitializeComponent();
            this.rb1.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
            this.rb2.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
            this.rb3.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
            this.rb4.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
            this.rb5.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
        }

        private readonly string connectionString = @"Data Source= DESKTOP-PPCBH42\SQLEXPREESS;
        Initial Catalog = dbHotel;Integrated Security=True";

        private void FormReport_Load(object sender, EventArgs e)
        {
            int year = DateTime.Now.Year;
            foreach (RadioButton rb in gbYear.Controls)
            {
                rb.Text = year.ToString();
                year--;
            }
            cbReportType.SelectedIndex = 0;
            rb5.Checked = true;
            choice = true;
            cbCriterion.SelectedIndex = 0;
        }

        private void CreateReport()
        {
            chartReport.Series[0].Points.Clear();

            string parameter = "";
            string yearPeriod = "";
            string type = "";

            switch (cbCriterion.SelectedIndex)
            {
                case 0:
                    parameter = "RoomNumber";
                    break;
                case 1:
                    parameter = "Comfort";
                    break;
                case 2:
                    parameter = "Accommodation";
                    break;
            }

            foreach (RadioButton rb in gbYear.Controls)
            {
                if (rb.Checked)
                {
                    yearPeriod = rb.Text.Trim();
                }
            }

            if (yearPeriod == "")
            {
                yearPeriod += "1";
            }

            if (cbReportType.SelectedIndex == 0)
            {
                type = "csQuarter";
            }
            else if (cbReportType.SelectedIndex == 1)
            {
                type = "csMonth";
            }

            string sql = "exec " + type + " @yearPeriod = '" + yearPeriod + "', @parameter = '" + parameter + "'";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);

                dgvReport.Columns.Clear();
                dgvReport.DataSource = ds.Tables[0];
                dgvReport.Columns[0].HeaderText = cbCriterion.Text;
                dgvReport.AutoResizeColumns();

                if (dgvReport.Rows[0].Cells[1].Value.ToString() != "")
                {
                    for (int j = 1; j < dgvReport.Columns.Count - 1; j++)
                    {
                        chartReport.Series[0].Points
                        .AddXY(ds.Tables[0].Columns[j].ColumnName,
                        ds.Tables[0].Rows[dgvReport.Rows.Count - 2].ItemArray[j]);
                        chartReport.Series[0].Points[j - 1].LabelBackColor = Color.Tan;
                        chartReport.Series[0].Points[j - 1].Color = Color.Sienna;
                    }
                    chartReport.Visible = true;
                }
                else
                {
                    chartReport.Visible = false;
                    MessageBox.Show("Нет данных для отчета", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void cbReportType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (choice)
            {
                CreateReport();
            }
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            if (choice && rb.Checked)
            {
                CreateReport();
            }
        }

        private void cbCriterion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (choice)
            {
                CreateReport();
            }
        }
    }
}
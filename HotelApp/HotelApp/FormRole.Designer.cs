﻿namespace HotelApp
{
    partial class FormRole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRole));
            this.btnSave = new System.Windows.Forms.Button();
            this.lblRole = new System.Windows.Forms.Label();
            this.tbRole = new System.Windows.Forms.TextBox();
            this.cbPrivileged = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Sienna;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(181, 68);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblRole
            // 
            this.lblRole.AutoSize = true;
            this.lblRole.Location = new System.Drawing.Point(12, 15);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(68, 13);
            this.lblRole.TabIndex = 10;
            this.lblRole.Text = "Должность:";
            // 
            // tbRole
            // 
            this.tbRole.Location = new System.Drawing.Point(86, 12);
            this.tbRole.MaxLength = 30;
            this.tbRole.Name = "tbRole";
            this.tbRole.Size = new System.Drawing.Size(306, 20);
            this.tbRole.TabIndex = 22;
            // 
            // cbPrivileged
            // 
            this.cbPrivileged.AutoSize = true;
            this.cbPrivileged.Location = new System.Drawing.Point(15, 47);
            this.cbPrivileged.Name = "cbPrivileged";
            this.cbPrivileged.Size = new System.Drawing.Size(131, 17);
            this.cbPrivileged.TabIndex = 23;
            this.cbPrivileged.Text = "Привилегированный";
            this.cbPrivileged.UseVisualStyleBackColor = true;
            // 
            // FormRole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tan;
            this.ClientSize = new System.Drawing.Size(447, 114);
            this.Controls.Add(this.cbPrivileged);
            this.Controls.Add(this.tbRole);
            this.Controls.Add(this.lblRole);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormRole";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormRole_FormClosing);
            this.Load += new System.EventHandler(this.FormRole_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox tbRole;
        public System.Windows.Forms.Label lblRole;
        private System.Windows.Forms.CheckBox cbPrivileged;
    }
}
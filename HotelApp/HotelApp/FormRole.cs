﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelApp
{
    public partial class FormRole : Form
    {
        public FormRole()
        {
            InitializeComponent();
        }

        static dbHotelEntities db = new dbHotelEntities();
        public int index = -1;
        private bool exit = false;

        private void FormRole_Load(object sender, EventArgs e)
        {
            if (this.Text == "Редактирование должности")
            {
                tbRole.Text = db.RoleTables.FirstOrDefault(n => n.RoleId == index).Role;
                cbPrivileged.Checked = db.RoleTables.FirstOrDefault(n => n.RoleId == index).Privileged;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tbRole.Text))
            {
                if (db.RoleTables.FirstOrDefault(n => n.Role == tbRole.Text && n.RoleId != index) == null)
                {
                    if (this.Text == "Добавление должности")
                    {
                        RoleTable role = new RoleTable
                        {
                            Role = tbRole.Text,
                            Privileged = cbPrivileged.Checked
                        };
                        db.RoleTables.Add(role);
                        db.SaveChanges();
                        this.DialogResult = DialogResult.OK;
                        MessageBox.Show("Должность добавлена.", "Внимание!",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else if (this.Text == "Редактирование должности")
                    {
                        var role = db.RoleTables.FirstOrDefault(n => n.RoleId == index);
                        role.Role = tbRole.Text;
                        role.Privileged = cbPrivileged.Checked;
                        db.SaveChanges();
                        this.DialogResult = DialogResult.OK;
                        MessageBox.Show("Данные успешно изменены.", "Внимание!",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    exit = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Данная запись уже существует", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Заполните поле", "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormRole_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }
}

﻿namespace HotelApp
{
    partial class FormRooms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRooms));
            this.btnSave = new System.Windows.Forms.Button();
            this.lblComfort = new System.Windows.Forms.Label();
            this.lblAccommodation = new System.Windows.Forms.Label();
            this.lblNumberName = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.cbAccommodation = new System.Windows.Forms.ComboBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.cbComfort = new System.Windows.Forms.ComboBox();
            this.nudPrice = new System.Windows.Forms.NumericUpDown();
            this.btnEditComfort = new System.Windows.Forms.Button();
            this.btnEditAccommodation = new System.Windows.Forms.Button();
            this.btnDeleteComfort = new System.Windows.Forms.Button();
            this.btnDeleteAccommodation = new System.Windows.Forms.Button();
            this.btnAddComfort = new System.Windows.Forms.Button();
            this.btnAddAccommodation = new System.Windows.Forms.Button();
            this.ttPrompt = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.nudPrice)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Sienna;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(188, 163);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblComfort
            // 
            this.lblComfort.AutoSize = true;
            this.lblComfort.Location = new System.Drawing.Point(60, 84);
            this.lblComfort.Name = "lblComfort";
            this.lblComfort.Size = new System.Drawing.Size(56, 13);
            this.lblComfort.TabIndex = 8;
            this.lblComfort.Text = "Комфорт:";
            // 
            // lblAccommodation
            // 
            this.lblAccommodation.AutoSize = true;
            this.lblAccommodation.Location = new System.Drawing.Point(39, 51);
            this.lblAccommodation.Name = "lblAccommodation";
            this.lblAccommodation.Size = new System.Drawing.Size(76, 13);
            this.lblAccommodation.TabIndex = 9;
            this.lblAccommodation.Text = "Размещение:";
            // 
            // lblNumberName
            // 
            this.lblNumberName.AutoSize = true;
            this.lblNumberName.Location = new System.Drawing.Point(14, 20);
            this.lblNumberName.Name = "lblNumberName";
            this.lblNumberName.Size = new System.Drawing.Size(101, 13);
            this.lblNumberName.TabIndex = 10;
            this.lblNumberName.Text = "Название номера:";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(33, 119);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(82, 13);
            this.lblPrice.TabIndex = 15;
            this.lblPrice.Text = "Цена за сутки:";
            // 
            // cbAccommodation
            // 
            this.cbAccommodation.BackColor = System.Drawing.Color.White;
            this.cbAccommodation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAccommodation.FormattingEnabled = true;
            this.cbAccommodation.Location = new System.Drawing.Point(124, 48);
            this.cbAccommodation.Name = "cbAccommodation";
            this.cbAccommodation.Size = new System.Drawing.Size(236, 21);
            this.cbAccommodation.TabIndex = 1;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(124, 17);
            this.tbName.MaxLength = 30;
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(192, 20);
            this.tbName.TabIndex = 0;
            // 
            // cbComfort
            // 
            this.cbComfort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbComfort.FormattingEnabled = true;
            this.cbComfort.Location = new System.Drawing.Point(124, 81);
            this.cbComfort.Name = "cbComfort";
            this.cbComfort.Size = new System.Drawing.Size(236, 21);
            this.cbComfort.TabIndex = 2;
            // 
            // nudPrice
            // 
            this.nudPrice.DecimalPlaces = 2;
            this.nudPrice.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudPrice.Location = new System.Drawing.Point(124, 119);
            this.nudPrice.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudPrice.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            131072});
            this.nudPrice.Name = "nudPrice";
            this.nudPrice.Size = new System.Drawing.Size(100, 20);
            this.nudPrice.TabIndex = 3;
            this.nudPrice.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // btnEditComfort
            // 
            this.btnEditComfort.FlatAppearance.BorderSize = 0;
            this.btnEditComfort.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditComfort.Image = global::HotelApp.Properties.Resources.edit;
            this.btnEditComfort.Location = new System.Drawing.Point(395, 78);
            this.btnEditComfort.Name = "btnEditComfort";
            this.btnEditComfort.Size = new System.Drawing.Size(26, 26);
            this.btnEditComfort.TabIndex = 9;
            this.ttPrompt.SetToolTip(this.btnEditComfort, "Редактировать");
            this.btnEditComfort.UseVisualStyleBackColor = true;
            this.btnEditComfort.Click += new System.EventHandler(this.btnEditComfort_Click);
            // 
            // btnEditAccommodation
            // 
            this.btnEditAccommodation.FlatAppearance.BorderSize = 0;
            this.btnEditAccommodation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditAccommodation.Image = global::HotelApp.Properties.Resources.edit;
            this.btnEditAccommodation.Location = new System.Drawing.Point(395, 45);
            this.btnEditAccommodation.Name = "btnEditAccommodation";
            this.btnEditAccommodation.Size = new System.Drawing.Size(26, 26);
            this.btnEditAccommodation.TabIndex = 6;
            this.ttPrompt.SetToolTip(this.btnEditAccommodation, "Редактировать");
            this.btnEditAccommodation.UseVisualStyleBackColor = true;
            this.btnEditAccommodation.Click += new System.EventHandler(this.btnEditAccommodation_Click);
            // 
            // btnDeleteComfort
            // 
            this.btnDeleteComfort.FlatAppearance.BorderSize = 0;
            this.btnDeleteComfort.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteComfort.Image = global::HotelApp.Properties.Resources.delete;
            this.btnDeleteComfort.Location = new System.Drawing.Point(425, 79);
            this.btnDeleteComfort.Name = "btnDeleteComfort";
            this.btnDeleteComfort.Size = new System.Drawing.Size(26, 26);
            this.btnDeleteComfort.TabIndex = 10;
            this.ttPrompt.SetToolTip(this.btnDeleteComfort, "Удалить");
            this.btnDeleteComfort.UseVisualStyleBackColor = true;
            this.btnDeleteComfort.Click += new System.EventHandler(this.btnDeleteComfort_Click);
            // 
            // btnDeleteAccommodation
            // 
            this.btnDeleteAccommodation.FlatAppearance.BorderSize = 0;
            this.btnDeleteAccommodation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteAccommodation.Image = global::HotelApp.Properties.Resources.delete;
            this.btnDeleteAccommodation.Location = new System.Drawing.Point(425, 46);
            this.btnDeleteAccommodation.Name = "btnDeleteAccommodation";
            this.btnDeleteAccommodation.Size = new System.Drawing.Size(26, 26);
            this.btnDeleteAccommodation.TabIndex = 7;
            this.ttPrompt.SetToolTip(this.btnDeleteAccommodation, "Удалить");
            this.btnDeleteAccommodation.UseVisualStyleBackColor = true;
            this.btnDeleteAccommodation.Click += new System.EventHandler(this.btnDeleteAccommodation_Click);
            // 
            // btnAddComfort
            // 
            this.btnAddComfort.FlatAppearance.BorderSize = 0;
            this.btnAddComfort.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddComfort.Image = global::HotelApp.Properties.Resources.add;
            this.btnAddComfort.Location = new System.Drawing.Point(365, 78);
            this.btnAddComfort.Name = "btnAddComfort";
            this.btnAddComfort.Size = new System.Drawing.Size(26, 26);
            this.btnAddComfort.TabIndex = 8;
            this.ttPrompt.SetToolTip(this.btnAddComfort, "Добавить");
            this.btnAddComfort.UseVisualStyleBackColor = true;
            this.btnAddComfort.Click += new System.EventHandler(this.btnAddComfort_Click);
            // 
            // btnAddAccommodation
            // 
            this.btnAddAccommodation.FlatAppearance.BorderSize = 0;
            this.btnAddAccommodation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddAccommodation.Image = global::HotelApp.Properties.Resources.add;
            this.btnAddAccommodation.Location = new System.Drawing.Point(365, 45);
            this.btnAddAccommodation.Name = "btnAddAccommodation";
            this.btnAddAccommodation.Size = new System.Drawing.Size(26, 26);
            this.btnAddAccommodation.TabIndex = 5;
            this.btnAddAccommodation.Tag = "";
            this.ttPrompt.SetToolTip(this.btnAddAccommodation, "Добавить");
            this.btnAddAccommodation.UseVisualStyleBackColor = true;
            this.btnAddAccommodation.Click += new System.EventHandler(this.btnAddAccommodation_Click);
            // 
            // FormRooms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tan;
            this.ClientSize = new System.Drawing.Size(481, 207);
            this.Controls.Add(this.btnEditComfort);
            this.Controls.Add(this.btnEditAccommodation);
            this.Controls.Add(this.btnDeleteComfort);
            this.Controls.Add(this.btnDeleteAccommodation);
            this.Controls.Add(this.btnAddComfort);
            this.Controls.Add(this.btnAddAccommodation);
            this.Controls.Add(this.nudPrice);
            this.Controls.Add(this.cbComfort);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.cbAccommodation);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.lblNumberName);
            this.Controls.Add(this.lblAccommodation);
            this.Controls.Add(this.lblComfort);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormRooms";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление номера";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormRooms_FormClosing);
            this.Load += new System.EventHandler(this.FormRooms_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudPrice)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblComfort;
        private System.Windows.Forms.Label lblAccommodation;
        private System.Windows.Forms.Label lblNumberName;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.ComboBox cbAccommodation;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.ComboBox cbComfort;
        private System.Windows.Forms.NumericUpDown nudPrice;
        private System.Windows.Forms.Button btnAddAccommodation;
        private System.Windows.Forms.Button btnAddComfort;
        private System.Windows.Forms.Button btnDeleteAccommodation;
        private System.Windows.Forms.Button btnDeleteComfort;
        private System.Windows.Forms.Button btnEditAccommodation;
        private System.Windows.Forms.Button btnEditComfort;
        private System.Windows.Forms.ToolTip ttPrompt;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelApp
{
    public partial class FormRooms : Form
    {
        public FormRooms()
        {
            InitializeComponent();
        }

        static dbHotelEntities db = new dbHotelEntities();
        public int index = -1;
        private bool exit = false;

        private void FormRooms_Load(object sender, EventArgs e)
        {
            db = new dbHotelEntities();
            cbAccommodation.DataSource = db.TypeAccommodationTables.Select(n => n.Accommodation).ToList();
            cbComfort.DataSource = db.TypeComfortTables.Select(n => n.Comfort).ToList();
            if (this.Text == "Изменение информации о номере")
            {
                var room = db.RoomTables.FirstOrDefault(n => n.RoomId == index);
                tbName.Text = room.RoomNumber;
                cbAccommodation.Text = room.TypeAccommodationTable.Accommodation;
                cbComfort.Text = room.TypeComfortTable.Comfort;
                nudPrice.Value = room.PriceperDay;
            }
            else
            {
                cbAccommodation.SelectedIndex = 0;
                cbComfort.SelectedIndex = 0;
            }
        }

        private void btnAddAccommodation_Click(object sender, EventArgs e)
        {
            FormAddReferenceInfo referenceInfo = new FormAddReferenceInfo();
            referenceInfo.Text = "Добавление типа размещения";
            referenceInfo.lblRoomFeatures.Text = "Тип размещения:";
            referenceInfo.ShowDialog();
            if(referenceInfo.DialogResult == DialogResult.OK)
            {
                cbAccommodation.DataSource = db.TypeAccommodationTables.Select(n => n.Accommodation).ToList();
                cbAccommodation.SelectedItem = db.TypeAccommodationTables
                    .FirstOrDefault(k => k.AccommodationId == (db.TypeAccommodationTables.Max(n => n.AccommodationId))).Accommodation;
            }
        }

        private void btnAddComfort_Click(object sender, EventArgs e)
        {
            FormAddReferenceInfo referenceInfo = new FormAddReferenceInfo();
            referenceInfo.Text = "Добавление уровня комфорта";
            referenceInfo.lblRoomFeatures.Text = "Уровень комфорта:";
            referenceInfo.ShowDialog();
            if (referenceInfo.DialogResult == DialogResult.OK)
            {
                cbComfort.DataSource = db.TypeComfortTables.Select(n => n.Comfort).ToList();
                cbComfort.SelectedItem = db.TypeComfortTables
                    .FirstOrDefault(k => k.ComfortId == (db.TypeComfortTables.Max(n => n.ComfortId))).Comfort;
            }
        }

        private void btnEditAccommodation_Click(object sender, EventArgs e)
        {
            int selectedIndex = cbAccommodation.SelectedIndex;
            FormAddReferenceInfo referenceInfo = new FormAddReferenceInfo();
            referenceInfo.Text = "Редактирование типа размещения";
            referenceInfo.lblRoomFeatures.Text = "Тип размещения:";
            referenceInfo.index = db.TypeAccommodationTables.FirstOrDefault(n => n.Accommodation == cbAccommodation.Text).AccommodationId;
            referenceInfo.ShowDialog();
            if (referenceInfo.DialogResult == DialogResult.OK)
            {
                cbAccommodation.DataSource = db.TypeAccommodationTables.Select(n => n.Accommodation).ToList();
                cbAccommodation.SelectedIndex = selectedIndex;
            }
        }

        private void btnEditComfort_Click(object sender, EventArgs e)
        {
            int selectedIndex = cbComfort.SelectedIndex;
            FormAddReferenceInfo referenceInfo = new FormAddReferenceInfo();
            referenceInfo.Text = "Редактирование уровня комфорта";
            referenceInfo.lblRoomFeatures.Text = "Уровень комфорта:";
            referenceInfo.index = db.TypeComfortTables.FirstOrDefault(n => n.Comfort == cbComfort.Text).ComfortId;
            referenceInfo.ShowDialog();
            if (referenceInfo.DialogResult == DialogResult.OK)
            {
                cbComfort.DataSource = db.TypeComfortTables.Select(n => n.Comfort).ToList();
                cbComfort.SelectedIndex = selectedIndex;
            }
        }

        private void btnDeleteAccommodation_Click(object sender, EventArgs e)
        {
            if (db.RoomTables.FirstOrDefault(n => n.TypeAccommodationTable.Accommodation == cbAccommodation.Text
            && n.RoomId != index) == null)
            {
                if ((MessageBox.Show("Вы уверены, что хотите удалить запись?", "Внимание!",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    if (db.RoomTables.FirstOrDefault(n => n.TypeAccommodationTable.Accommodation == cbAccommodation.Text
                    && n.RoomId == index) != null)
                    {
                        var room = db.RoomTables.FirstOrDefault(n => n.TypeAccommodationTable.Accommodation == cbAccommodation.Text
                            && n.RoomId == index);
                        room.TypeAccommodationId = db.TypeAccommodationTables.FirstOrDefault(n => n.Accommodation != cbAccommodation.Text).AccommodationId;
                    }
                    db.SaveChanges();

                    db.TypeAccommodationTables.Remove(db.TypeAccommodationTables
                        .FirstOrDefault(n => n.Accommodation == cbAccommodation.Text));
                    db.SaveChanges();
                    cbAccommodation.DataSource = db.TypeAccommodationTables.Select(n => n.Accommodation).ToList();
                }
            }
            else
            {
                MessageBox.Show("Невозможно удалить!", "Ошибка!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDeleteComfort_Click(object sender, EventArgs e)
        {
            if (db.RoomTables.FirstOrDefault(n => n.TypeComfortTable.Comfort == cbComfort.Text
            && n.RoomId != index) == null)
            {
                if ((MessageBox.Show("Вы уверены, что хотите удалить запись?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    if (db.RoomTables.FirstOrDefault(n => n.TypeComfortTable.Comfort == cbComfort.Text 
                    && n.RoomId == index) != null)
                    {
                        var room = db.RoomTables.FirstOrDefault(n => n.TypeComfortTable.Comfort == cbComfort.Text
                            && n.RoomId == index);
                        room.TypeComfortId = db.TypeComfortTables.FirstOrDefault(n => n.Comfort != cbComfort.Text).ComfortId;
                    }
                    db.SaveChanges();
                    db.TypeComfortTables.Remove(db.TypeComfortTables
                        .FirstOrDefault(n => n.Comfort == cbComfort.Text));
                    db.SaveChanges();
                    cbComfort.DataSource = db.TypeComfortTables.Select(n => n.Comfort).ToList();
                }
            }
            else
            {
                MessageBox.Show("Невозможно удалить!", "Ошибка!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tbName.Text))
            {
                if (db.RoomTables.FirstOrDefault(n => n.RoomNumber == tbName.Text && n.RoomId != index) == null)
                {
                    if (this.Text == "Добавление номера")
                    {
                        RoomTable room = new RoomTable
                        {
                            RoomNumber = tbName.Text,
                            TypeAccommodationId = db.TypeAccommodationTables
                            .FirstOrDefault(n => n.Accommodation == cbAccommodation.Text).AccommodationId,
                            TypeComfortId = db.TypeComfortTables.FirstOrDefault(n => n.Comfort == cbComfort.Text).ComfortId,
                            PriceperDay = nudPrice.Value

                        };
                        db.RoomTables.Add(room);
                        db.SaveChanges();
                        MessageBox.Show("Номер добавлен.", "Внимание!", 
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else if (this.Text == "Изменение информации о номере")
                    {
                        var room = db.RoomTables.FirstOrDefault(n => n.RoomId == index);
                        room.RoomNumber = tbName.Text;
                        room.TypeAccommodationId = db.TypeAccommodationTables
                            .FirstOrDefault(n => n.Accommodation == cbAccommodation.Text).AccommodationId;
                        room.TypeComfortId = db.TypeComfortTables.FirstOrDefault(n => n.Comfort == cbComfort.Text).ComfortId;
                        room.PriceperDay = nudPrice.Value;
                        db.SaveChanges();
                        MessageBox.Show("Данные успешно изменены.", "Внимание!", 
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    this.DialogResult = DialogResult.OK;
                    exit = true;
                    this.Close();
                } else
                {
                    MessageBox.Show("Номер с таким названием уже существует.", "Ошибка", 
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Заполните все поля", "Ошибка", MessageBoxButtons.OK, 
                    MessageBoxIcon.Error);
            }
        }

        private void FormRooms_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }
}

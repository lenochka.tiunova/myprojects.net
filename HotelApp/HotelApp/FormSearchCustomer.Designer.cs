﻿namespace HotelApp
{
    partial class FormSearchCustomer
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSearchHotelStay));
            this.menuCustomer = new System.Windows.Forms.MenuStrip();
            this.miEditing = new System.Windows.Forms.ToolStripMenuItem();
            this.miAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.miEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.miDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvShowCustomer = new System.Windows.Forms.DataGridView();
            this.tbNumberPhone = new System.Windows.Forms.TextBox();
            this.saveExcel = new System.Windows.Forms.SaveFileDialog();
            this.saveWord = new System.Windows.Forms.SaveFileDialog();
            this.tbFullName = new System.Windows.Forms.TextBox();
            this.lblNumberPhone = new System.Windows.Forms.Label();
            this.lblFullName = new System.Windows.Forms.Label();
            this.lblPrompt = new System.Windows.Forms.Label();
            this.menuCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShowCustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // menuCustomer
            // 
            this.menuCustomer.BackColor = System.Drawing.Color.Peru;
            this.menuCustomer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miEditing});
            this.menuCustomer.Location = new System.Drawing.Point(0, 0);
            this.menuCustomer.Name = "menuCustomer";
            this.menuCustomer.Size = new System.Drawing.Size(872, 24);
            this.menuCustomer.TabIndex = 0;
            this.menuCustomer.Text = "menuStrip1";
            // 
            // miEditing
            // 
            this.miEditing.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAdd,
            this.miEdit,
            this.miDelete});
            this.miEditing.Name = "miEditing";
            this.miEditing.Size = new System.Drawing.Size(108, 20);
            this.miEditing.Text = "Редактирование";
            // 
            // miAdd
            // 
            this.miAdd.Name = "miAdd";
            this.miAdd.Size = new System.Drawing.Size(154, 22);
            this.miAdd.Text = "Добавить";
            this.miAdd.Click += new System.EventHandler(this.miAdd_Click);
            // 
            // miEdit
            // 
            this.miEdit.Name = "miEdit";
            this.miEdit.Size = new System.Drawing.Size(154, 22);
            this.miEdit.Text = "Редактировать";
            this.miEdit.Click += new System.EventHandler(this.miEdit_Click);
            // 
            // miDelete
            // 
            this.miDelete.Name = "miDelete";
            this.miDelete.Size = new System.Drawing.Size(154, 22);
            this.miDelete.Text = "Удалить";
            this.miDelete.Click += new System.EventHandler(this.miDelete_Click);
            // 
            // dgvShowCustomer
            // 
            this.dgvShowCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvShowCustomer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvShowCustomer.BackgroundColor = System.Drawing.Color.Tan;
            this.dgvShowCustomer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvShowCustomer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvShowCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.Format = "f";
            dataGridViewCellStyle2.NullValue = null;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvShowCustomer.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvShowCustomer.Location = new System.Drawing.Point(13, 63);
            this.dgvShowCustomer.MultiSelect = false;
            this.dgvShowCustomer.Name = "dgvShowCustomer";
            this.dgvShowCustomer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvShowCustomer.Size = new System.Drawing.Size(847, 430);
            this.dgvShowCustomer.TabIndex = 1;
            this.dgvShowCustomer.TabStop = false;
            this.dgvShowCustomer.DoubleClick += new System.EventHandler(this.dgvShowCustomer_DoubleClick);
            // 
            // tbNumberPhone
            // 
            this.tbNumberPhone.Location = new System.Drawing.Point(165, 30);
            this.tbNumberPhone.MaxLength = 20;
            this.tbNumberPhone.Name = "tbNumberPhone";
            this.tbNumberPhone.Size = new System.Drawing.Size(151, 20);
            this.tbNumberPhone.TabIndex = 0;
            this.tbNumberPhone.TextChanged += new System.EventHandler(this.tbNumberPhone_TextChanged);
            // 
            // tbFullName
            // 
            this.tbFullName.Location = new System.Drawing.Point(431, 30);
            this.tbFullName.MaxLength = 150;
            this.tbFullName.Name = "tbFullName";
            this.tbFullName.Size = new System.Drawing.Size(221, 20);
            this.tbFullName.TabIndex = 1;
            this.tbFullName.TextChanged += new System.EventHandler(this.tbName_TextChanged);
            // 
            // lblNumberPhone
            // 
            this.lblNumberPhone.AutoSize = true;
            this.lblNumberPhone.Location = new System.Drawing.Point(10, 33);
            this.lblNumberPhone.Name = "lblNumberPhone";
            this.lblNumberPhone.Size = new System.Drawing.Size(149, 13);
            this.lblNumberPhone.TabIndex = 11;
            this.lblNumberPhone.Text = "Поиск по номеру телефона:";
            // 
            // lblFullName
            // 
            this.lblFullName.AutoSize = true;
            this.lblFullName.Location = new System.Drawing.Point(333, 33);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.Size = new System.Drawing.Size(92, 13);
            this.lblFullName.TabIndex = 12;
            this.lblFullName.Text = "Поиск по имени:";
            // 
            // lblPrompt
            // 
            this.lblPrompt.AutoSize = true;
            this.lblPrompt.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPrompt.Location = new System.Drawing.Point(336, 500);
            this.lblPrompt.Name = "lblPrompt";
            this.lblPrompt.Size = new System.Drawing.Size(209, 12);
            this.lblPrompt.TabIndex = 13;
            this.lblPrompt.Text = "Для выбора клиента сделайте двойной щелчок";
            // 
            // FormSearchCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tan;
            this.ClientSize = new System.Drawing.Size(872, 518);
            this.Controls.Add(this.lblPrompt);
            this.Controls.Add(this.lblFullName);
            this.Controls.Add(this.lblNumberPhone);
            this.Controls.Add(this.tbNumberPhone);
            this.Controls.Add(this.dgvShowCustomer);
            this.Controls.Add(this.tbFullName);
            this.Controls.Add(this.menuCustomer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuCustomer;
            this.MaximizeBox = false;
            this.Name = "FormSearchCustomer";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Выбор клиента";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSearchCustomer_FormClosing);
            this.Load += new System.EventHandler(this.FormSearchCustomer_Load);
            this.menuCustomer.ResumeLayout(false);
            this.menuCustomer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShowCustomer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuCustomer;
        private System.Windows.Forms.DataGridView dgvShowCustomer;
        private System.Windows.Forms.ToolStripMenuItem miEditing;
        private System.Windows.Forms.ToolStripMenuItem miEdit;
        private System.Windows.Forms.ToolStripMenuItem miAdd;
        private System.Windows.Forms.TextBox tbNumberPhone;
        private System.Windows.Forms.SaveFileDialog saveExcel;
        private System.Windows.Forms.SaveFileDialog saveWord;
        private System.Windows.Forms.ToolStripMenuItem miDelete;
        private System.Windows.Forms.TextBox tbFullName;
        private System.Windows.Forms.Label lblNumberPhone;
        private System.Windows.Forms.Label lblFullName;
        private System.Windows.Forms.Label lblPrompt;
    }
}


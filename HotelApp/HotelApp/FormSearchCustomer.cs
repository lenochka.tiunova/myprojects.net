﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using Word = Microsoft.Office.Interop.Word;

namespace HotelApp
{
    public partial class FormSearchCustomer : Form
    {
        static dbHotelEntities db = new dbHotelEntities();
        public int customerIndex = -1;
        bool exit = false;

        public FormSearchCustomer()
        {
            InitializeComponent();
        }

        private void FormSearchCustomer_Load(object sender, EventArgs e)
        {
            UpdateCustomer();
            if (customerIndex != -1)
            {
                for(int i = 0; i < dgvShowCustomer.Rows.Count; i++)
                {
                    if(Convert.ToInt32(dgvShowCustomer[0, i].Value) == customerIndex)
                    {
                        dgvShowCustomer.CurrentCell = dgvShowCustomer[1, i];
                    }
                }
            }
        }

        private void UpdateCustomer()
        {
            miAdd.Text = "Зарегистрировать клиента";
            miEdit.Text = "Редактировать данные о клиенте";
            miDelete.Text = "Удалить данные о клиенте";
            tbFullName.Clear();
            tbNumberPhone.Clear();
            FilterCustomers();
        }

        private void FilterCustomers()
        {
            var search = db.CustomerTables
                .Select(n => new
                {
                    n.CustomerId,
                    n.Surname,
                    n.Name,
                    n.Middle,
                    n.Gender,
                    n.DateOfBirthday,
                    n.PlaceBirth,
                    n.RegistrationAddress,
                    n.ResidentialAddress,
                    n.ContactNumber,
                    n.Сitizenship,
                    n.DocumentType,
                    n.Series,
                    n.Number,
                    n.IssuedBy,
                    n.DateOfIssue
                });

            search = search
           .Where(n => n.ContactNumber.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").StartsWith(tbNumberPhone.Text)
           || n.ContactNumber.Replace("+7", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").StartsWith(tbNumberPhone.Text)
           || n.ContactNumber.Replace("+7", "8").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").StartsWith(tbNumberPhone.Text)
           || n.ContactNumber.Remove(0, 1).Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").StartsWith(tbNumberPhone.Text)
           || n.ContactNumber.Remove(0, 1).Insert(0, "+7").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").StartsWith(tbNumberPhone.Text));

            search = search.Where(n => String.Concat(n.Surname, " ", n.Name, " ", n.Middle).Contains(tbFullName.Text));

            dgvShowCustomer.DataSource = search.OrderByDescending(n => n.CustomerId).ToList();
            InterfaceDataGridViewForCustomer();
        }

        public void InterfaceDataGridViewForCustomer()
        {
            dgvShowCustomer.Columns[0].Visible = false;
            dgvShowCustomer.Columns[1].HeaderText = "Фамилия";
            dgvShowCustomer.Columns[2].HeaderText = "Имя";
            dgvShowCustomer.Columns[3].HeaderText = "Отчество";
            dgvShowCustomer.Columns[4].HeaderText = "Пол";
            dgvShowCustomer.Columns[5].HeaderText = "Дата рождения";
            dgvShowCustomer.Columns[5].DefaultCellStyle.Format = "dd.MM.yyyy";
            dgvShowCustomer.Columns[6].HeaderText = "Место рождения";
            dgvShowCustomer.Columns[7].HeaderText = "Адрес регистрации";
            dgvShowCustomer.Columns[8].HeaderText = "Адрес проживания";
            dgvShowCustomer.Columns[9].HeaderText = "Телефон";
            dgvShowCustomer.Columns[10].HeaderText = "Гражданство";
            dgvShowCustomer.Columns[11].HeaderText = "Тип документа";
            dgvShowCustomer.Columns[12].HeaderText = "Серия";
            dgvShowCustomer.Columns[13].HeaderText = "Номер";
            dgvShowCustomer.Columns[14].HeaderText = "Кем выдан";
            dgvShowCustomer.Columns[15].HeaderText = "Дата выдачи";
            dgvShowCustomer.Columns[15].DefaultCellStyle.Format = "dd.MM.yyyy";
        }

        private void tbNumberPhone_TextChanged(object sender, EventArgs e)
        {
            FilterCustomers();
        }

        private void tbName_TextChanged(object sender, EventArgs e)
        {
            FilterCustomers();
        }

        private void miAdd_Click(object sender, EventArgs e)
        {
            FormCustomers customers = new FormCustomers();
            customers.Text = "Регистрация клиента";
            customers.ShowDialog();
            if (customers.DialogResult == DialogResult.OK)
            {
                UpdateCustomer();
            }
        }

        private void miEdit_Click(object sender, EventArgs e)
        {
            int currRow = dgvShowCustomer.CurrentRow.Index;
            FormCustomers customers = new FormCustomers();
            customers.Text = "Редактирование данных о клиенте";
            customers.index = int.Parse(dgvShowCustomer[0, dgvShowCustomer.CurrentRow.Index].Value.ToString());
            customers.ShowDialog();
            if (customers.DialogResult == DialogResult.OK)
            {
                UpdateCustomer();
            }
            dgvShowCustomer.CurrentCell = dgvShowCustomer[1, currRow];
        }

        private void miDelete_Click(object sender, EventArgs e)
        {
            var index = int.Parse(dgvShowCustomer[0, dgvShowCustomer.CurrentRow.Index].Value.ToString());
            var rec = db.CustomerTables.FirstOrDefault(n => n.CustomerId == index);
            db.CustomerTables.Remove(rec);
            db.SaveChanges();
            UpdateCustomer();
        }

        private int customer = 0;

        private void dgvShowCustomer_DoubleClick(object sender, EventArgs e)
        {
            customer = int.Parse(dgvShowCustomer[0, dgvShowCustomer.CurrentRow.Index].Value.ToString());
            DialogResult = DialogResult.OK;
            exit = true;
            this.Close();
        }

        public int ReturnData()
        {
            return customer;
        }

        private void FormSearchCustomer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
﻿namespace HotelApp
{
    partial class FormSearchHotelStay
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSearchHotelStay));
            this.dgvShowHotelStay = new System.Windows.Forms.DataGridView();
            this.tbNumber = new System.Windows.Forms.TextBox();
            this.saveExcel = new System.Windows.Forms.SaveFileDialog();
            this.saveWord = new System.Windows.Forms.SaveFileDialog();
            this.lblNumber = new System.Windows.Forms.Label();
            this.lblPrompt = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShowHotelStay)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvShowHotelStay
            // 
            this.dgvShowHotelStay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvShowHotelStay.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvShowHotelStay.BackgroundColor = System.Drawing.Color.Tan;
            this.dgvShowHotelStay.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvShowHotelStay.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvShowHotelStay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.Format = "f";
            dataGridViewCellStyle2.NullValue = null;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvShowHotelStay.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvShowHotelStay.Location = new System.Drawing.Point(3, 45);
            this.dgvShowHotelStay.MultiSelect = false;
            this.dgvShowHotelStay.Name = "dgvShowHotelStay";
            this.dgvShowHotelStay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvShowHotelStay.Size = new System.Drawing.Size(868, 430);
            this.dgvShowHotelStay.TabIndex = 1;
            this.dgvShowHotelStay.TabStop = false;
            this.dgvShowHotelStay.DoubleClick += new System.EventHandler(this.dgvShowHotelStay_DoubleClick);
            // 
            // tbNumber
            // 
            this.tbNumber.Location = new System.Drawing.Point(111, 12);
            this.tbNumber.MaxLength = 20;
            this.tbNumber.Name = "tbNumber";
            this.tbNumber.Size = new System.Drawing.Size(151, 20);
            this.tbNumber.TabIndex = 0;
            // 
            // lblNumber
            // 
            this.lblNumber.AutoSize = true;
            this.lblNumber.Location = new System.Drawing.Point(10, 15);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(97, 13);
            this.lblNumber.TabIndex = 11;
            this.lblNumber.Text = "Поиск по номеру:";
            // 
            // lblPrompt
            // 
            this.lblPrompt.AutoSize = true;
            this.lblPrompt.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPrompt.Location = new System.Drawing.Point(336, 500);
            this.lblPrompt.Name = "lblPrompt";
            this.lblPrompt.Size = new System.Drawing.Size(209, 12);
            this.lblPrompt.TabIndex = 13;
            this.lblPrompt.Text = "Для выбора клиента сделайте двойной щелчок";
            // 
            // FormSearchHotelStay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tan;
            this.ClientSize = new System.Drawing.Size(872, 518);
            this.Controls.Add(this.lblPrompt);
            this.Controls.Add(this.lblNumber);
            this.Controls.Add(this.tbNumber);
            this.Controls.Add(this.dgvShowHotelStay);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormSearchHotelStay";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Выбор услуги";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSearchHotelStay_FormClosing);
            this.Load += new System.EventHandler(this.FormSearchHotelStay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvShowHotelStay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvShowHotelStay;
        private System.Windows.Forms.TextBox tbNumber;
        private System.Windows.Forms.SaveFileDialog saveExcel;
        private System.Windows.Forms.SaveFileDialog saveWord;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.Label lblPrompt;
    }
}


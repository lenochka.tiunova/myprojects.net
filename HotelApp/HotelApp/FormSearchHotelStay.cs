﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using Word = Microsoft.Office.Interop.Word;

namespace HotelApp
{
    public partial class FormSearchHotelStay : Form
    {
        static dbHotelEntities db = new dbHotelEntities();
        public int settlementIndex = -1;
        bool exit = false;

        public FormSearchHotelStay()
        {
            InitializeComponent();
        }

        private void FormSearchHotelStay_Load(object sender, EventArgs e)
        {
            UpdateHotelStay();
            if (settlementIndex != -1)
            {
                for (int i = 0; i < dgvShowHotelStay.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dgvShowHotelStay[0, i].Value) == settlementIndex)
                    {
                        dgvShowHotelStay.CurrentCell = dgvShowHotelStay[1, i];
                    }
                }
            }
        }

        private void UpdateHotelStay()
        {
            tbNumber.Clear();
            FilterHotelStay();
        }

        private void FilterHotelStay()
        {
            var search = db.SettlementTables
                .Select(n => new
                {
                    n.SettlementId,
                    n.RoomTable.RoomNumber,
                    SettlementDate = n.HotelStayTables.Where(x => x.SettlementId == n.SettlementId).Min(x => x.SettlementDate),
                    CheckOutDate = n.HotelStayTables
                    .FirstOrDefault(x => x.SettlementId == n.SettlementId && x.CheckOutDate == null) == null ? 
                    n.HotelStayTables.Where(x => x.SettlementId == n.SettlementId).Max(x => x.CheckOutDate) :
                    n.HotelStayTables.FirstOrDefault(x => x.SettlementId == n.SettlementId && x.CheckOutDate == null).CheckOutDate,
                    n.PlannedСheckOutDate
                });

            DateTime dt = DateTime.Now.AddHours(-1);
            search = search.Where(n => n.CheckOutDate == null || n.CheckOutDate > dt);

            if (!String.IsNullOrEmpty(tbNumber.Text))
            {
                search = search.Where(n => n.RoomNumber == tbNumber.Text);
            }

            dgvShowHotelStay.DataSource = search.Select(n => new
            {
                n.SettlementId,
                n.RoomNumber,
                n.SettlementDate,
                n.PlannedСheckOutDate
            }).OrderByDescending(n => n.SettlementId).ToList();
            InterfaceForHotelStay();
        }

        public void InterfaceForHotelStay()
        {
            dgvShowHotelStay.Columns[0].Visible = false;
            dgvShowHotelStay.Columns[1].HeaderText = "Номер";
            dgvShowHotelStay.Columns[2].HeaderText = "Дата заселения номера";
            dgvShowHotelStay.Columns[2].DefaultCellStyle.Format = "dd MMMM yyyy г. HH:mm";
            dgvShowHotelStay.Columns[3].HeaderText = "Дата освобождения номера";
            dgvShowHotelStay.Columns[3].DefaultCellStyle.Format = "dd MMMM yyyy г. HH:mm";
        }

        private void tbFullName_TextChanged(object sender, EventArgs e)
        {
            FilterHotelStay();
        }

        private void dgvShowHotelStay_DoubleClick(object sender, EventArgs e)
        {
            settlementIndex = int.Parse(dgvShowHotelStay[0, dgvShowHotelStay.CurrentRow.Index].Value.ToString());
            DialogResult = DialogResult.OK;
            exit = true;
            this.Close();
        }

        public int ReturnData()
        {
            return settlementIndex;
        }

        private void FormSearchHotelStay_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
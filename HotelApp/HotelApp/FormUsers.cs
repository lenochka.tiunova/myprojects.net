﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotelApp
{
    public partial class FormUsers : Form
    {
        public FormUsers()
        {
            InitializeComponent();
        }

        static dbHotelEntities db = new dbHotelEntities();
        public int index = -1;
        private bool exit = false;

        private void FormUsers_Load(object sender, EventArgs e)
        {
            cbRole.DataSource = db.RoleTables.Select(n => n.Role).ToList();
            if (this.Text == "Редактирование данных о пользователе")
            {
                var user = db.UserTables.FirstOrDefault(n => n.UserId == index);
                tbSurname.Text = user.Surname;
                tbName.Text = user.Name;
                tbMiddle.Text = user.Middle;
                tbLogin.Text = user.Login;
                tbPassword.Text = user.Password;
                cbRole.Text = user.RoleTable.Role;
            }
            else
            {
                cbRole.SelectedIndex = 0;
            }
        }

        private void btnAddRole_Click(object sender, EventArgs e)
        {
            FormRole role = new FormRole();
            role.Text = "Добавление должности";
            role.ShowDialog();
            if (role.DialogResult == DialogResult.OK)
            {
                cbRole.DataSource = db.RoleTables.Select(n => n.Role).ToList();
                cbRole.SelectedItem = db.RoleTables
                    .FirstOrDefault(k => k.RoleId == (db.RoleTables.Max(n => n.RoleId))).Role;
            }
        }

        private void btnEditRole_Click(object sender, EventArgs e)
        {
            int selectedIndex = cbRole.SelectedIndex;
            FormRole role = new FormRole();
            role.Text = "Редактирование должности";
            role.index = db.RoleTables
                .FirstOrDefault(n => n.Role == cbRole.Text).RoleId;
            role.ShowDialog();
            if (role.DialogResult == DialogResult.OK)
            {
                cbRole.DataSource = db.RoleTables.Select(n => n.Role).ToList();
                cbRole.SelectedIndex = selectedIndex;
            }
        }

        private void btnDeleteRole_Click(object sender, EventArgs e)
        {
            if (db.UserTables.FirstOrDefault(n => n.RoleTable.Role == cbRole.Text && n.UserId != index) == null)
            {
                if ((MessageBox.Show("Вы уверены, что хотите удалить запись?", "Внимание!",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    if (db.UserTables.FirstOrDefault(n => n.RoleTable.Role == cbRole.Text && n.UserId == index) != null)
                    {
                        var user = db.UserTables.FirstOrDefault(n => n.RoleTable.Role == cbRole.Text && n.UserId == index);
                        user.RoleId = db.RoleTables.FirstOrDefault(n => n.Role != cbRole.Text).RoleId;
                    }
                    db.SaveChanges();
                    db.RoleTables.Remove(db.RoleTables
                        .FirstOrDefault(n => n.Role == cbRole.Text));
                    db.SaveChanges();
                    cbRole.DataSource = db.RoleTables.Select(n => n.Role).ToList();
                }
            }
            else
            {
                MessageBox.Show("Невозможно удалить!", "Ошибка!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tbName.Text)
                    && !String.IsNullOrEmpty(tbSurname.Text)
                    && !String.IsNullOrEmpty(tbLogin.Text)
                    && !String.IsNullOrEmpty(tbPassword.Text))
            {
                if (db.UserTables.FirstOrDefault(n => n.Login == tbLogin.Text && n.UserId != index) == null)
                {
                    if (this.Text == "Регистрация пользователя")
                    {
                        UserTable user = new UserTable
                        {
                            RoleId = db.RoleTables.FirstOrDefault(n => n.Role == cbRole.Text).RoleId,
                            Login = tbLogin.Text,
                            Password = tbPassword.Text,
                            Surname = tbSurname.Text,
                            Name = tbName.Text,
                            Middle = String.IsNullOrEmpty(tbMiddle.Text) ? null : tbMiddle.Text
                        };
                        db.UserTables.Add(user);
                        db.SaveChanges();
                        MessageBox.Show("Пользователь добавлен.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else if (this.Text == "Редактирование данных о пользователе")
                    {
                        var user = db.UserTables.FirstOrDefault(n => n.UserId == index);
                        user.Surname = tbSurname.Text;
                        user.Name = tbName.Text;
                        user.Middle = String.IsNullOrEmpty(tbMiddle.Text) ? null : tbMiddle.Text;
                        user.Login = tbLogin.Text;
                        user.Password = tbPassword.Text;
                        user.RoleId = db.RoleTables.FirstOrDefault(n => n.Role == cbRole.Text).RoleId;
                        db.SaveChanges();
                        MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    this.DialogResult = DialogResult.OK;
                    exit = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Пользователь с таким логином уже существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Заполните все поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormUsers_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
    }
}

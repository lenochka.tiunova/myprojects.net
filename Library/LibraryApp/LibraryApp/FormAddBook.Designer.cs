﻿
namespace LibraryApp
{
    partial class FormAddBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbNameBook = new System.Windows.Forms.TextBox();
            this.nudYear = new System.Windows.Forms.NumericUpDown();
            this.nudCount = new System.Windows.Forms.NumericUpDown();
            this.nudPrice = new System.Windows.Forms.NumericUpDown();
            this.lblNameBook = new System.Windows.Forms.Label();
            this.lblPublisher = new System.Windows.Forms.Label();
            this.lblYear = new System.Windows.Forms.Label();
            this.lblCountPages = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbAuthor = new System.Windows.Forms.CheckBox();
            this.lblName = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.lblSurname = new System.Windows.Forms.Label();
            this.tbSurname = new System.Windows.Forms.TextBox();
            this.lblMiddle = new System.Windows.Forms.Label();
            this.tbMiddle = new System.Windows.Forms.TextBox();
            this.gbAuthor = new System.Windows.Forms.GroupBox();
            this.cbPublisher = new System.Windows.Forms.ComboBox();
            this.cbCity = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrice)).BeginInit();
            this.gbAuthor.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbNameBook
            // 
            this.tbNameBook.Location = new System.Drawing.Point(139, 30);
            this.tbNameBook.MaxLength = 100;
            this.tbNameBook.Multiline = true;
            this.tbNameBook.Name = "tbNameBook";
            this.tbNameBook.Size = new System.Drawing.Size(255, 44);
            this.tbNameBook.TabIndex = 0;
            // 
            // nudYear
            // 
            this.nudYear.Location = new System.Drawing.Point(139, 171);
            this.nudYear.Maximum = new decimal(new int[] {
            1960,
            0,
            0,
            0});
            this.nudYear.Minimum = new decimal(new int[] {
            1960,
            0,
            0,
            0});
            this.nudYear.Name = "nudYear";
            this.nudYear.Size = new System.Drawing.Size(120, 20);
            this.nudYear.TabIndex = 3;
            this.nudYear.Value = new decimal(new int[] {
            1960,
            0,
            0,
            0});
            // 
            // nudCount
            // 
            this.nudCount.Location = new System.Drawing.Point(139, 205);
            this.nudCount.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudCount.Minimum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.nudCount.Name = "nudCount";
            this.nudCount.Size = new System.Drawing.Size(120, 20);
            this.nudCount.TabIndex = 4;
            this.nudCount.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // nudPrice
            // 
            this.nudPrice.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nudPrice.Location = new System.Drawing.Point(139, 240);
            this.nudPrice.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudPrice.Name = "nudPrice";
            this.nudPrice.Size = new System.Drawing.Size(120, 20);
            this.nudPrice.TabIndex = 5;
            this.nudPrice.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblNameBook
            // 
            this.lblNameBook.AutoSize = true;
            this.lblNameBook.Location = new System.Drawing.Point(30, 46);
            this.lblNameBook.Name = "lblNameBook";
            this.lblNameBook.Size = new System.Drawing.Size(92, 13);
            this.lblNameBook.TabIndex = 34;
            this.lblNameBook.Text = "Название книги:";
            // 
            // lblPublisher
            // 
            this.lblPublisher.AutoSize = true;
            this.lblPublisher.Location = new System.Drawing.Point(40, 92);
            this.lblPublisher.Name = "lblPublisher";
            this.lblPublisher.Size = new System.Drawing.Size(82, 13);
            this.lblPublisher.TabIndex = 35;
            this.lblPublisher.Text = "Издательство:";
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Location = new System.Drawing.Point(49, 173);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(73, 13);
            this.lblYear.TabIndex = 37;
            this.lblYear.Text = "Год издания:";
            // 
            // lblCountPages
            // 
            this.lblCountPages.AutoSize = true;
            this.lblCountPages.Location = new System.Drawing.Point(9, 207);
            this.lblCountPages.Name = "lblCountPages";
            this.lblCountPages.Size = new System.Drawing.Size(113, 13);
            this.lblCountPages.TabIndex = 38;
            this.lblCountPages.Text = "Количество страниц:";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(57, 242);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(65, 13);
            this.lblPrice.TabIndex = 39;
            this.lblPrice.Text = "Стоимость:";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(163, 432);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(82, 133);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Город:";
            // 
            // cbAuthor
            // 
            this.cbAuthor.AutoSize = true;
            this.cbAuthor.Location = new System.Drawing.Point(44, 269);
            this.cbAuthor.Name = "cbAuthor";
            this.cbAuthor.Size = new System.Drawing.Size(83, 17);
            this.cbAuthor.TabIndex = 10;
            this.cbAuthor.Text = "Нет автора";
            this.cbAuthor.UseVisualStyleBackColor = true;
            this.cbAuthor.CheckedChanged += new System.EventHandler(this.cbAuthor_CheckedChanged);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(37, 62);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(32, 13);
            this.lblName.TabIndex = 17;
            this.lblName.Text = "Имя:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(77, 59);
            this.tbName.MaxLength = 30;
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(255, 20);
            this.tbName.TabIndex = 7;
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(10, 36);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(59, 13);
            this.lblSurname.TabIndex = 15;
            this.lblSurname.Text = "Фамилия:";
            // 
            // tbSurname
            // 
            this.tbSurname.Location = new System.Drawing.Point(77, 33);
            this.tbSurname.MaxLength = 30;
            this.tbSurname.Name = "tbSurname";
            this.tbSurname.Size = new System.Drawing.Size(255, 20);
            this.tbSurname.TabIndex = 6;
            // 
            // lblMiddle
            // 
            this.lblMiddle.AutoSize = true;
            this.lblMiddle.Location = new System.Drawing.Point(12, 88);
            this.lblMiddle.Name = "lblMiddle";
            this.lblMiddle.Size = new System.Drawing.Size(57, 13);
            this.lblMiddle.TabIndex = 19;
            this.lblMiddle.Text = "Отчество:";
            // 
            // tbMiddle
            // 
            this.tbMiddle.Location = new System.Drawing.Point(77, 85);
            this.tbMiddle.MaxLength = 30;
            this.tbMiddle.Name = "tbMiddle";
            this.tbMiddle.Size = new System.Drawing.Size(255, 20);
            this.tbMiddle.TabIndex = 8;
            // 
            // gbAuthor
            // 
            this.gbAuthor.Controls.Add(this.tbSurname);
            this.gbAuthor.Controls.Add(this.lblName);
            this.gbAuthor.Controls.Add(this.lblMiddle);
            this.gbAuthor.Controls.Add(this.tbName);
            this.gbAuthor.Controls.Add(this.lblSurname);
            this.gbAuthor.Controls.Add(this.tbMiddle);
            this.gbAuthor.Location = new System.Drawing.Point(44, 292);
            this.gbAuthor.Name = "gbAuthor";
            this.gbAuthor.Size = new System.Drawing.Size(350, 125);
            this.gbAuthor.TabIndex = 6;
            this.gbAuthor.TabStop = false;
            this.gbAuthor.Text = "Автор";
            // 
            // cbPublisher
            // 
            this.cbPublisher.FormattingEnabled = true;
            this.cbPublisher.Location = new System.Drawing.Point(139, 89);
            this.cbPublisher.Name = "cbPublisher";
            this.cbPublisher.Size = new System.Drawing.Size(255, 21);
            this.cbPublisher.TabIndex = 1;
            // 
            // cbCity
            // 
            this.cbCity.FormattingEnabled = true;
            this.cbCity.Location = new System.Drawing.Point(138, 130);
            this.cbCity.Name = "cbCity";
            this.cbCity.Size = new System.Drawing.Size(256, 21);
            this.cbCity.TabIndex = 2;
            // 
            // FormAddBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cornsilk;
            this.ClientSize = new System.Drawing.Size(435, 467);
            this.Controls.Add(this.cbCity);
            this.Controls.Add(this.cbPublisher);
            this.Controls.Add(this.gbAuthor);
            this.Controls.Add(this.cbAuthor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.lblCountPages);
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.lblPublisher);
            this.Controls.Add(this.lblNameBook);
            this.Controls.Add(this.nudPrice);
            this.Controls.Add(this.nudCount);
            this.Controls.Add(this.nudYear);
            this.Controls.Add(this.tbNameBook);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormAddBook";
            this.ShowInTaskbar = false;
            this.Text = "FormAddField";
            this.Load += new System.EventHandler(this.FormAddBook_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrice)).EndInit();
            this.gbAuthor.ResumeLayout(false);
            this.gbAuthor.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbNameBook;
        private System.Windows.Forms.NumericUpDown nudYear;
        private System.Windows.Forms.NumericUpDown nudCount;
        private System.Windows.Forms.NumericUpDown nudPrice;
        private System.Windows.Forms.Label lblNameBook;
        private System.Windows.Forms.Label lblPublisher;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.Label lblCountPages;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbAuthor;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.TextBox tbSurname;
        private System.Windows.Forms.Label lblMiddle;
        private System.Windows.Forms.TextBox tbMiddle;
        private System.Windows.Forms.GroupBox gbAuthor;
        private System.Windows.Forms.ComboBox cbPublisher;
        private System.Windows.Forms.ComboBox cbCity;
    }
}
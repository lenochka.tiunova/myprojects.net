﻿
namespace LibraryApp
{
    partial class FormAddField
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNameBooks = new System.Windows.Forms.Label();
            this.lblField = new System.Windows.Forms.Label();
            this.cbField = new System.Windows.Forms.ComboBox();
            this.cbNameBooks = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNameBooks
            // 
            this.lblNameBooks.AutoSize = true;
            this.lblNameBooks.Location = new System.Drawing.Point(30, 30);
            this.lblNameBooks.Name = "lblNameBooks";
            this.lblNameBooks.Size = new System.Drawing.Size(89, 26);
            this.lblNameBooks.TabIndex = 7;
            this.lblNameBooks.Text = "Название книги\r\n(код книги):";
            // 
            // lblField
            // 
            this.lblField.AutoSize = true;
            this.lblField.Location = new System.Drawing.Point(30, 88);
            this.lblField.Name = "lblField";
            this.lblField.Size = new System.Drawing.Size(92, 13);
            this.lblField.TabIndex = 8;
            this.lblField.Text = "Область знаний:";
            // 
            // cbField
            // 
            this.cbField.FormattingEnabled = true;
            this.cbField.Location = new System.Drawing.Point(144, 85);
            this.cbField.Name = "cbField";
            this.cbField.Size = new System.Drawing.Size(293, 21);
            this.cbField.TabIndex = 9;
            // 
            // cbNameBooks
            // 
            this.cbNameBooks.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNameBooks.FormattingEnabled = true;
            this.cbNameBooks.Location = new System.Drawing.Point(144, 35);
            this.cbNameBooks.Name = "cbNameBooks";
            this.cbNameBooks.Size = new System.Drawing.Size(293, 21);
            this.cbNameBooks.TabIndex = 10;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(186, 134);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FormAddField
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cornsilk;
            this.ClientSize = new System.Drawing.Size(456, 182);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cbNameBooks);
            this.Controls.Add(this.cbField);
            this.Controls.Add(this.lblField);
            this.Controls.Add(this.lblNameBooks);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "FormAddField";
            this.Text = "FormAddField";
            this.Load += new System.EventHandler(this.FormAddField_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNameBooks;
        private System.Windows.Forms.Label lblField;
        private System.Windows.Forms.ComboBox cbField;
        private System.Windows.Forms.ComboBox cbNameBooks;
        private System.Windows.Forms.Button btnSave;
    }
}
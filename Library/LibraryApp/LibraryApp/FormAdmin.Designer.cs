﻿
namespace LibraryApp
{
    partial class FormAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuAdmin = new System.Windows.Forms.MenuStrip();
            this.miFile = new System.Windows.Forms.ToolStripMenuItem();
            this.miBook = new System.Windows.Forms.ToolStripMenuItem();
            this.miDeptor = new System.Windows.Forms.ToolStripMenuItem();
            this.miAct = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvAdmin = new System.Windows.Forms.DataGridView();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.lblSearch = new System.Windows.Forms.Label();
            this.cbFilter = new System.Windows.Forms.ComboBox();
            this.pnlBooks = new System.Windows.Forms.Panel();
            this.tbDeptorBooks = new System.Windows.Forms.TextBox();
            this.pnlDeptor = new System.Windows.Forms.Panel();
            this.menuAdmin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdmin)).BeginInit();
            this.pnlBooks.SuspendLayout();
            this.pnlDeptor.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuAdmin
            // 
            this.menuAdmin.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.menuAdmin.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFile,
            this.miExit});
            this.menuAdmin.Location = new System.Drawing.Point(0, 0);
            this.menuAdmin.Name = "menuAdmin";
            this.menuAdmin.Size = new System.Drawing.Size(882, 24);
            this.menuAdmin.TabIndex = 0;
            this.menuAdmin.Text = "menuStrip1";
            // 
            // miFile
            // 
            this.miFile.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.miFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miBook,
            this.miDeptor,
            this.miAct});
            this.miFile.Name = "miFile";
            this.miFile.Size = new System.Drawing.Size(48, 20);
            this.miFile.Text = "Файл";
            // 
            // miBook
            // 
            this.miBook.Name = "miBook";
            this.miBook.Size = new System.Drawing.Size(180, 22);
            this.miBook.Text = "Книги";
            this.miBook.Click += new System.EventHandler(this.miBook_Click);
            // 
            // miDeptor
            // 
            this.miDeptor.Name = "miDeptor";
            this.miDeptor.Size = new System.Drawing.Size(180, 22);
            this.miDeptor.Text = "Должники";
            this.miDeptor.Click += new System.EventHandler(this.miDeptor_Click);
            // 
            // miAct
            // 
            this.miAct.Name = "miAct";
            this.miAct.Size = new System.Drawing.Size(180, 22);
            this.miAct.Text = "Акт списания";
            this.miAct.Click += new System.EventHandler(this.miAct_Click);
            // 
            // miExit
            // 
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(120, 20);
            this.miExit.Text = "Выйти из аккаунта";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // dgvAdmin
            // 
            this.dgvAdmin.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAdmin.BackgroundColor = System.Drawing.Color.Cornsilk;
            this.dgvAdmin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAdmin.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAdmin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAdmin.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAdmin.Location = new System.Drawing.Point(228, 24);
            this.dgvAdmin.Name = "dgvAdmin";
            this.dgvAdmin.Size = new System.Drawing.Size(654, 466);
            this.dgvAdmin.TabIndex = 1;
            this.dgvAdmin.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvAdmin_CellFormatting);
            this.dgvAdmin.CurrentCellChanged += new System.EventHandler(this.dgvAdmin_CurrentCellChanged);
            // 
            // tbSearch
            // 
            this.tbSearch.Location = new System.Drawing.Point(6, 39);
            this.tbSearch.MaxLength = 30;
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(200, 20);
            this.tbSearch.TabIndex = 2;
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Location = new System.Drawing.Point(3, 11);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(135, 13);
            this.lblSearch.TabIndex = 3;
            this.lblSearch.Text = "Введите название книги:";
            // 
            // cbFilter
            // 
            this.cbFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFilter.FormattingEnabled = true;
            this.cbFilter.Items.AddRange(new object[] {
            "Показать все",
            "Показать непопулярные",
            "Показать популярные"});
            this.cbFilter.Location = new System.Drawing.Point(6, 94);
            this.cbFilter.Name = "cbFilter";
            this.cbFilter.Size = new System.Drawing.Size(200, 21);
            this.cbFilter.TabIndex = 4;
            this.cbFilter.SelectedIndexChanged += new System.EventHandler(this.cbFilter_SelectedIndexChanged);
            // 
            // pnlBooks
            // 
            this.pnlBooks.Controls.Add(this.lblSearch);
            this.pnlBooks.Controls.Add(this.cbFilter);
            this.pnlBooks.Controls.Add(this.tbSearch);
            this.pnlBooks.Location = new System.Drawing.Point(3, 30);
            this.pnlBooks.Name = "pnlBooks";
            this.pnlBooks.Size = new System.Drawing.Size(222, 125);
            this.pnlBooks.TabIndex = 5;
            // 
            // tbDeptorBooks
            // 
            this.tbDeptorBooks.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tbDeptorBooks.Location = new System.Drawing.Point(3, 3);
            this.tbDeptorBooks.Multiline = true;
            this.tbDeptorBooks.Name = "tbDeptorBooks";
            this.tbDeptorBooks.ReadOnly = true;
            this.tbDeptorBooks.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbDeptorBooks.Size = new System.Drawing.Size(216, 125);
            this.tbDeptorBooks.TabIndex = 6;
            // 
            // pnlDeptor
            // 
            this.pnlDeptor.Controls.Add(this.tbDeptorBooks);
            this.pnlDeptor.Location = new System.Drawing.Point(3, 30);
            this.pnlDeptor.Name = "pnlDeptor";
            this.pnlDeptor.Size = new System.Drawing.Size(222, 138);
            this.pnlDeptor.TabIndex = 7;
            // 
            // FormAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cornsilk;
            this.ClientSize = new System.Drawing.Size(882, 493);
            this.Controls.Add(this.pnlDeptor);
            this.Controls.Add(this.pnlBooks);
            this.Controls.Add(this.dgvAdmin);
            this.Controls.Add(this.menuAdmin);
            this.MainMenuStrip = this.menuAdmin;
            this.MinimumSize = new System.Drawing.Size(898, 532);
            this.Name = "FormAdmin";
            this.Text = "Библиотека";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormAdmin_FormClosed);
            this.Load += new System.EventHandler(this.FormAdmin_Load);
            this.menuAdmin.ResumeLayout(false);
            this.menuAdmin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdmin)).EndInit();
            this.pnlBooks.ResumeLayout(false);
            this.pnlBooks.PerformLayout();
            this.pnlDeptor.ResumeLayout(false);
            this.pnlDeptor.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuAdmin;
        private System.Windows.Forms.DataGridView dgvAdmin;
        private System.Windows.Forms.ToolStripMenuItem miFile;
        private System.Windows.Forms.ToolStripMenuItem miBook;
        private System.Windows.Forms.ToolStripMenuItem miDeptor;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.ComboBox cbFilter;
        private System.Windows.Forms.Panel pnlBooks;
        private System.Windows.Forms.TextBox tbDeptorBooks;
        private System.Windows.Forms.Panel pnlDeptor;
        private System.Windows.Forms.ToolStripMenuItem miAct;
    }
}
﻿
namespace LibraryApp
{
    partial class FormCopyLocation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMove = new System.Windows.Forms.Button();
            this.cbNameBooks = new System.Windows.Forms.ComboBox();
            this.lblLocation = new System.Windows.Forms.Label();
            this.lblNameBooks = new System.Windows.Forms.Label();
            this.tbLocation = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnMove
            // 
            this.btnMove.Location = new System.Drawing.Point(168, 133);
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(83, 23);
            this.btnMove.TabIndex = 16;
            this.btnMove.Text = "Разместить";
            this.btnMove.UseVisualStyleBackColor = true;
            this.btnMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // cbNameBooks
            // 
            this.cbNameBooks.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNameBooks.FormattingEnabled = true;
            this.cbNameBooks.Location = new System.Drawing.Point(135, 34);
            this.cbNameBooks.Name = "cbNameBooks";
            this.cbNameBooks.Size = new System.Drawing.Size(293, 21);
            this.cbNameBooks.TabIndex = 15;
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Location = new System.Drawing.Point(10, 92);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(110, 13);
            this.lblLocation.TabIndex = 13;
            this.lblLocation.Text = "Место размещения:";
            // 
            // lblNameBooks
            // 
            this.lblNameBooks.AutoSize = true;
            this.lblNameBooks.Location = new System.Drawing.Point(31, 29);
            this.lblNameBooks.Name = "lblNameBooks";
            this.lblNameBooks.Size = new System.Drawing.Size(89, 26);
            this.lblNameBooks.TabIndex = 12;
            this.lblNameBooks.Text = "Название книги\r\n(код книги):";
            // 
            // tbLocation
            // 
            this.tbLocation.Location = new System.Drawing.Point(135, 89);
            this.tbLocation.MaxLength = 5;
            this.tbLocation.Name = "tbLocation";
            this.tbLocation.Size = new System.Drawing.Size(149, 20);
            this.tbLocation.TabIndex = 17;
            this.tbLocation.Text = " , , ";
            this.tbLocation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbLocation_KeyPress);
            // 
            // FormCopyLocation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cornsilk;
            this.ClientSize = new System.Drawing.Size(452, 180);
            this.Controls.Add(this.tbLocation);
            this.Controls.Add(this.btnMove);
            this.Controls.Add(this.cbNameBooks);
            this.Controls.Add(this.lblLocation);
            this.Controls.Add(this.lblNameBooks);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "FormCopyLocation";
            this.Text = "Размещение экземпляра";
            this.Load += new System.EventHandler(this.FormCopyLocation_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMove;
        private System.Windows.Forms.ComboBox cbNameBooks;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.Label lblNameBooks;
        private System.Windows.Forms.TextBox tbLocation;
    }
}
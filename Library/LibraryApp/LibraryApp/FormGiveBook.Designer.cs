﻿
namespace LibraryApp
{
    partial class FormGiveBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbBook = new System.Windows.Forms.ComboBox();
            this.cbReader = new System.Windows.Forms.ComboBox();
            this.lblBook = new System.Windows.Forms.Label();
            this.lblReader = new System.Windows.Forms.Label();
            this.btnGive = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbBook
            // 
            this.cbBook.DropDownWidth = 300;
            this.cbBook.FormattingEnabled = true;
            this.cbBook.Location = new System.Drawing.Point(135, 37);
            this.cbBook.Name = "cbBook";
            this.cbBook.Size = new System.Drawing.Size(225, 21);
            this.cbBook.TabIndex = 0;
            this.cbBook.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbBook_KeyPress);
            // 
            // cbReader
            // 
            this.cbReader.DropDownWidth = 300;
            this.cbReader.FormattingEnabled = true;
            this.cbReader.Location = new System.Drawing.Point(135, 87);
            this.cbReader.Name = "cbReader";
            this.cbReader.Size = new System.Drawing.Size(225, 21);
            this.cbReader.TabIndex = 1;
            this.cbReader.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbReader_KeyPress);
            // 
            // lblBook
            // 
            this.lblBook.AutoSize = true;
            this.lblBook.Location = new System.Drawing.Point(63, 32);
            this.lblBook.Name = "lblBook";
            this.lblBook.Size = new System.Drawing.Size(66, 26);
            this.lblBook.TabIndex = 2;
            this.lblBook.Text = "Книга\r\n(код книги):";
            // 
            // lblReader
            // 
            this.lblReader.AutoSize = true;
            this.lblReader.Location = new System.Drawing.Point(12, 82);
            this.lblReader.Name = "lblReader";
            this.lblReader.Size = new System.Drawing.Size(117, 26);
            this.lblReader.TabIndex = 3;
            this.lblReader.Text = "Читатель \r\n(читательский билет):";
            // 
            // btnGive
            // 
            this.btnGive.Location = new System.Drawing.Point(145, 123);
            this.btnGive.Name = "btnGive";
            this.btnGive.Size = new System.Drawing.Size(75, 23);
            this.btnGive.TabIndex = 4;
            this.btnGive.Text = "Выдать";
            this.btnGive.UseVisualStyleBackColor = true;
            this.btnGive.Click += new System.EventHandler(this.btnGive_Click);
            // 
            // FormGiveBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cornsilk;
            this.ClientSize = new System.Drawing.Size(381, 158);
            this.Controls.Add(this.btnGive);
            this.Controls.Add(this.lblReader);
            this.Controls.Add(this.lblBook);
            this.Controls.Add(this.cbReader);
            this.Controls.Add(this.cbBook);
            this.Name = "FormGiveBook";
            this.Text = "Выдача книги";
            this.Load += new System.EventHandler(this.FormGiveBook_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbBook;
        private System.Windows.Forms.ComboBox cbReader;
        private System.Windows.Forms.Label lblBook;
        private System.Windows.Forms.Label lblReader;
        private System.Windows.Forms.Button btnGive;
    }
}
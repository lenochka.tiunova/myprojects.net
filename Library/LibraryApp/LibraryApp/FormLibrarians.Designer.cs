﻿
namespace LibraryApp
{
    partial class FormLibrarians
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuLibrarians = new System.Windows.Forms.MenuStrip();
            this.miFile = new System.Windows.Forms.ToolStripMenuItem();
            this.miBook = new System.Windows.Forms.ToolStripMenuItem();
            this.miBookFieldOfKnowledge = new System.Windows.Forms.ToolStripMenuItem();
            this.miCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.miReaders = new System.Windows.Forms.ToolStripMenuItem();
            this.miHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.miWork = new System.Windows.Forms.ToolStripMenuItem();
            this.miAddBook = new System.Windows.Forms.ToolStripMenuItem();
            this.miDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.miClose = new System.Windows.Forms.ToolStripMenuItem();
            this.miBookField = new System.Windows.Forms.ToolStripMenuItem();
            this.miCopyLocation = new System.Windows.Forms.ToolStripMenuItem();
            this.miEditField = new System.Windows.Forms.ToolStripMenuItem();
            this.miGive = new System.Windows.Forms.ToolStripMenuItem();
            this.miAccept = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvLibrarians = new System.Windows.Forms.DataGridView();
            this.cbLiner = new System.Windows.Forms.ComboBox();
            this.gbLiner = new System.Windows.Forms.GroupBox();
            this.просмотретьАктСписанияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miAct = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLibrarians.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLibrarians)).BeginInit();
            this.gbLiner.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuLibrarians
            // 
            this.menuLibrarians.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.menuLibrarians.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFile,
            this.miWork,
            this.miExit});
            this.menuLibrarians.Location = new System.Drawing.Point(0, 0);
            this.menuLibrarians.Name = "menuLibrarians";
            this.menuLibrarians.ShowItemToolTips = true;
            this.menuLibrarians.Size = new System.Drawing.Size(882, 24);
            this.menuLibrarians.TabIndex = 0;
            this.menuLibrarians.Text = "menuStrip1";
            // 
            // miFile
            // 
            this.miFile.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.miFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miBook,
            this.miBookFieldOfKnowledge,
            this.miCopy,
            this.miReaders,
            this.miHistory,
            this.miAct});
            this.miFile.Name = "miFile";
            this.miFile.Size = new System.Drawing.Size(48, 20);
            this.miFile.Text = "Файл";
            // 
            // miBook
            // 
            this.miBook.Name = "miBook";
            this.miBook.Size = new System.Drawing.Size(216, 22);
            this.miBook.Text = "Книги";
            this.miBook.Click += new System.EventHandler(this.miBook_Click);
            // 
            // miBookFieldOfKnowledge
            // 
            this.miBookFieldOfKnowledge.Name = "miBookFieldOfKnowledge";
            this.miBookFieldOfKnowledge.Size = new System.Drawing.Size(216, 22);
            this.miBookFieldOfKnowledge.Text = "Книги по области знаний";
            this.miBookFieldOfKnowledge.Click += new System.EventHandler(this.miBookFieldOfKnowledge_Click);
            // 
            // miCopy
            // 
            this.miCopy.Name = "miCopy";
            this.miCopy.Size = new System.Drawing.Size(216, 22);
            this.miCopy.Text = "Экземпляры";
            this.miCopy.Click += new System.EventHandler(this.miCopy_Click);
            // 
            // miReaders
            // 
            this.miReaders.Name = "miReaders";
            this.miReaders.Size = new System.Drawing.Size(216, 22);
            this.miReaders.Text = "Читатели";
            this.miReaders.Click += new System.EventHandler(this.miReaders_Click);
            // 
            // miHistory
            // 
            this.miHistory.Name = "miHistory";
            this.miHistory.Size = new System.Drawing.Size(216, 22);
            this.miHistory.Text = "История выдачи книг";
            this.miHistory.Click += new System.EventHandler(this.miHistory_Click);
            // 
            // miWork
            // 
            this.miWork.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.miWork.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAddBook,
            this.miDelete,
            this.miClose,
            this.miBookField,
            this.miCopyLocation,
            this.miEditField,
            this.miGive,
            this.miAccept});
            this.miWork.Name = "miWork";
            this.miWork.Size = new System.Drawing.Size(70, 20);
            this.miWork.Text = "Действие";
            // 
            // miAddBook
            // 
            this.miAddBook.Name = "miAddBook";
            this.miAddBook.Size = new System.Drawing.Size(244, 22);
            this.miAddBook.Text = "Зарегистрировать книгу";
            this.miAddBook.Click += new System.EventHandler(this.miAddBook_Click);
            // 
            // miDelete
            // 
            this.miDelete.Name = "miDelete";
            this.miDelete.Size = new System.Drawing.Size(244, 22);
            this.miDelete.Text = "Добавить книгу в акт списания";
            this.miDelete.Click += new System.EventHandler(this.miDelete_Click);
            // 
            // miClose
            // 
            this.miClose.Name = "miClose";
            this.miClose.Size = new System.Drawing.Size(244, 22);
            this.miClose.Text = "Закрыть абонемент читателя";
            this.miClose.Click += new System.EventHandler(this.miClose_Click);
            // 
            // miBookField
            // 
            this.miBookField.Name = "miBookField";
            this.miBookField.Size = new System.Drawing.Size(244, 22);
            this.miBookField.Text = "Задать книге область знаний";
            this.miBookField.Click += new System.EventHandler(this.miBookField_Click);
            // 
            // miCopyLocation
            // 
            this.miCopyLocation.Name = "miCopyLocation";
            this.miCopyLocation.Size = new System.Drawing.Size(244, 22);
            this.miCopyLocation.Text = "Разместить экземпляр";
            this.miCopyLocation.Click += new System.EventHandler(this.miCopyLocation_Click);
            // 
            // miEditField
            // 
            this.miEditField.Name = "miEditField";
            this.miEditField.Size = new System.Drawing.Size(244, 22);
            this.miEditField.Text = "Изменить";
            this.miEditField.Click += new System.EventHandler(this.miEditField_Click);
            // 
            // miGive
            // 
            this.miGive.Name = "miGive";
            this.miGive.Size = new System.Drawing.Size(244, 22);
            this.miGive.Text = "Выдать книгу";
            this.miGive.Click += new System.EventHandler(this.miGive_Click);
            // 
            // miAccept
            // 
            this.miAccept.Name = "miAccept";
            this.miAccept.Size = new System.Drawing.Size(244, 22);
            this.miAccept.Text = "Принять книгу";
            this.miAccept.Click += new System.EventHandler(this.miAccept_Click);
            // 
            // miExit
            // 
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(120, 20);
            this.miExit.Text = "Выйти из аккаунта";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // dgvLibrarians
            // 
            this.dgvLibrarians.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvLibrarians.BackgroundColor = System.Drawing.Color.Cornsilk;
            this.dgvLibrarians.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLibrarians.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLibrarians.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLibrarians.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvLibrarians.Location = new System.Drawing.Point(0, 24);
            this.dgvLibrarians.Name = "dgvLibrarians";
            this.dgvLibrarians.Size = new System.Drawing.Size(882, 414);
            this.dgvLibrarians.TabIndex = 1;
            this.dgvLibrarians.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvLibrarians_CellFormatting);
            // 
            // cbLiner
            // 
            this.cbLiner.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLiner.FormattingEnabled = true;
            this.cbLiner.Location = new System.Drawing.Point(6, 36);
            this.cbLiner.Name = "cbLiner";
            this.cbLiner.Size = new System.Drawing.Size(109, 21);
            this.cbLiner.TabIndex = 2;
            this.cbLiner.SelectedIndexChanged += new System.EventHandler(this.cbLiner_SelectedIndexChanged);
            // 
            // gbLiner
            // 
            this.gbLiner.Controls.Add(this.cbLiner);
            this.gbLiner.Location = new System.Drawing.Point(12, 444);
            this.gbLiner.Name = "gbLiner";
            this.gbLiner.Size = new System.Drawing.Size(134, 80);
            this.gbLiner.TabIndex = 3;
            this.gbLiner.TabStop = false;
            this.gbLiner.Text = "Читательский билет";
            // 
            // просмотретьАктСписанияToolStripMenuItem
            // 
            this.просмотретьАктСписанияToolStripMenuItem.Name = "просмотретьАктСписанияToolStripMenuItem";
            this.просмотретьАктСписанияToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // miAct
            // 
            this.miAct.Name = "miAct";
            this.miAct.Size = new System.Drawing.Size(216, 22);
            this.miAct.Text = "Посмотреть акт списания";
            this.miAct.Click += new System.EventHandler(this.miAct_Click);
            // 
            // FormLibrarians
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cornsilk;
            this.ClientSize = new System.Drawing.Size(882, 538);
            this.Controls.Add(this.gbLiner);
            this.Controls.Add(this.dgvLibrarians);
            this.Controls.Add(this.menuLibrarians);
            this.MainMenuStrip = this.menuLibrarians;
            this.MinimumSize = new System.Drawing.Size(898, 532);
            this.Name = "FormLibrarians";
            this.Text = "Библиотека";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormLibrarians_FormClosed);
            this.Load += new System.EventHandler(this.FormLibrarians_Load);
            this.menuLibrarians.ResumeLayout(false);
            this.menuLibrarians.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLibrarians)).EndInit();
            this.gbLiner.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuLibrarians;
        private System.Windows.Forms.DataGridView dgvLibrarians;
        private System.Windows.Forms.ToolStripMenuItem miFile;
        private System.Windows.Forms.ToolStripMenuItem miBook;
        private System.Windows.Forms.ToolStripMenuItem miBookFieldOfKnowledge;
        private System.Windows.Forms.ToolStripMenuItem miCopy;
        private System.Windows.Forms.ToolStripMenuItem miReaders;
        private System.Windows.Forms.ToolStripMenuItem miWork;
        private System.Windows.Forms.ToolStripMenuItem miDelete;
        private System.Windows.Forms.ToolStripMenuItem miClose;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.ToolStripMenuItem miAddBook;
        private System.Windows.Forms.ToolStripMenuItem miBookField;
        private System.Windows.Forms.ToolStripMenuItem miHistory;
        private System.Windows.Forms.ToolStripMenuItem miEditField;
        private System.Windows.Forms.ToolStripMenuItem miCopyLocation;
        private System.Windows.Forms.ToolStripMenuItem miGive;
        private System.Windows.Forms.ToolStripMenuItem miAccept;
        private System.Windows.Forms.ComboBox cbLiner;
        private System.Windows.Forms.GroupBox gbLiner;
        private System.Windows.Forms.ToolStripMenuItem просмотретьАктСписанияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miAct;
    }
}
﻿
namespace LibraryApp
{
    partial class FormReaders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuReaders = new System.Windows.Forms.MenuStrip();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvBooks = new System.Windows.Forms.DataGridView();
            this.cbFieldOfKnowledge = new System.Windows.Forms.ComboBox();
            this.cbAuthor = new System.Windows.Forms.ComboBox();
            this.lblFieldOfKnowledge = new System.Windows.Forms.Label();
            this.lblAuthor = new System.Windows.Forms.Label();
            this.btnGive = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.menuReaders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBooks)).BeginInit();
            this.SuspendLayout();
            // 
            // menuReaders
            // 
            this.menuReaders.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.menuReaders.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miExit});
            this.menuReaders.Location = new System.Drawing.Point(0, 0);
            this.menuReaders.Name = "menuReaders";
            this.menuReaders.Size = new System.Drawing.Size(882, 24);
            this.menuReaders.TabIndex = 0;
            this.menuReaders.Text = "menuStrip1";
            // 
            // miExit
            // 
            this.miExit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(120, 20);
            this.miExit.Text = "Выйти из аккаунта";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // dgvBooks
            // 
            this.dgvBooks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvBooks.BackgroundColor = System.Drawing.Color.Cornsilk;
            this.dgvBooks.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBooks.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBooks.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvBooks.Location = new System.Drawing.Point(268, 26);
            this.dgvBooks.Name = "dgvBooks";
            this.dgvBooks.Size = new System.Drawing.Size(613, 465);
            this.dgvBooks.TabIndex = 1;
            this.dgvBooks.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvBooks_CellFormatting);
            // 
            // cbFieldOfKnowledge
            // 
            this.cbFieldOfKnowledge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFieldOfKnowledge.DropDownWidth = 300;
            this.cbFieldOfKnowledge.FormattingEnabled = true;
            this.cbFieldOfKnowledge.Location = new System.Drawing.Point(9, 77);
            this.cbFieldOfKnowledge.Name = "cbFieldOfKnowledge";
            this.cbFieldOfKnowledge.Size = new System.Drawing.Size(250, 21);
            this.cbFieldOfKnowledge.Sorted = true;
            this.cbFieldOfKnowledge.TabIndex = 2;
            this.cbFieldOfKnowledge.SelectedIndexChanged += new System.EventHandler(this.cbFieldOfKnowledge_SelectedIndexChanged);
            // 
            // cbAuthor
            // 
            this.cbAuthor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAuthor.FormattingEnabled = true;
            this.cbAuthor.Location = new System.Drawing.Point(9, 168);
            this.cbAuthor.Name = "cbAuthor";
            this.cbAuthor.Size = new System.Drawing.Size(250, 21);
            this.cbAuthor.Sorted = true;
            this.cbAuthor.TabIndex = 3;
            this.cbAuthor.SelectedIndexChanged += new System.EventHandler(this.cbAuthor_SelectedIndexChanged);
            // 
            // lblFieldOfKnowledge
            // 
            this.lblFieldOfKnowledge.AutoSize = true;
            this.lblFieldOfKnowledge.Location = new System.Drawing.Point(6, 51);
            this.lblFieldOfKnowledge.Name = "lblFieldOfKnowledge";
            this.lblFieldOfKnowledge.Size = new System.Drawing.Size(143, 13);
            this.lblFieldOfKnowledge.TabIndex = 4;
            this.lblFieldOfKnowledge.Text = "Выберите область знаний:";
            // 
            // lblAuthor
            // 
            this.lblAuthor.AutoSize = true;
            this.lblAuthor.Location = new System.Drawing.Point(6, 142);
            this.lblAuthor.Name = "lblAuthor";
            this.lblAuthor.Size = new System.Drawing.Size(98, 13);
            this.lblAuthor.TabIndex = 5;
            this.lblAuthor.Text = "Выберите автора:";
            // 
            // btnGive
            // 
            this.btnGive.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.btnGive.Location = new System.Drawing.Point(56, 302);
            this.btnGive.Name = "btnGive";
            this.btnGive.Size = new System.Drawing.Size(130, 55);
            this.btnGive.TabIndex = 6;
            this.btnGive.Text = "Получить инвентарный номер \r\nэкземпляра";
            this.btnGive.UseVisualStyleBackColor = false;
            this.btnGive.Click += new System.EventHandler(this.btnGive_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.btnClear.Location = new System.Drawing.Point(79, 229);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(87, 23);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Очистить все";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // FormReaders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cornsilk;
            this.ClientSize = new System.Drawing.Size(882, 493);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnGive);
            this.Controls.Add(this.lblAuthor);
            this.Controls.Add(this.lblFieldOfKnowledge);
            this.Controls.Add(this.cbAuthor);
            this.Controls.Add(this.cbFieldOfKnowledge);
            this.Controls.Add(this.dgvBooks);
            this.Controls.Add(this.menuReaders);
            this.MainMenuStrip = this.menuReaders;
            this.MinimumSize = new System.Drawing.Size(898, 532);
            this.Name = "FormReaders";
            this.Text = "Библиотека";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormReaders_FormClosed);
            this.Load += new System.EventHandler(this.FormReaders_Load);
            this.menuReaders.ResumeLayout(false);
            this.menuReaders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBooks)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuReaders;
        private System.Windows.Forms.DataGridView dgvBooks;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.ComboBox cbFieldOfKnowledge;
        private System.Windows.Forms.ComboBox cbAuthor;
        private System.Windows.Forms.Label lblFieldOfKnowledge;
        private System.Windows.Forms.Label lblAuthor;
        private System.Windows.Forms.Button btnGive;
        private System.Windows.Forms.Button btnClear;
    }
}
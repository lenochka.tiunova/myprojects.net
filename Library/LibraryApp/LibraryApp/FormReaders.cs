﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryApp
{
    public partial class FormReaders : Form
    {
        private readonly string connectionString = @"Data Source= DESKTOP-PPCBH42\SQLEXPREESS;
        Initial Catalog = dbLibrary;Integrated Security=True";
        public FormReaders()
        {
            InitializeComponent();
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Restart();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            cbAuthor.SelectedIndex = -1;
            cbFieldOfKnowledge.SelectedIndex = -1;
        }

        private void btnGive_Click(object sender, EventArgs e)
        {
            if (dgvBooks.CurrentRow != null)
            {
                var bookFree = "SELECT * FROM sCopyFree WHERE BookId  = " + dgvBooks.Rows[dgvBooks.CurrentRow.Index].Cells[0].Value;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter(bookFree, connection);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        MessageBox.Show("Инвентарный номер свободного экземпляра: " + ds.Tables[0].Rows[0][0]
                            + "\r\nВсего свободных экземпляров: " + ds.Tables[0].Rows.Count, "Внимание",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Нет свободных экземпляров", "Внимание",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            else
            {
                MessageBox.Show("Сначала выберите книгу", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void FormReaders_Load(object sender, EventArgs e)
        {
            string sql = "SELECT * FROM sBooksForReaders";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                dgvBooks.DataSource = ds.Tables[0];
            }
            dgvEdit();  
            string author = "SELECT * FROM AuthorTable";
            string nameField = "SELECT * FROM FieldOfKnowledgeTable";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(author, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                DataTable fieldTable = ds.Tables.Add("Field");
                adapter = new SqlDataAdapter(nameField, connection);
                adapter.Fill(ds, "Field");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    cbAuthor.Items.Add(ds.Tables[0].Rows[i][1].ToString() + " " +
                        ds.Tables[0].Rows[i][2].ToString() + " " + ds.Tables[0].Rows[i][3].ToString());
                }
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    cbFieldOfKnowledge.Items.Add(ds.Tables[1].Rows[i][1].ToString());
                }
            }
        }

        private void FormReaders_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        public void Filter()
        {
            bool check = false;
            string sql = "SELECT * FROM sBooksForReaders ";
            if (cbAuthor.SelectedIndex != -1)
            {
                check = true;
                string surname = cbAuthor.SelectedItem.ToString().Split(' ')[0];
                string name = cbAuthor.SelectedItem.ToString().Split(' ')[1];
                string middle = cbAuthor.SelectedItem.ToString().Split(' ')[2];

                sql += "WHERE SurnameAuthor = '" + surname + "' AND NameAuthor = '" + name + "' AND MiddleAuthor = '" + middle + "' ";
            }
            if (cbFieldOfKnowledge.SelectedIndex != -1)
            {
                if (check)
                {
                    sql += "AND";
                }
                else
                {
                    sql += "WHERE";
                }
                sql += " NameFieldOfKnowledge = '" + cbFieldOfKnowledge.SelectedItem.ToString() + "' ";
            }

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                dgvBooks.DataSource = ds.Tables[0];
            }
            dgvEdit();
        }

        public void dgvEdit()
        {
            dgvBooks.Columns[0].HeaderText = "Номер книги";
            dgvBooks.Columns[5].HeaderText = "Область знаний";
            dgvBooks.Columns[6].HeaderText = "Автор";
            dgvBooks.Columns[7].HeaderText = " ";
            dgvBooks.Columns[8].HeaderText = "  ";
            dgvBooks.AutoResizeColumns();
            dgvBooks.AutoResizeRows();
        }

        private void cbFieldOfKnowledge_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filter();
        }

        private void cbAuthor_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filter();
        }

        private void dgvBooks_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (DBNull.Value == e.Value)
            {
                e.Value = "-";
            }
        }
    }
}
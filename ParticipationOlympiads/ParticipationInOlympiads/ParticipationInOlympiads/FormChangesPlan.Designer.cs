﻿
namespace ParticipationInOlympiads
{
    partial class FormChangesPlan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormChangesPlan));
            this.cbSchoolStage = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblSchoolStage = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.cbLevel = new System.Windows.Forms.ComboBox();
            this.dtpPlan = new System.Windows.Forms.DateTimePicker();
            this.lblLevel = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.cbNameOlympiad = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.pnlNewOlympiad = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.pnlNewOlympiad.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbSchoolStage
            // 
            this.cbSchoolStage.BackColor = System.Drawing.Color.White;
            this.cbSchoolStage.FormattingEnabled = true;
            this.cbSchoolStage.Location = new System.Drawing.Point(124, 60);
            this.cbSchoolStage.Name = "cbSchoolStage";
            this.cbSchoolStage.Size = new System.Drawing.Size(271, 21);
            this.cbSchoolStage.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(158, 171);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblSchoolStage
            // 
            this.lblSchoolStage.AutoSize = true;
            this.lblSchoolStage.Location = new System.Drawing.Point(12, 63);
            this.lblSchoolStage.Name = "lblSchoolStage";
            this.lblSchoolStage.Size = new System.Drawing.Size(98, 13);
            this.lblSchoolStage.TabIndex = 7;
            this.lblSchoolStage.Text = "Область классов:";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(50, 30);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(60, 13);
            this.lblName.TabIndex = 6;
            this.lblName.Text = "Название:";
            // 
            // cbLevel
            // 
            this.cbLevel.BackColor = System.Drawing.Color.White;
            this.cbLevel.FormattingEnabled = true;
            this.cbLevel.Location = new System.Drawing.Point(124, 95);
            this.cbLevel.Name = "cbLevel";
            this.cbLevel.Size = new System.Drawing.Size(271, 21);
            this.cbLevel.TabIndex = 2;
            // 
            // dtpPlan
            // 
            this.dtpPlan.CustomFormat = "\"yyyy-dd-MM\"";
            this.dtpPlan.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPlan.Location = new System.Drawing.Point(124, 130);
            this.dtpPlan.MinDate = new System.DateTime(2021, 3, 28, 0, 0, 0, 0);
            this.dtpPlan.Name = "dtpPlan";
            this.dtpPlan.Size = new System.Drawing.Size(271, 20);
            this.dtpPlan.TabIndex = 3;
            this.dtpPlan.Value = new System.DateTime(2021, 3, 28, 0, 0, 0, 0);
            // 
            // lblLevel
            // 
            this.lblLevel.AutoSize = true;
            this.lblLevel.Location = new System.Drawing.Point(56, 98);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(54, 13);
            this.lblLevel.TabIndex = 14;
            this.lblLevel.Text = "Уровень:";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(74, 133);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(36, 13);
            this.lblDate.TabIndex = 15;
            this.lblDate.Text = "Дата:";
            // 
            // cbNameOlympiad
            // 
            this.cbNameOlympiad.BackColor = System.Drawing.Color.White;
            this.cbNameOlympiad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNameOlympiad.FormattingEnabled = true;
            this.cbNameOlympiad.Location = new System.Drawing.Point(124, 27);
            this.cbNameOlympiad.Name = "cbNameOlympiad";
            this.cbNameOlympiad.Size = new System.Drawing.Size(271, 21);
            this.cbNameOlympiad.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAdd);
            this.groupBox1.Location = new System.Drawing.Point(28, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(352, 52);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Добавьте новую олимпиаду, если вашей нет в списке";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(115, 20);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // pnlNewOlympiad
            // 
            this.pnlNewOlympiad.Controls.Add(this.groupBox1);
            this.pnlNewOlympiad.Location = new System.Drawing.Point(15, 207);
            this.pnlNewOlympiad.Name = "pnlNewOlympiad";
            this.pnlNewOlympiad.Size = new System.Drawing.Size(391, 78);
            this.pnlNewOlympiad.TabIndex = 18;
            // 
            // FormChangesPlan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(425, 287);
            this.Controls.Add(this.pnlNewOlympiad);
            this.Controls.Add(this.cbNameOlympiad);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblLevel);
            this.Controls.Add(this.dtpPlan);
            this.Controls.Add(this.cbLevel);
            this.Controls.Add(this.cbSchoolStage);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblSchoolStage);
            this.Controls.Add(this.lblName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormChangesPlan";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormChangesPlan";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormChangesPlan_FormClosing);
            this.Load += new System.EventHandler(this.FormChangesPlan_Load);
            this.groupBox1.ResumeLayout(false);
            this.pnlNewOlympiad.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cbSchoolStage;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblSchoolStage;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ComboBox cbLevel;
        private System.Windows.Forms.Label lblLevel;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.ComboBox cbNameOlympiad;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Panel pnlNewOlympiad;
        public System.Windows.Forms.DateTimePicker dtpPlan;
    }
}
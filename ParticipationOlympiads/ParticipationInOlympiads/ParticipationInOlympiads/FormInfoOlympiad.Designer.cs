﻿namespace ParticipationInOlympiads
{
    partial class FormInfoOlympiad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormInfoOlympiad));
            this.lblNameOlympiad = new System.Windows.Forms.Label();
            this.lblSubject = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.tbNameOlympiad = new System.Windows.Forms.TextBox();
            this.tbSubject = new System.Windows.Forms.TextBox();
            this.tblDescription = new System.Windows.Forms.TextBox();
            this.lblTeam = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblNameOlympiad
            // 
            this.lblNameOlympiad.AutoSize = true;
            this.lblNameOlympiad.Location = new System.Drawing.Point(27, 25);
            this.lblNameOlympiad.Name = "lblNameOlympiad";
            this.lblNameOlympiad.Size = new System.Drawing.Size(60, 13);
            this.lblNameOlympiad.TabIndex = 0;
            this.lblNameOlympiad.Text = "Название:";
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Location = new System.Drawing.Point(32, 58);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(55, 13);
            this.lblSubject.TabIndex = 1;
            this.lblSubject.Text = "Предмет:";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(27, 90);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 13);
            this.lblDescription.TabIndex = 2;
            this.lblDescription.Text = "Описание:";
            // 
            // tbNameOlympiad
            // 
            this.tbNameOlympiad.Location = new System.Drawing.Point(114, 25);
            this.tbNameOlympiad.MaxLength = 50;
            this.tbNameOlympiad.Name = "tbNameOlympiad";
            this.tbNameOlympiad.ReadOnly = true;
            this.tbNameOlympiad.Size = new System.Drawing.Size(321, 20);
            this.tbNameOlympiad.TabIndex = 3;
            this.tbNameOlympiad.TabStop = false;
            // 
            // tbSubject
            // 
            this.tbSubject.Location = new System.Drawing.Point(114, 58);
            this.tbSubject.MaxLength = 30;
            this.tbSubject.Name = "tbSubject";
            this.tbSubject.ReadOnly = true;
            this.tbSubject.Size = new System.Drawing.Size(321, 20);
            this.tbSubject.TabIndex = 4;
            this.tbSubject.TabStop = false;
            // 
            // tblDescription
            // 
            this.tblDescription.Location = new System.Drawing.Point(114, 90);
            this.tblDescription.Multiline = true;
            this.tblDescription.Name = "tblDescription";
            this.tblDescription.ReadOnly = true;
            this.tblDescription.Size = new System.Drawing.Size(321, 98);
            this.tblDescription.TabIndex = 5;
            this.tblDescription.TabStop = false;
            // 
            // lblTeam
            // 
            this.lblTeam.AutoSize = true;
            this.lblTeam.Location = new System.Drawing.Point(111, 206);
            this.lblTeam.Name = "lblTeam";
            this.lblTeam.Size = new System.Drawing.Size(35, 13);
            this.lblTeam.TabIndex = 6;
            this.lblTeam.Text = "label4";
            // 
            // FormInfoOlympiad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(450, 293);
            this.Controls.Add(this.lblTeam);
            this.Controls.Add(this.tblDescription);
            this.Controls.Add(this.tbSubject);
            this.Controls.Add(this.tbNameOlympiad);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblSubject);
            this.Controls.Add(this.lblNameOlympiad);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormInfoOlympiad";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Просмотр информации об олимпиаде";
            this.Load += new System.EventHandler(this.FormInfoOlympiad_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNameOlympiad;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox tbNameOlympiad;
        private System.Windows.Forms.TextBox tbSubject;
        private System.Windows.Forms.TextBox tblDescription;
        private System.Windows.Forms.Label lblTeam;
    }
}
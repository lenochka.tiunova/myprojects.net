﻿
namespace ParticipationInOlympiads
{
    partial class FormNewOlympiad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNewOlympiad));
            this.lblDescription = new System.Windows.Forms.Label();
            this.cbSubject = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblSubiect = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.cbTeam = new System.Windows.Forms.CheckBox();
            this.tbNameOlympiad = new System.Windows.Forms.TextBox();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(24, 97);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 13);
            this.lblDescription.TabIndex = 25;
            this.lblDescription.Text = "Описание:";
            // 
            // cbSubject
            // 
            this.cbSubject.BackColor = System.Drawing.Color.White;
            this.cbSubject.FormattingEnabled = true;
            this.cbSubject.Location = new System.Drawing.Point(92, 59);
            this.cbSubject.Name = "cbSubject";
            this.cbSubject.Size = new System.Drawing.Size(318, 21);
            this.cbSubject.TabIndex = 1;
            this.cbSubject.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbSubject_KeyPress);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(183, 246);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblSubiect
            // 
            this.lblSubiect.AutoSize = true;
            this.lblSubiect.Location = new System.Drawing.Point(23, 62);
            this.lblSubiect.Name = "lblSubiect";
            this.lblSubiect.Size = new System.Drawing.Size(55, 13);
            this.lblSubiect.TabIndex = 20;
            this.lblSubiect.Text = "Предмет:";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(18, 29);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(60, 13);
            this.lblName.TabIndex = 19;
            this.lblName.Text = "Название:";
            // 
            // cbTeam
            // 
            this.cbTeam.AutoSize = true;
            this.cbTeam.Location = new System.Drawing.Point(92, 214);
            this.cbTeam.Name = "cbTeam";
            this.cbTeam.Size = new System.Drawing.Size(143, 17);
            this.cbTeam.TabIndex = 3;
            this.cbTeam.Text = "Олимпиада командная";
            this.cbTeam.UseVisualStyleBackColor = true;
            // 
            // tbNameOlympiad
            // 
            this.tbNameOlympiad.Location = new System.Drawing.Point(92, 21);
            this.tbNameOlympiad.Name = "tbNameOlympiad";
            this.tbNameOlympiad.Size = new System.Drawing.Size(318, 20);
            this.tbNameOlympiad.TabIndex = 0;
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(92, 97);
            this.tbDescription.Multiline = true;
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbDescription.Size = new System.Drawing.Size(318, 103);
            this.tbDescription.TabIndex = 2;
            // 
            // FormNewOlympiad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(457, 278);
            this.Controls.Add(this.tbDescription);
            this.Controls.Add(this.tbNameOlympiad);
            this.Controls.Add(this.cbTeam);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.cbSubject);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblSubiect);
            this.Controls.Add(this.lblName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormNewOlympiad";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление новой олимпиады";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormNewOlympiad_FormClosing);
            this.Load += new System.EventHandler(this.FormNewOlympiad_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.ComboBox cbSubject;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblSubiect;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.CheckBox cbTeam;
        private System.Windows.Forms.TextBox tbNameOlympiad;
        private System.Windows.Forms.TextBox tbDescription;
    }
}
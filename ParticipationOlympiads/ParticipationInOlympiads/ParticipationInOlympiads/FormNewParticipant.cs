﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ParticipationInOlympiads
{
    public partial class FormNewParticipant : Form
    {
        public FormNewParticipant()
        {
            InitializeComponent();
        }

        dbRegistrationOfParticipationInOlympiadsEntities db = new dbRegistrationOfParticipationInOlympiadsEntities();
        public int index = -1;
        Regex rgx0 = new Regex("[0-9]{2}[,]{1}");
        Regex rgx = new Regex("[,]{1}[0-9]{2}");

        public void Vvod(string stroka, KeyPressEventArgs e, int cursor, TextBox text)
        {
            if (e.KeyChar != Convert.ToChar(8))
            {
                if (!Char.IsDigit(e.KeyChar))
                {
                    if (!(cursor != 0 && cursor < 3 && e.KeyChar == ',' && !stroka.Contains(",")))
                    {
                        e.Handled = true;
                    }
                }

                if (Regex.IsMatch(stroka, rgx0.ToString()) && cursor < 3
                    || Regex.IsMatch(stroka, rgx.ToString()) && cursor > 2
                    || (stroka.Length > 2 && e.KeyChar != ',' && !stroka.Contains(",")))
                {
                    e.Handled = true;
                }
            }
            else if (stroka.Length > 4 && cursor - 1 == stroka.IndexOf(","))
            {
                e.Handled = true;
            }
        }

        private void tbScores_KeyPress(object sender, KeyPressEventArgs e)
        {
            int cursor = tbScores.SelectionStart;
            Vvod(tbScores.Text, e, cursor, tbScores);
        }

        bool save = false;

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Text == "Регистрация участника")
            {
                if (cbStudent.SelectedIndex != -1)
                {
                    string name = cbStudent.Text.Split(' ')[1];
                    string surname = cbStudent.Text.Split(' ')[0];
                    string middle = cbStudent.Text.Split(' ')[2];
                    int classStudent = Convert.ToInt32(tbClass.Text);

                    RegistrationOfParticipantsTable registration = new RegistrationOfParticipantsTable
                    {
                        StudentId = db.StudentTables.FirstOrDefault(n => n.StudentName.Replace("\r\n", "").Trim() == name && n.StudentSurname.Replace("\r\n", "").Trim() == surname
                            && n.StudentMiddle.Replace("\r\n", "").Trim() == middle).StudentId,
                        Class = classStudent,
                        OlympiadPlanId = db.OlympiadPlanTables.FirstOrDefault(n => n.OlimpiadByAgeAndLevelTable.LevelTable.OlympiadLevel.Replace("\r\n", "").Trim() == cbLevel.Text.Replace("\r\n", "").Trim()
                        && n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.SchoolStageTable.SchoolStage.Replace("\r\n", "").Trim() == cbSchoolStage.Text.Replace("\r\n", "").Trim()
                        && n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName.Replace("\r\n", "").Trim() == cbNameOlympiad.Text.Replace("\r\n", "").Trim()).OlympiadPlanId
                    };
                    if (db.RegistrationOfParticipantsTables.FirstOrDefault(n => n.StudentId == registration.StudentId
                    && n.OlympiadPlanId == registration.OlympiadPlanId) == null)
                    {
                        db.RegistrationOfParticipantsTables.Add(registration);
                        db.SaveChanges();
                        MessageBox.Show("Участник добавлен.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        save = true;
                    }
                    else
                    {
                        MessageBox.Show("Участник был добавлен ранее.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Выберите студента из списка.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                var olympiadAccounting = db.RegistrationOfParticipantsTables.FirstOrDefault(n => n.RegistrationOfParticipantsId == index);

                if (cbAbsent.Checked)
                {
                    olympiadAccounting.Absent = true;
                }
                else
                {
                    olympiadAccounting.Absent = false;
                    if (tbScores.Text == "")
                    {
                        olympiadAccounting.Scores = 0;
                    }
                    else
                    {
                        olympiadAccounting.Scores = Convert.ToDecimal(tbScores.Text);
                    }
                }

                if (!String.IsNullOrEmpty(cbResult.Text))
                {
                    if (db.ResultTables.FirstOrDefault(n => n.Result == cbResult.Text) == null)
                    {
                        ResultTable resultTable = new ResultTable
                        {
                            Result = cbResult.Text
                        };
                        db.ResultTables.Add(resultTable);
                        db.SaveChanges();
                    }
                    olympiadAccounting.ResultId = db.ResultTables
                        .FirstOrDefault(n => n.Result.Replace("\r\n", "").Trim() == cbResult.Text.Replace("\r\n", "").Trim()).ResultId;
                }


                if (!String.IsNullOrEmpty(cbReward.Text))
                {
                    if (db.RewardTables.FirstOrDefault(n => n.Reward == cbReward.Text) == null)
                    {
                        RewardTable rewardTable = new RewardTable
                        {
                            Reward = cbReward.Text
                        };
                        db.RewardTables.Add(rewardTable);
                        db.SaveChanges();
                    }
                    olympiadAccounting.RewardId = db.RewardTables
                        .FirstOrDefault(n => n.Reward.Replace("\r\n", "").Trim() == cbReward.Text.Replace("\r\n", "").Trim()).RewardId;
                }

                db.SaveChanges();
                MessageBox.Show("Данные успешно зафиксированы.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                save = true;
            }
        }

        private void Next(int index)
        {
            cbNameOlympiad.Items.Clear();
            cbSchoolStage.Items.Clear();
            cbLevel.Items.Clear();

            cbNameOlympiad.Items.Add(db.RegistrationOfParticipantsTables.FirstOrDefault(n => n.RegistrationOfParticipantsId == index)
                   .OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName);
            cbNameOlympiad.SelectedIndex = 0;
            cbSchoolStage.Items.Add(db.RegistrationOfParticipantsTables.FirstOrDefault(n => n.RegistrationOfParticipantsId == index)
                .OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.SchoolStageTable.SchoolStage);
            cbSchoolStage.SelectedIndex = 0;
            cbLevel.Items.Add(db.RegistrationOfParticipantsTables.FirstOrDefault(n => n.RegistrationOfParticipantsId == index)
                .OlympiadPlanTable.OlimpiadByAgeAndLevelTable.LevelTable.OlympiadLevel);
            cbLevel.SelectedIndex = 0;
            var students = db.RegistrationOfParticipantsTables.FirstOrDefault(n => n.RegistrationOfParticipantsId == index).StudentTable;
            cbStudent.Text = students.StudentSurname + " " + students.StudentName + " " + students.StudentMiddle;
            tbClass.Text = db.RegistrationOfParticipantsTables.FirstOrDefault(n => n.RegistrationOfParticipantsId == index)
                .StudentTable.ClassTable.Class.ToString();
        }

        private void NotFixed()
        {
            notFixed = new List<int>();
            string one = db.RegistrationOfParticipantsTables.FirstOrDefault(n => n.RegistrationOfParticipantsId == index)
                .OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName;
            notFixed.AddRange(db.RegistrationOfParticipantsTables.Where(n => n.Absent == null && n.OlympiadPlanTable
                    .OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName == one && n.OlympiadPlanTable.Date <= DateTime.Now)
                .Select(n => n.RegistrationOfParticipantsId).ToList());
            notFixed.AddRange(db.RegistrationOfParticipantsTables.Where(n => n.Absent == null && n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName != one
            && n.OlympiadPlanTable.Date <= DateTime.Now).Select(n => n.RegistrationOfParticipantsId).ToList());
            notFixed.Remove(index);
        }

        private List<int> notFixed;

        private void FormNewParticipant_Load(object sender, EventArgs e)
        {
            if (this.Text == "Регистрация участника")
            {
                cbNameOlympiad.DataSource = db.OlympiadPlanTables.Where(n => n.Date > DateTime.Now)
                    .Select(n => n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName)
                    .Distinct()
                    .ToList();
                
                if (cbNameOlympiad.Items.Count == 0)
                {
                    MessageBox.Show("Олимпиад пока не планируется", 
                        "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    exit = true;
                    this.Close();
                }else if (Properties.Settings.Default.saveForm)
                {
                    cbNameOlympiad.SelectedIndex = Properties.Settings.Default.olympiad;
                    cbSchoolStage.SelectedIndex = Properties.Settings.Default.stage;
                    cbLevel.SelectedIndex = Properties.Settings.Default.level;
                    cbStudent.SelectedIndex = Properties.Settings.Default.student;
                }
            }
            else
            {
                NotFixed();
                cbResult.Items.AddRange(db.ResultTables.Select(n => n.Result).ToArray());
                cbReward.Items.AddRange(db.RewardTables.Select(n => n.Reward).ToArray());
                Next(index);
                cbNameOlympiad.Enabled = false;
                cbSchoolStage.Enabled = false;
                cbLevel.Enabled = false;
                cbStudent.Enabled = false;
                btnEditInfoParticipant.Visible = false;
            }
        }

        bool exit = false;

        private void FormNewParticipant_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!exit)
            {
                if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes)
                {
                    exit = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void cbStudent_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbStudent.SelectedIndex != -1)
            {
                if (cbStudent.SelectedItem.ToString() == "Добавить ученика")
                {
                    FormNewStudent newStudent = new FormNewStudent();
                    newStudent.Text = "Добавление ученика";
                    newStudent.nudClass.Minimum = min;
                    newStudent.nudClass.Maximum = max;
                    newStudent.ShowDialog();
                    if (newStudent.DialogResult == DialogResult.Yes)
                    {
                        ItemsComboboxStudents();
                        cbStudent.SelectedIndex = nameNewStudent;
                    }
                    else if (cbStudent.Items.Count > 1)
                    {
                        cbStudent.SelectedIndex = 0;
                    }
                    else
                    {
                        cbStudent.SelectedIndex = -1;
                    }
                }
                else
                {
                    string name = cbStudent.Text.Split(' ')[1];
                    string surname = cbStudent.Text.Split(' ')[0];
                    string middle = cbStudent.Text.Split(' ')[2];

                    tbClass.Text = db.StudentTables.FirstOrDefault(n => n.StudentName.Replace("\r\n", "").Trim() == name && n.StudentSurname.Replace("\r\n", "").Trim() == surname
                        && n.StudentMiddle.Replace("\r\n", "").Trim() == middle).ClassTable.Class.ToString();
                }
            }
            else
            {
                tbClass.Text = "";
            }

            save = false;
        }

        int nameNewStudent = 0;
        List<string> name;

        private void ItemsComboboxStudents()
        {
            cbStudent.Items.Clear();
            name = new List<string>();
            var students = db.StudentTables.OrderBy(n => n.StudentSurname).ToList();

            foreach (var s in students)
            {
                if (s.ClassTable.Class >= min && s.ClassTable.Class <= max)
                {
                    name.Add(s.StudentSurname.Trim() + " " + s.StudentName.Trim() + " " + s.StudentMiddle.Trim());
                    cbStudent.Items.Add(s.StudentSurname.Trim() + " " + s.StudentName.Trim() + " " + s.StudentMiddle.Trim());
                    if (s.StudentId == db.StudentTables.Select(n => n.StudentId).Max())
                    {
                        nameNewStudent = name.Count - 1;
                    }
                }
            }

            cbStudent.Items.Add("Добавить ученика");
            cbStudent.SelectedIndex = -1;
            cbStudent.DropDownHeight = 106;
        }

        int min;
        int max;

        private void cbSchoolStage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.Text == "Регистрация участника")
            {
                if (cbSchoolStage.SelectedItem.ToString() == "Начальные классы")
                {
                    min = 1;
                    max = 4;
                }
                else if (cbSchoolStage.SelectedItem.ToString() == "Средние классы")
                {
                    min = 5;
                    max = 9;
                }
                else if (cbSchoolStage.SelectedItem.ToString() == "Старшие классы")
                {
                    min = 10;
                    max = 12;
                }

                cbStudent.SelectedIndex = -1;
                ItemsComboboxStudents();

                cbLevel.DataSource = db.OlympiadPlanTables
                    .Where(n => n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName == cbNameOlympiad.Text
                    && n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.SchoolStageTable.SchoolStage == cbSchoolStage.Text &&
                    n.Date > DateTime.Now)
                    .Select(n => n.OlimpiadByAgeAndLevelTable.LevelTable.OlympiadLevel)
                    .Distinct()
                    .ToList();

                save = false;
            }
        }

        private void cbStudent_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index != -1)
            {
                e.DrawBackground();
                string text = ((ComboBox)sender).Items[e.Index].ToString();
                Color color = Color.White;
                if (e.Index == cbStudent.Items.Count - 1)
                {
                    color = Color.Gray;
                }
                e.Graphics.FillRectangle(new SolidBrush(color), e.Bounds);
                e.Graphics.DrawString(text,
                        e.Font, Brushes.Black, e.Bounds, StringFormat.GenericDefault);
                e.DrawFocusRectangle();
            }
        }

        private void cbNameOlympiad_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.Text == "Регистрация участника")
            {
                cbLevel.DropDownHeight = 106;
                cbSchoolStage.DropDownHeight = 106;

                cbSchoolStage.DataSource = db.OlympiadPlanTables
                    .Where(n => n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName == cbNameOlympiad.Text && n.Date > DateTime.Now)
                    .Select(n => n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.SchoolStageTable.SchoolStage)
                    .Distinct()
                    .ToList();
                save = false;
            }

        }

        private void cbLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbLevel.SelectedIndex != -1)
            {
                if (this.Text == "Регистрация участника")
                {
                    cbTeam.Checked = db.OlympiadPlanTables
                .FirstOrDefault(n => n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName == cbNameOlympiad.Text
                && n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.SchoolStageTable.SchoolStage == cbSchoolStage.Text
                && n.OlimpiadByAgeAndLevelTable.LevelTable.OlympiadLevel == cbLevel.Text
                && n.Date > DateTime.Today)
                .OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.Team;

                    dtpActualDate.Value = db.OlympiadPlanTables
                    .FirstOrDefault(n => n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName == cbNameOlympiad.Text
                    && n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.SchoolStageTable.SchoolStage == cbSchoolStage.Text
                    && n.OlimpiadByAgeAndLevelTable.LevelTable.OlympiadLevel == cbLevel.Text && n.Date > DateTime.Today)
                    .Date;

                    save = false;
                }
                else
                {
                    var rgrt = index;

                    cbTeam.Checked = db.OlympiadPlanTables
                    .FirstOrDefault(n => n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName == cbNameOlympiad.Text
                    && n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.SchoolStageTable.SchoolStage == cbSchoolStage.Text
                    && n.OlimpiadByAgeAndLevelTable.LevelTable.OlympiadLevel == cbLevel.Text
                    && n.Date <= DateTime.Today)
                    .OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.Team;

                    dtpActualDate.Value = db.OlympiadPlanTables
                    .FirstOrDefault(n => n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.OlympiadTable.OlympiadName == cbNameOlympiad.Text
                    && n.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.SchoolStageTable.SchoolStage == cbSchoolStage.Text
                    && n.OlimpiadByAgeAndLevelTable.LevelTable.OlympiadLevel == cbLevel.Text && n.Date <= DateTime.Today)
                    .Date;
                }
            }
        }

        private void btnEditInfoParticipant_Click(object sender, EventArgs e)
        {
            if (cbStudent.SelectedIndex != -1)
            {
                string name = cbStudent.Text.Split(' ')[1];
                string surname = cbStudent.Text.Split(' ')[0];
                string middle = cbStudent.Text.Split(' ')[2];

                FormNewStudent newStudent = new FormNewStudent();
                newStudent.Text = "Изменение информации об ученике";
                newStudent.nudClass.Minimum = 1;
                newStudent.nudClass.Maximum = 12;
                newStudent.nudClass.Value = Convert.ToDecimal(tbClass.Text);
                newStudent.index = db.StudentTables.FirstOrDefault(n => n.StudentName.Replace("\r\n", "").Trim() == name && n.StudentSurname.Replace("\r\n", "").Trim() == surname
                        && n.StudentMiddle.Replace("\r\n", "").Trim() == middle).StudentId;
                newStudent.ShowDialog();
                if (newStudent.DialogResult == DialogResult.Yes)
                {
                    Properties.Settings.Default.saveForm = true;
                    Properties.Settings.Default.olympiad = cbNameOlympiad.SelectedIndex;
                    Properties.Settings.Default.stage = cbSchoolStage.SelectedIndex;
                    Properties.Settings.Default.level = cbLevel.SelectedIndex;
                    Properties.Settings.Default.student = cbStudent.SelectedIndex;
                    exit = true;
                    DialogResult = DialogResult.Yes;
                }
                else
                {
                    Properties.Settings.Default.saveForm = false;
                }
            }
            else
            {
                MessageBox.Show("Сначала выберите студента из списка","Внимание", 
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        Regex regex;

        private void cbStudent_TextChanged(object sender, EventArgs e)
        {
            if (this.Text == "Регистрация участника")
            {
                if (cbStudent.SelectedIndex == -1)
                {
                    regex = new Regex(@"^" + cbStudent.Text + @"(\w*)", RegexOptions.IgnoreCase);
                    cbStudent.Items.Clear();
                    for (int i = 0; i < name.Count; i++)
                    {
                        if (regex.IsMatch(name[i]))
                        {
                            cbStudent.Items.Add(name[i]);
                        }
                    }
                    cbStudent.Items.Add("Добавить ученика");
                    cbStudent.SelectionStart = cbStudent.Text.Length;
                }
            }
        }

        private void tbScores_Leave(object sender, EventArgs e)
        {
            if (tbScores.Text.Contains(","))
            {
                tbScores.Text = tbScores.Text.TrimStart('0');
                tbScores.Text = tbScores.Text.TrimEnd('0');
                if (tbScores.Text == ",")
                {
                    tbScores.Text = "";
                }
                else
                {
                    if (tbScores.Text[0] == ',')
                    {
                        tbScores.Text = tbScores.Text.Insert(0, "0");
                    }
                    if (tbScores.Text[tbScores.Text.Length - 1] == ',')
                    {
                        tbScores.Text = tbScores.Text.Remove(tbScores.Text.Length - 1, 1);
                    }
                }
            }
            else
            {
                tbScores.Text = tbScores.Text.TrimStart('0');
            }
        }

        private void Check()
        {
            if (cbStudent.SelectedIndex != -1 && !save)
            {
                if (MessageBox.Show("Сохранить заявку на олимпиаду?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    btnSave_Click(null, null);
                    save = false;
                }
            }
        }

        private void Fixed()
        {
            if (!save)
            {
                if (MessageBox.Show("Зафиксировать результат перед выходом?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    btnSave_Click(null, null);
                    save = false;
                }
            }
            else
            {
                save = false;
            }
        }

        private void btnFurther_Click(object sender, EventArgs e)
        {
            if (this.Text == "Регистрация участника")
            {
                Check();
                cbStudent.SelectedIndex = -1;
                save = false;
            }
            else
            {
                Fixed();
                if (cbAbsent.Checked)
                {
                    cbAbsent.CheckState = CheckState.Unchecked;
                }
                else
                {
                    Clear();
                }
                if (notFixed.Count == 0)
                {
                    NotFixed();
                }
                if (notFixed.Count != 0)
                {
                    index = notFixed[0];
                    Next(index);
                    notFixed.Remove(index);
                }
                else
                {
                    MessageBox.Show("Нет записей для фиксации", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы уверены, что хотите выйти?", "Внимание!",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (this.Text == "Регистрация участника")
                {
                    Check();
                }
                else
                {
                    Fixed();
                }
                exit = true;
                this.Close();
            }
        }

        private void Clear()
        {
            tbScores.Clear();
            cbResult.Text = "";
            cbResult.SelectedIndex = -1;
            cbReward.Text = "";
            cbReward.SelectedIndex = -1;
        }

        private void cbAbsent_CheckedChanged(object sender, EventArgs e)
        {
            if (cbAbsent.CheckState == CheckState.Checked)
            {
                tbScores.ReadOnly = true;
                cbResult.Enabled = false;
                cbReward.Enabled = false;
                Clear();
            }
            else
            {
                tbScores.ReadOnly = false;
                cbResult.Enabled = true;
                cbReward.Enabled = true;
            }
        }
    }
}
﻿
namespace ParticipationInOlympiads
{
    partial class FormNewStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNewStudent));
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblMiddle = new System.Windows.Forms.Label();
            this.lblClass = new System.Windows.Forms.Label();
            this.nudClass = new System.Windows.Forms.NumericUpDown();
            this.tbSurname = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbMiddle = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudClass)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(18, 22);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(59, 13);
            this.lblSurname.TabIndex = 0;
            this.lblSurname.Text = "Фамилия:";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(45, 56);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(32, 13);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Имя:";
            // 
            // lblMiddle
            // 
            this.lblMiddle.AutoSize = true;
            this.lblMiddle.Location = new System.Drawing.Point(20, 95);
            this.lblMiddle.Name = "lblMiddle";
            this.lblMiddle.Size = new System.Drawing.Size(57, 13);
            this.lblMiddle.TabIndex = 2;
            this.lblMiddle.Text = "Отчество:";
            // 
            // lblClass
            // 
            this.lblClass.AutoSize = true;
            this.lblClass.Location = new System.Drawing.Point(36, 133);
            this.lblClass.Name = "lblClass";
            this.lblClass.Size = new System.Drawing.Size(41, 13);
            this.lblClass.TabIndex = 3;
            this.lblClass.Text = "Класс:";
            // 
            // nudClass
            // 
            this.nudClass.Location = new System.Drawing.Point(92, 131);
            this.nudClass.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.nudClass.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudClass.Name = "nudClass";
            this.nudClass.Size = new System.Drawing.Size(65, 20);
            this.nudClass.TabIndex = 3;
            this.nudClass.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tbSurname
            // 
            this.tbSurname.Location = new System.Drawing.Point(92, 19);
            this.tbSurname.MaxLength = 30;
            this.tbSurname.Name = "tbSurname";
            this.tbSurname.Size = new System.Drawing.Size(305, 20);
            this.tbSurname.TabIndex = 0;
            this.tbSurname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSurname_KeyPress);
            this.tbSurname.Leave += new System.EventHandler(this.tbSurname_Leave);
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(92, 53);
            this.tbName.MaxLength = 30;
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(305, 20);
            this.tbName.TabIndex = 1;
            this.tbName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbName_KeyPress);
            this.tbName.Leave += new System.EventHandler(this.tbName_Leave);
            // 
            // tbMiddle
            // 
            this.tbMiddle.Location = new System.Drawing.Point(92, 92);
            this.tbMiddle.MaxLength = 30;
            this.tbMiddle.Name = "tbMiddle";
            this.tbMiddle.Size = new System.Drawing.Size(305, 20);
            this.tbMiddle.TabIndex = 2;
            this.tbMiddle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbMiddle_KeyPress);
            this.tbMiddle.Leave += new System.EventHandler(this.tbMiddle_Leave);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(167, 173);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FormNewStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(429, 218);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbMiddle);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.tbSurname);
            this.Controls.Add(this.nudClass);
            this.Controls.Add(this.lblClass);
            this.Controls.Add(this.lblMiddle);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblSurname);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormNewStudent";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление ученика";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormNewStudent_FormClosing);
            this.Load += new System.EventHandler(this.FormNewStudent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudClass)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblMiddle;
        private System.Windows.Forms.Label lblClass;
        private System.Windows.Forms.TextBox tbSurname;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbMiddle;
        private System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.NumericUpDown nudClass;
    }
}
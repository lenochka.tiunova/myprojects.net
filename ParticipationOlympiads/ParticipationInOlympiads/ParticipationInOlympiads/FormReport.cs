﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ParticipationInOlympiads
{
    public partial class FormReport : Form
    {
        public FormReport()
        {
            InitializeComponent();
        }

        dbRegistrationOfParticipationInOlympiadsEntities db = new dbRegistrationOfParticipationInOlympiadsEntities();
        string statistic;

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btnBack.Visible = false;
            tbResult.Text = " ";
            chartReport.Series[0].Points.Clear();
            bool checkNull = false;
            int year = 0;
            if (DateTime.Now.Month > 8)
            {
                year = DateTime.Now.Year;
            }
            else
            {
                year = DateTime.Now.Year - 1;
            }

            if (cbStatistics.SelectedIndex == 0)
            {
                var report = db.RegistrationOfParticipantsTables
                    .Where(n => n.StudentTable.ClassTable.DateAdmission <= dtpStart.Value
                    && n.StudentTable.ClassTable.DateAdmission.Year + n.StudentTable.ClassTable.Class + (dtpEnd.Value.Year - dtpStart.Value.Year) >= dtpEnd.Value.Year
                    && n.OlympiadPlanTable.Date > dtpStart.Value && n.OlympiadPlanTable.Date < dtpEnd.Value)
                    .Select(k => new
                    {
                        k.StudentId,
                        k.StudentTable.StudentSurname,
                        k.StudentTable.StudentName,
                        k.StudentTable.StudentMiddle,
                        k.StudentTable.ClassTable.Class,
                        CountParticipation = db.RegistrationOfParticipantsTables.Where(m => m.StudentId == k.StudentId && m.Absent != true
                        && m.Absent != null && m.OlympiadPlanTable.Date > dtpStart.Value && m.OlympiadPlanTable.Date < dtpEnd.Value).Count()
                    }).Distinct().OrderByDescending(n => new { n.CountParticipation, n.Class }).ToList();

                if (report.Count() != 0)
                {
                    checkNull = true;
                    dgvReport.DataSource = report;
                    statistic = "student";
                    dgvReport.Columns[0].Visible = false;
                    dgvReport.Columns[1].HeaderText = "Фамилия";
                    dgvReport.Columns[1].DisplayIndex = 1;
                    dgvReport.Columns[2].HeaderText = "Имя";
                    dgvReport.Columns[2].DisplayIndex = 2;
                    dgvReport.Columns[3].HeaderText = "Отчество";
                    dgvReport.Columns[3].DisplayIndex = 3;
                    dgvReport.Columns[4].HeaderText = "Класс";
                    dgvReport.Columns[4].DisplayIndex = 4;
                    dgvReport.Columns[5].HeaderText = "Количество участий";
                    dgvReport.Columns[5].DisplayIndex = 5;

                    for (int i = 0; i < report.Count; i++)
                    {
                        if (i < 15)
                        {
                            if (report[i].CountParticipation != 0)
                            {
                                chartReport.Series[0].Points
                                .AddXY(report[i].StudentSurname + " " + report[i].StudentName + " " + report[i].StudentMiddle,
                                report[i].CountParticipation);
                                chartReport.Series[0].Points[i].LabelBackColor = Color.LightBlue;
                            }
                        }
                    }
                }
            }
            else
            {
                var report = db.RegistrationOfParticipantsTables
                   .Where(n => n.StudentTable.ClassTable.DateAdmission <= dtpStart.Value
                   && n.StudentTable.ClassTable.DateAdmission.Year + n.Class + (dtpEnd.Value.Year - dtpStart.Value.Year) > dtpEnd.Value.Year)
                   .Select(k => new
                   {
                       k.StudentTable.ClassTable.ClassId,
                       k.Class,
                       CountParticipation = db.RegistrationOfParticipantsTables.Where(m => m.StudentTable.ClassTable.ClassId == k.StudentTable.ClassTable.ClassId && m.Absent != true
                       && m.Absent != null && m.OlympiadPlanTable.Date > dtpStart.Value && m.OlympiadPlanTable.Date < dtpEnd.Value).Count()
                   }).Distinct().OrderByDescending(n => new { n.CountParticipation, n.Class }).ToList();

                if (report.Count() != 0)
                {
                    checkNull = true;
                    dgvReport.DataSource = report;
                    statistic = "class";
                    dgvReport.Columns[0].Visible = false;
                    dgvReport.Columns[1].HeaderText = "Класс";
                    dgvReport.Columns[2].HeaderText = "Количество участий";

                    for (int i = 0; i < report.Count; i++)
                    {
                        if (report[i].CountParticipation != 0)
                        {
                            chartReport.Series[0].Points
                            .AddXY(report[i].ClassId.ToString(),
                            report[i].CountParticipation);
                            chartReport.Series[0].Points[i].LabelBackColor = Color.LightBlue;
                        }
                    }
                }
            }

            dgvReport.AutoResizeColumns();

            if (!checkNull)
            {
                MessageBox.Show("За данный период нет результатов олимпиад", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dgvReport.DataSource = null;
            }
        }

        bool table = false;

        private void FormReport_Load(object sender, EventArgs e)
        {
            cbStatistics.SelectedIndex = 0;
            chartReport.Series[0].IsVisibleInLegend = false;

            int year;
            if (DateTime.Now.Month > 8)
            {
                year = DateTime.Now.Year;
            }
            else
            {
                year = DateTime.Now.Year - 1;
            }
            dtpStart.Value = new DateTime(year, 09, 01);
            dtpEnd.Value = new DateTime(year + 1, 05, 31);
            table = true;
        }

        private void cbStatistics_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (table)
            {
                btnCreate_Click(null, null);
            }
        }

        private void dgvReport_DoubleClick(object sender, EventArgs e)
        {
            chartReport.Series[0].Points.Clear();
            int index = (int)(dgvReport.Rows[dgvReport.CurrentRow.Index].Cells[0].Value);
            btnBack.Visible = true;
            if (statistic == "student")
            {
                dgvReport.DataSource = db.RegistrationOfParticipantsTables.Where(n => n.StudentTable.ClassTable.DateAdmission <= dtpStart.Value
                    && n.StudentTable.ClassTable.DateAdmission.Year + n.StudentTable.ClassTable.Class + (dtpEnd.Value.Year - dtpStart.Value.Year) >= dtpEnd.Value.Year
                    && n.StudentId == index && n.Absent != true
                        && n.Absent != null && n.OlympiadPlanTable.Date > dtpStart.Value && n.OlympiadPlanTable.Date < dtpEnd.Value)
                    .Select(n => new {n.Class, 
                        n.ResultTable.Result, n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.
                    OlympiadTable.OlympiadName, n.OlympiadPlanTable.Date}).ToList();

                dgvReport.Columns[0].HeaderText = "Класс";
                dgvReport.Columns[0].DisplayIndex = 0;
                dgvReport.Columns[1].HeaderText = "Результат";
                dgvReport.Columns[1].DisplayIndex = 1;
                dgvReport.Columns[2].HeaderText = "Олимпиада";
                dgvReport.Columns[2].DisplayIndex = 2;
                dgvReport.Columns[3].HeaderText = "Дата проведения";
                dgvReport.Columns[3].DisplayIndex = 3;

                var studentName = db.StudentTables.FirstOrDefault(n => n.StudentId == index);
                tbResult.Text = studentName.StudentSurname + " " + studentName.StudentName + " " + studentName.StudentMiddle;
            }
            else
            {
                dgvReport.DataSource = db.RegistrationOfParticipantsTables.Where(n => n.StudentTable.ClassTable.DateAdmission <= dtpStart.Value
                    && n.StudentTable.ClassTable.DateAdmission.Year + n.StudentTable.ClassTable.Class + (dtpEnd.Value.Year - dtpStart.Value.Year) >= dtpEnd.Value.Year
                    && n.StudentTable.ClassTable.ClassId == index && n.Absent != true
                        && n.Absent != null && n.OlympiadPlanTable.Date > dtpStart.Value && n.OlympiadPlanTable.Date < dtpEnd.Value)
                    .Select(n => new {
                        n.StudentTable.StudentSurname,
                        n.StudentTable.StudentName,
                        n.StudentTable.StudentMiddle,
                        n.Class,
                        n.ResultTable.Result,
                        n.OlympiadPlanTable.OlimpiadByAgeAndLevelTable.OlympiadByAgeTable.
                    OlympiadTable.OlympiadName,
                        n.OlympiadPlanTable.Date
                    }).ToList();

                dgvReport.Columns[0].HeaderText = "Фамилия";
                dgvReport.Columns[0].DisplayIndex = 0;
                dgvReport.Columns[1].HeaderText = "Имя";
                dgvReport.Columns[1].DisplayIndex = 1;
                dgvReport.Columns[2].HeaderText = "Отчество";
                dgvReport.Columns[2].DisplayIndex = 2;
                dgvReport.Columns[3].HeaderText = "Класс";
                dgvReport.Columns[3].DisplayIndex = 0;
                dgvReport.Columns[4].HeaderText = "Результат";
                dgvReport.Columns[4].DisplayIndex = 1;
                dgvReport.Columns[5].HeaderText = "Олимпиада";
                dgvReport.Columns[5].DisplayIndex = 2;
                dgvReport.Columns[6].HeaderText = "Дата проведения";
                dgvReport.Columns[6].DisplayIndex = 3;

                tbResult.Text = "На текущий момент класс: " + db.ClassTables.FirstOrDefault(n => n.ClassId == index).Class;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            btnCreate_Click(null, null);
        }
    }
}
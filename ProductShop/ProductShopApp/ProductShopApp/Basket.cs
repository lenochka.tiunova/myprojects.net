﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductShopApp
{
    public partial class Basket : Form
    {
        public Basket()
        {
            InitializeComponent();
            dgBasket.DefaultCellStyle.NullValue = 1;
        }

        dbProductShopEntities db = new dbProductShopEntities();

        private void Basket_Load(object sender, EventArgs e)
        {
            var basket = db.BasketTable.Select(n => new { n.BasketId, n.DeliveryTable.ProductTable.ProductName, n.CountBasket }).ToList();
            dgBasket.DataSource = basket;
            dgBasket.Columns[0].Visible = false;
            dgBasket.Columns[1].HeaderText = "Наименование товара";
            dgBasket.Columns[2].HeaderText = "Количество";
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "Выбрать";
            dgBasket.Columns.Add(checkColumn);
            dgBasket.Columns[3].HeaderText = "Выбрать";
            dgBasket.AutoResizeColumns();
            dgBasket.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgBasket.ColumnHeadersDefaultCellStyle.Font = new Font(dgBasket.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
            dgBasket.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void btnBuy_Click(object sender, EventArgs e)
        {
            Random rand = new Random();

            int seller = rand.Next(13, 18);

            CheckTable checkTable = new CheckTable
            {
                DateCheck = DateTime.Now,
                SellerId = db.UserTable.FirstOrDefault(n => n.UserId == seller).UserId
            };
            db.CheckTable.Add(checkTable);
            db.SaveChanges();

            for (int i = 0; i < dgBasket.RowCount; i++)
            {
                try
                {
                    if ((bool)dgBasket.Rows[i].Cells["Выбрать"].Value == true)
                    {
                        var index = int.Parse(dgBasket.Rows[i].Cells[0].Value.ToString());
                        var index_2 = db.BasketTable.FirstOrDefault(n => n.BasketId == index).DeliveryId;

                        SaleTable saleTable = new SaleTable
                        {
                            DeliveryId = index_2,
                            PriceWithoutDiscount = (int)db.DeliveryTable.FirstOrDefault(n => n.DeliveryId == index_2).UnitPrice,
                            CountSale = (int)dgBasket.Rows[i].Cells[2].Value,
                            CheckId = db.CheckTable.Select(n => n.CheckId).Max(),
                            Discount = (byte)(rand.Next(1, 3) * 5 * rand.Next(0, 2)),
                            VATSale = db.DeliveryTable.FirstOrDefault(n => n.DeliveryId == index_2).ProductTable.CategoryTable.VATTable.Percent
                        };
                        db.SaleTable.Add(saleTable);
                        db.BasketTable.Remove(db.BasketTable.FirstOrDefault(n => n.BasketId == index));
                    }
                }
                catch (NullReferenceException)
                {
                }
            }
            db.SaveChanges();
            this.Close();
            MessageBox.Show("Покупка успешно совершена.", "Внимание", MessageBoxButtons.OK);
        }
    }
}
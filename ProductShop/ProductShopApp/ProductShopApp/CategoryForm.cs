﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductShopApp
{
    public partial class CategoryForm : Form
    {
        public CategoryForm()
        {
            InitializeComponent();
        }

        dbProductShopEntities db = new dbProductShopEntities();
        public int index = -1;

        private void CategoryForm_Load(object sender, EventArgs e)
        {
            cbVAT.DataSource = db.VATTable.Select(n => n.Percent).ToList();
            if (this.Text == "Редактирование")
            {
                tbNameCategory.Text = db.CategoryTable.FirstOrDefault(n => n.CategoryId == index).CategoryName;
                cbVAT.Text = db.CategoryTable.FirstOrDefault(n => n.CategoryId == index).VATTable.Percent.ToString();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tbNameCategory.Text != "" && cbVAT.Text != "")
            {
                if (this.Text == "Добавление")
                {
                    CategoryTable categoryTable = new CategoryTable
                    {
                        CategoryName = tbNameCategory.Text, VATId = db.VATTable.FirstOrDefault(n => n.Percent.ToString() == cbVAT.Text).VATId
                    };
                    db.CategoryTable.Add(categoryTable);
                    db.SaveChanges();
                    MessageBox.Show("Данные добавлены в таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    var categoryTable = db.CategoryTable.FirstOrDefault(n => n.CategoryId == index);
                    categoryTable.CategoryName = tbNameCategory.Text;
                    categoryTable.VATId = db.VATTable.FirstOrDefault(n => n.Percent.ToString() == cbVAT.Text).VATId;
                    db.SaveChanges();
                    MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Заполните все поля.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}

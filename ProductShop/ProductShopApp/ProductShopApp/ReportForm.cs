﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ProductShopApp
{
    public partial class reportForm : Form
    {
        public reportForm()
        {
            InitializeComponent();

            int countQuarter = (DateTime.Now.Month + 2) / 3;
            string[] quarter = new string[countQuarter];
            for (int i = 0; i < countQuarter; i++)
            {
                quarter[i] = i+1 + "-й квартал";
            }
            int rvrvev = DateTime.Now.Month + 8;
            int countMonth = DateTime.Now.Month;
            string[] month = new string[countMonth];
            for (int i = 0; i < countMonth; i++)
            {
                month[i] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i+1);
            }

            cbMonth.DataSource = month;
            cbQuarter.DataSource = quarter;
        }

        dbProductShopEntities db = new dbProductShopEntities();

        private void btnReport_Click(object sender, EventArgs e)
        {
            chartReport.Series[0].Points.Clear();
            DateTime dateStart = new DateTime();
            DateTime dateEnd = new DateTime();
            if (rbPeriod.Checked)
            {
                if (dtpStart.Value <= dtpEnd.Value)
                {
                    dateStart = dtpStart.Value.AddDays(-1); // без этого дополнения, именно в этом случае, дата берется не включительно
                    dateEnd = dtpEnd.Value;
                }
                else
                {
                    MessageBox.Show("Неверный формат даты.","Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } 
            else if (rbMonth.Checked)
            {
                dateStart = new DateTime(DateTime.Now.Year, cbMonth.SelectedIndex + 1, 1);
                dateEnd = new DateTime(DateTime.Now.Year, cbMonth.SelectedIndex + 1, DateTime.DaysInMonth(DateTime.Now.Year, cbMonth.SelectedIndex + 1));
            } 
            else if (rbQuarter.Checked)
            {
                int month = (cbQuarter.SelectedIndex + 1) * 3;
                dateStart = new DateTime(DateTime.Now.Year, month - 2, 1);
                dateEnd = new DateTime(DateTime.Now.Year, month, DateTime.DaysInMonth(DateTime.Now.Year,month));
            }
            else
            {
                dateStart = new DateTime(DateTime.Now.Year - 5, 1, 1);
                dateEnd = new DateTime(DateTime.Now.Year, 12, 31);
            }
            if (dtpStart.Value <= dtpEnd.Value || !rbPeriod.Checked)
            {
                var report = db.SaleTable.Where(n => n.CheckTable.DateCheck >= dateStart && n.CheckTable.DateCheck <= dateEnd)
                    .Select(m => new
                    {
                        m.DeliveryTable.ProductTable.ProductName,
                        m.CountSale,
                         price = Math.Round(m.PriceWithoutDiscount + (double)m.PriceWithoutDiscount / 100 * m.VATSale -
                    (m.PriceWithoutDiscount + (double)m.PriceWithoutDiscount / 100 * m.VATSale) / 100 * m.Discount),
                        pricecount = Math.Round((m.PriceWithoutDiscount + (double)m.PriceWithoutDiscount / 100 * m.VATSale) -
                    (m.PriceWithoutDiscount + (double)m.PriceWithoutDiscount / 100 * m.VATSale) / 100 * m.Discount) * m.CountSale
                    }).ToList();
                if (report.Count != 0)
                {
                    dgvReport.DataSource = report;
                    dgvReport.Columns[0].HeaderText = "Наименование продукта";
                    dgvReport.Columns[1].HeaderText = "Количество";
                    dgvReport.Columns[2].HeaderText = "Цена за единицу";
                    dgvReport.Columns[3].HeaderText = "Цена";
                    dgvReport.AutoResizeColumns();
                    dgvReport.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgvReport.ColumnHeadersDefaultCellStyle.Font = new Font(dgvReport.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                    lblSum.Text = "Общая стоимость: " + report.Sum(n => n.pricecount).ToString() + " руб. ";
                    lblCount.Text = "Общее количество: " + report.Count().ToString();
                    lblAverage.Text = "Средняя стоимость покупки: " + Math.Round(report.Average(n => n.pricecount)).ToString() + " руб. ";
                    lblMax.Text = "Максимальная стоимость покупки: " + report.Max(n => n.pricecount).ToString() + " руб. ";
                    lblMin.Text = "Минимальная стоимость покупки: " + report.Min(n => n.pricecount).ToString() + " руб. ";

                    chartReport.ChartAreas[0].AxisX.LabelStyle.Angle = -90;
                    chartReport.ChartAreas[0].AxisX.IntervalAutoMode = IntervalAutoMode.VariableCount;
                    chartReport.ChartAreas[0].AxisX.IsLabelAutoFit = false;
                    chartReport.Series[0].IsValueShownAsLabel = true;

                    for (int i = 0; i < report.Count(); i++)
                    {
                        bool check = true;

                        for (int j = 0; j < chartReport.Series[0].Points.Count(); j++)
                        {
                            if (chartReport.Series[0].Points[j].AxisLabel == report[i].ProductName)
                            {
                                check = false;
                            }
                        }
                        if (check)
                        {
                            chartReport.Series[0].Points.AddXY(report[i].ProductName, report.Where(n => n.ProductName == report[i].ProductName).Sum(n => n.pricecount));
                        }
                    }
                }
                else
                {
                    MessageBox.Show("За данный период не было продаж", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgvReport.DataSource = null;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProductShopApp
{
    public partial class RoleForm : Form
    {
        public RoleForm()
        {
            InitializeComponent();
        }

        dbProductShopEntities db = new dbProductShopEntities();
        public int index = -1;

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("Вы уверены, что хотите отменить действие?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Information)) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tbNameRole.Text != "")
            {
                if (this.Text == "Добавление")
                {
                    RoleTable roleTable = new RoleTable
                    {
                        NameRole = tbNameRole.Text
                    };
                    db.RoleTable.Add(roleTable);
                    db.SaveChanges();
                    MessageBox.Show("Данные добавлены в таблицу.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    var roleTable = db.RoleTable.FirstOrDefault(n => n.RoleId == index);
                    roleTable.NameRole = tbNameRole.Text;
                    db.SaveChanges();
                    MessageBox.Show("Данные успешно изменены.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Заполните поле.", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void RoleForm_Load(object sender, EventArgs e)
        {
            if (this.Text == "Редактирование")
            {
                tbNameRole.Text = db.RoleTable.FirstOrDefault(n => n.RoleId == index).NameRole;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestApp
{
    /// <summary>
    /// Логика взаимодействия для Hello.xaml
    /// </summary>
    public partial class Hello : Page
    {
        public Hello()
        {
            InitializeComponent();
        }
        private void btnTest_Click(object sender, RoutedEventArgs e)
        {
            bool proverka = false;
            if (tbName.Text == String.Empty && tbName.Text.All(char.IsWhiteSpace) != false)
            {
                MessageBox.Show("Вы забыли ввести ваше имя :)", "Ошибка");
            }
            Regex regex = new Regex(@"[А-Яа-яA-Za-z]");
            foreach (char test in tbName.Text)
            {
                if (!regex.IsMatch(test.ToString()))
                {
                    proverka = true;
                    MessageBox.Show("Имя может содержать только буквы :)", "Ошибка");
                    break;
                }
            }
            if (proverka == false)
            {
                ClassName.Name = tbName.Text;
                ClassManager.MainFrame.Navigate(new Instructions());
            }
        }

        private void tbName_PreviewExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == ApplicationCommands.Paste)
            {
                e.Handled = true;
            }
        }
    }
}

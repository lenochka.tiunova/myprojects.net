﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestApp
{
    /// <summary>
    /// Логика взаимодействия для Insructions.xaml
    /// </summary>
    public partial class Instructions : Page
    {
        public Instructions()
        {
            InitializeComponent();
             tbName.Text = ClassName.Name + ", данный тест разработан английскими психологами. \nОн поможет Вам объяснить, " +
                "насколько вы скупы в подарках. \nНа каждый вопрос отвечайте одним из трех способов: \n \"часто\", \"иногда\", \"никогда\".";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ClassManager.MainFrame.Navigate(new TestPage());
        }
    }
}

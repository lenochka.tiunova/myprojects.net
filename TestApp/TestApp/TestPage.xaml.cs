﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestApp
{
    /// <summary>
    /// Логика взаимодействия для TestPage.xaml
    /// </summary>
    public partial class TestPage : Page
    {
        public TestPage()
        {
            InitializeComponent();
            tbkQuestion1.Text += "Вы мастерите подарок своими руками, чтобы избежать необходимости бегать по магазинам?";
            tbkQuestion2.Text += "Вы покупаете подарок заранее, воспользовавшись \"барахолкой\"?";
            tbkQuestion3.Text += "Вы заворачиваете дешевый подарок в дорогую бумагу?";
            tbkQuestion4.Text += "Вы посылаете письмо вместо подарка?";
            tbkQuestion5.Text += "Вы бесследно исчезаете на время праздников?";
            tbkQuestion6.Text += "Вы берете не ценой, а оригинальностью (например, дарите дешевый сувенир из экзотической страны)?";
            tbkQuestion7.Text += "Вы составляете обширный список магазинов и \"барахолок\", даже если этот план требует значительных усилий?";
            tbkQuestion8.Text += "Вы меняете обычную упаковку дешевого подарка на более дорогую?";
            tbkQuestion9.Text += "Вы скрываете отсутствие подарка за пышными поздравлениями и скромным букетом?";
            tbkQuestion10.Text += "Вы сохраняете простодушный вид, придя в гости вообще без подарка?";
            tbkQuestion11.Text += "Вы дарите недорогие, но тщательно выбранные и полезные подарки?";
            tbkQuestion12.Text += "Вы заранее составляете список подарков и определяете сумму, которую вы собираетесь потратить?";
            tbkQuestion13.Text += "Вы передаривайте подарки, преподнесенные вам раньше и припасенные специально на такой случай?";
            tbkQuestion14.Text += "Вы дарите лотерейный билет, упакованный в килограмм золоченой бумаги?";
            tbkQuestion15.Text += "Вы \"заболеваете\" накануне праздника и рассылаете открытки с извинениями?";
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (rbCh1.IsChecked == false && rbI1.IsChecked == false && rbN1.IsChecked == false ||
                rbCh2.IsChecked == false && rbI2.IsChecked == false && rbN2.IsChecked == false ||
                rbCh3.IsChecked == false && rbI3.IsChecked == false && rbN3.IsChecked == false ||
                rbCh4.IsChecked == false && rbI4.IsChecked == false && rbN4.IsChecked == false ||
                rbCh5.IsChecked == false && rbI5.IsChecked == false && rbN5.IsChecked == false ||
                rbCh6.IsChecked == false && rbI6.IsChecked == false && rbN6.IsChecked == false ||
                rbCh7.IsChecked == false && rbI7.IsChecked == false && rbN7.IsChecked == false ||
                rbCh8.IsChecked == false && rbI8.IsChecked == false && rbN8.IsChecked == false ||
                rbCh9.IsChecked == false && rbI9.IsChecked == false && rbN9.IsChecked == false ||
                rbCh10.IsChecked == false && rbI10.IsChecked == false && rbN10.IsChecked == false ||
                rbCh11.IsChecked == false && rbI11.IsChecked == false && rbN11.IsChecked == false ||
                rbCh12.IsChecked == false && rbI12.IsChecked == false && rbN12.IsChecked == false ||
                rbCh13.IsChecked == false && rbI13.IsChecked == false && rbN13.IsChecked == false ||
                rbCh14.IsChecked == false && rbI14.IsChecked == false && rbN14.IsChecked == false ||
                rbCh15.IsChecked == false && rbI15.IsChecked == false && rbN15.IsChecked == false)
            {
                MessageBox.Show("Ответьте на все вопросы", "Сообщение", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                int i = 0, o = 0, p = 0, s = 0, n = 0;
                if (rbCh1.IsChecked == true)
                {
                    i += 3;
                }
                if (rbI1.IsChecked == true)
                {
                    i += 1;
                }
                if (rbCh6.IsChecked == true)
                {
                    i += 3;
                }
                if (rbI6.IsChecked == true)
                {
                    i += 1;
                }
                if (rbCh11.IsChecked == true)
                {
                    i += 3;
                }
                if (rbI11.IsChecked == true)
                {
                    i += 1;
                }
                if (rbCh2.IsChecked == true)
                {
                    o += 3;
                }
                if (rbI2.IsChecked == true)
                {
                    o += 1;
                }
                if (rbCh7.IsChecked == true)
                {
                    o += 3;
                }
                if (rbI7.IsChecked == true)
                {
                    o += 1;
                }
                if (rbCh12.IsChecked == true)
                {
                    o += 3;
                }
                if (rbI12.IsChecked == true)
                {
                    o += 1;
                }
                if (rbCh3.IsChecked == true)
                {
                    p += 3;
                }
                if (rbI3.IsChecked == true)
                {
                    p += 1;
                }
                if (rbCh8.IsChecked == true)
                {
                    p += 3;
                }
                if (rbI8.IsChecked == true)
                {
                    p += 1;
                }
                if (rbCh13.IsChecked == true)
                {
                    p += 3;
                }
                if (rbI13.IsChecked == true)
                {
                    p += 1;
                }
                if (rbCh4.IsChecked == true)
                {
                    s += 3;
                }
                if (rbI4.IsChecked == true)
                {
                    s += 1;
                }
                if (rbCh9.IsChecked == true)
                {
                    s += 3;
                }
                if (rbI9.IsChecked == true)
                {
                    s += 1;
                }
                if (rbCh14.IsChecked == true)
                {
                    s += 3;
                }
                if (rbI14.IsChecked == true)
                {
                    s += 1;
                }
                if (rbCh5.IsChecked == true)
                {
                    n += 3;
                }
                if (rbI5.IsChecked == true)
                {
                    n += 1;
                }
                if (rbCh10.IsChecked == true)
                {
                    n += 3;
                }
                if (rbI10.IsChecked == true)
                {
                    n += 1;
                }
                if (rbCh15.IsChecked == true)
                {
                    n += 3;
                }
                if (rbI15.IsChecked == true)
                {
                    n += 1;
                }
                string result = "";
                if (i >= o && i > p && i > s && i > n || i + o + p + s + n <= 10)
                {
                    result += ClassName.Name + ", вы изобретательно - экономны. \nВозможно, вы немного скуповаты, но прилагаете так много фантазии, чтобы скрыть это, что лишь" +
                        "\nсамые проницательные раскроют ваш секрет. Ваш девиз: подарок может быть недорогим, но должен" +
                        "\nбыть оригинальным. Вы просто суперспециалист по всяким изобретательным трюкам.";
                }
                else if (o > i && o >= p && o > s && o > n || i + o + p + s + n < 15)
                {
                    result += ClassName.Name + ", вы организованно-экономны. \nВы очень бережно относитесь к содержимому вашего кошелька, но компенсируете невозможность" +
                        "\nтратить крупные суммы большим количеством энергии, потраченной на процесс покупки подарков." +
                        "\nВаш конек: тщательное изучение рынка, поиск адресов дешевых магазинов, покупки, сделанные" +
                        "\nзаранее во время распродаж. Вы готовы проходить пешком большие расстояния, но всегда " +
                        "\nделаете покупки по заранее намеченному плану.";
                }
                else if (p > i && p > o && p >= s && i > n || i + o + p + s + n <= 25)
                {
                    result += ClassName.Name + ", вы экономная плутовка. \nВы обожаете пускать пыль в глаза, украшая копеечные подарки тоннами яркой бумаги" +
                        "\nи перешивая престижные этикетки на дешевую одежду. Вы прячете подаренные вам вещи" +
                        "\nдля того, чтобы передарить их при случае. Ведь главное — произвести впечатление.";
                }
                else if (s > i && s > o && s > p && s >= n || i + o + p + s + n <= 35)
                {
                    result += ClassName.Name + ", вы экономная сердечность. \nВы не можете позволить себе шикарные дорогие презенты. Зато, как никто другой, " +
                        "\nвы умеете выразить свои чувства в виде многочисленных комплиментов и поздравлений." +
                        "\nВсе это раздается в таком количестве, что никому и в голову не придет задуматься, " +
                        "\nпочему вы пришли без подарка.";
                }
                else if (n > i && n > o && n > p && n > s || i + o + p + s + n > 35)
                {
                    result += ClassName.Name + ", вы настоящая жадина. \nОдни пускаются на хитрости, другие рассыпаются в нежных чувствах, " +
                        "\nтретьи экономят за счет тщательно разработанного плана. Вы же просто - напросто" +
                        "\nнеспособны преодолеть свою природную скупость. Поэтому в период праздников" +
                        "\nвы преображаетесь в угрюмую медведицу или в бесследно пропавший призрак." +
                        "\nКонечно, вас не упрекнешь в неискренности, но все-таки постарайтесь понять, " +
                        "\nчто дарить бывает так же приятно, как и получать подарки.";
                }
                ClassManager.MainFrame.Navigate(new Result(result));
            }
        }
    }
}
